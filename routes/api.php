<?php

use App\Http\Controllers\Admin\ArticlesController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\FeedbackController;
use App\Http\Controllers\Admin\FilesController;
use App\Http\Controllers\Admin\FoldersController;
use App\Http\Controllers\Admin\LinksController;
use App\Http\Controllers\Admin\LocalesController;
use App\Http\Controllers\Admin\MenusController;
use App\Http\Controllers\Admin\ModulesController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\OrdersController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\Admin\PermissionsController;
use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\RolesController;
use App\Http\Controllers\Admin\SectionsController;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\Admin\SocialNetworksController;
use App\Http\Controllers\Admin\StatisticsController;
use App\Http\Controllers\Admin\TagsController;
use App\Http\Controllers\Admin\TranslationsController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\ArticlesController as AppArticlesController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CategoriesController as AppCategoriesController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\FaqController as AppFaqController;
use App\Http\Controllers\LocalesController as AppLocalesController;
use App\Http\Controllers\MenusController as AppMenusController;
use App\Http\Controllers\NewsController as AppNewsController;
use App\Http\Controllers\PagesController as AppPagesController;
use App\Http\Controllers\ProductsController as AppProductsController;
use App\Http\Controllers\SettingsController as AppSettingsController;
use App\Http\Controllers\UsersController as AppUsersController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('admin')
     ->group(function () {
         auth_routes(UsersController::class);

         getting_route('locales', AppLocalesController::class);
         getting_route('settings', SettingsController::class);

         Route::middleware('auth:api')
              ->group(function () {
                  Route::get('dashboard', [DashboardController::class, 'index']);
                  Route::get('statistics', [StatisticsController::class, 'index']);

                  sorting_route('locales', LocalesController::class);
                  sorting_route('social-networks', SocialNetworksController::class);
                  sorting_route('links', LinksController::class);
                  sorting_route('menus', MenusController::class);
                  sorting_route('categories', CategoriesController::class);

                  getting_route('modules', ModulesController::class);
                  getting_route('roles', RolesController::class);
                  getting_route('menus', MenusController::class);
                  getting_route('categories', CategoriesController::class);

                  Route::get('menus/links', [MenusController::class, 'links']);
                  Route::get('actions/get', [PermissionsController::class, 'actions']);

                  search_route('tags', TagsController::class);

                  postable('articles', ArticlesController::class, true);
                  postable('news', NewsController::class, true);
                  postable('products', ProductsController::class, true);

                  Route::prefix('settings')
                       ->group(function () {
                           Route::get('general', [SettingsController::class, 'manage']);
                           Route::put('save', [SettingsController::class, 'save']);
                       });

                  Route::prefix('translations')
                       ->group(function () {
                           Route::post('import', [TranslationsController::class, 'import']);
                           Route::post('publish', [TranslationsController::class, 'publish']);
                       });

                  Route::resources([
                      'modules' => ModulesController::class,
                      'locales' => LocalesController::class,
                      'permissions' => PermissionsController::class,
                      'roles' => RolesController::class,
                      'users' => UsersController::class,
                      'settings' => SettingsController::class,
                      'translations' => TranslationsController::class,
                      'social-networks' => SocialNetworksController::class,
                      'sections' => SectionsController::class,
                      'pages' => PagesController::class,
                      'faq' => FaqController::class,
                      'menus' => MenusController::class,
                      'categories' => CategoriesController::class,
                      'links' => LinksController::class,
                  ], ['except' => ['create', 'show']]);

                  Route::resource('orders', OrdersController::class)->except('create');

                  Route::apiResource('feedback', FeedbackController::class)
                       ->except([
                           'store',
                           'update',
                       ]);

                  /** Folders & files */
                  Route::apiResource('folders', FoldersController::class)->except('show');
                  Route::apiResource('files', FilesController::class)->except('index');
                  # todo: moving files

                  Route::prefix('roles/{role}/permissions')
                       ->group(function () {
                           Route::get('', [RolesController::class, 'listPermissions']);
                           Route::put('', [RolesController::class, 'savePermissions']);
                       });

                  batch_delete('modules', ModulesController::class);
                  batch_delete('permissions', PermissionsController::class);
                  batch_delete('roles', RolesController::class);
                  batch_delete('users', UsersController::class);
                  batch_delete('settings', SettingsController::class);
                  batch_delete('sections', SectionsController::class);
                  batch_delete('pages', PagesController::class);

                  toggle('modules', ModulesController::class);
                  toggle('locales', LocalesController::class);
                  toggle('roles', RolesController::class);
                  toggle('settings', SettingsController::class);
                  toggle('sections', SectionsController::class);
                  toggle('menus', MenusController::class);
                  toggle('categories', CategoriesController::class);
                  toggle('faq', FaqController::class);
                  toggle('links', LinksController::class);
              });
     });

Route::middleware('tracker')
     ->group(function () {
         auth_routes(AppUsersController::class, true);

         getting_route('locales', AppLocalesController::class);
         getting_route('settings', AppSettingsController::class);

         Route::get('locales/{locale}', [AppLocalesController::class, 'show']);

         Route::get('', [DashboardController::class, 'index'])->name('homepage');
         Route::get('menus', [AppMenusController::class, 'index'])->name('products');
         Route::get('contact-us', [DashboardController::class, 'index'])->name('contacts');

         postable('articles', AppArticlesController::class);
         postable('news', AppNewsController::class);
         postable('products', AppProductsController::class);

         Route::get('categories/{slug}', [AppCategoriesController::class, 'show'])
              ->name('categories.show');

         Route::get('pages/{slug}', [AppPagesController::class, 'show'])->name('pages.show');
         Route::get('faq', [AppFaqController::class, 'index'])->name('faq');
         Route::get('contacts', [ContactsController::class, 'index'])->name('contacts');
     });

/**
 * Generate authentication routes.
 *
 * @param string $userController
 * @param bool   $register
 *
 * @return void
 */
function auth_routes(string $userController, bool $register = false): void
{
    Route::prefix('auth')
         ->group(function () use ($userController, $register) {
             Route::middleware('guest:api')
                  ->group(function () use ($register) {
                      if ($register) {
                          Route::post('register', [RegisterController::class, 'register']);
                      }

                      Route::post('login', [LoginController::class, 'login']);
                  });

             Route::middleware('auth:api')
                  ->group(function () use ($userController) {
                      Route::get('user', [$userController, 'info']);
                      Route::delete('logout', [LoginController::class, 'logout']);
                  });
         });
}

/**
 * Generate toggle route.
 *
 * @param string $api
 * @param string $class
 *
 * @return void
 */
function toggle(string $api, string $class): void
{
    Route::patch("$api/{{$api}}/toggle", [$class, 'toggle']);
}

/**
 * Generate getting route.
 *
 * @param string      $api
 * @param string      $class
 * @param string|null $id
 *
 * @return void
 */
function getting_route(string $api, string $class, string $id = null): void
{
    if ($id) {
        Route::get("$api/{{$id}}/get", [$class, 'get']);
    } else {
        Route::get("$api/get", [$class, 'get']);
    }
}

/**
 * Generate sorting route.
 *
 * @param string $api
 * @param string $class
 *
 * @return void
 */
function sorting_route(string $api, string $class): void
{
    Route::patch("$api/sort", [$class, 'sort']);
}

/**
 * Generate getting route.
 *
 * @param string $api
 * @param string $class
 *
 * @return void
 */
function search_route(string $api, string $class): void
{
    Route::get("$api/search", [$class, 'search']);
}

/**
 * Generate batch delete route.
 *
 * @param string $api
 * @param string $class
 *
 * @return void
 */
function batch_delete(string $api, string $class): void
{
    Route::delete("$api/batch/destroy", [$class, 'batchDelete']);
}

/**
 * Generate postable routes.
 *
 * @param string    $postable
 * @param string    $class
 * @param bool|null $admin
 *
 * @return void
 */
function postable(string $postable, string $class, ?bool $admin = false): void
{
    if ($admin) {
        toggle($postable, $class);
        batch_delete($postable, $class);

        Route::resource($postable, $class)
             ->except(['create', 'show']);

        Route::prefix($postable)
             ->group(function () use ($postable, $class) {
                 Route::get('trash', [$class, 'trash']);

                 Route::prefix('batch')
                      ->group(function () use ($class) {
                          Route::patch('move-to-trash', [$class, 'batchMoveToTrash']);
                          Route::patch('restore', [$class, 'batchRestore']);
                      });

                 Route::prefix('{'.Str::singular($postable).'}')
                      ->group(function () use ($class) {
                          Route::patch('move-to-trash', [$class, 'moveToTrash']);
                          Route::patch('restore', [$class, 'restore']);
                      });
             });
    } else {
        Route::prefix($postable)
             ->group(function () use ($postable, $class) {
                 Route::get('', [$class, 'index']);
                 Route::get('{slug}', [$class, 'show'])->name("$postable.show");
             });
    }
}
