<?php

use App\Http\Controllers\FilesController;
use App\Http\Controllers\ImagesController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix(LaravelLocalization::setLocale())
     ->group(function () {
         // Files
         Route::prefix('file')
              ->as('file.')
              ->group(function () {
                  Route::get('view/{file}', [FilesController::class, 'view'])->name('view');
                  Route::get('download/{file}', [FilesController::class, 'download'])->name('download');
              });

         // Images
         Route::prefix('image')
              ->group(function () {
                  Route::get('{image}', [ImagesController::class, 'original'])->name('image');
                  Route::get('{dimensions}/{image}', [ImagesController::class, 'thumbnail'])->name('thumbnail');
              });
     });
