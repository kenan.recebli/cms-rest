<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\DB;

class FolderScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param \Illuminate\Database\Eloquent\Model   $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model): void
    {
        $builder->addSelect(
            DB::raw('(select count(*) from `folders` `f` where `folders`.`id` = `f`.`folder_id`) as `folders_count`'),
            DB::raw('(select sum(`size`) from `files` where `folders`.`id` = `files`.`folder_id`) as `files_size`')
        );
    }
}
