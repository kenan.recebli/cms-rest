<?php

namespace App\Policies;

use App\Models\Module;
use App\Policies\Concerns\CommonPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use CommonPolicy;
    use HandlesAuthorization;

    /**
     * The associated module ID.
     *
     * @var int
     */
    public int $moduleId;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->moduleId = Module::getByKey('users')->id;
    }
}
