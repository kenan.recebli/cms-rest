<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\Concerns\CommonPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use CommonPolicy;
    use HandlesAuthorization;

    /**
     * Allow all actions for the super.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function before(User $user): bool
    {
        return $user->is_developer;
    }
}
