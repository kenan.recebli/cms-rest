<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\Concerns\CommonPolicy;
use App\Policies\Concerns\StatusPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocalePolicy
{
    use CommonPolicy;
    use HandlesAuthorization;
    use StatusPolicy;

    /**
     * Allow all actions for the super.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function before(User $user): bool
    {
        return $user->is_developer;
    }
}
