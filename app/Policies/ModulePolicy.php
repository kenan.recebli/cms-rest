<?php

namespace App\Policies;

use App\Policies\Concerns\CommonPolicy;
use App\Policies\Concerns\StatusPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModulePolicy
{
    use CommonPolicy;
    use HandlesAuthorization;
    use StatusPolicy;

    /**
     * The associated module ID.
     *
     * @var string
     */
    public $moduleId;
}
