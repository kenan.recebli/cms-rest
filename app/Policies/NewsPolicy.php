<?php

namespace App\Policies;

use App\Models\Module;
use App\Policies\Concerns\PostPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use PostPolicy;
    use HandlesAuthorization;

    /**
     * The associated module ID.
     *
     * @var string
     */
    public $moduleId;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->moduleId = Module::getByKey('news')->id;
    }
}
