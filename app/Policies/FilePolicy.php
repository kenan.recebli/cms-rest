<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\Concerns\CommonPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use CommonPolicy;
    use HandlesAuthorization;

    /**
     * Allow all actions.
     *
     * @param \App\Models\User $user
     *
     * @return bool|null
     */
    public function before(User $user): ?bool
    {
        return true;
    }
}
