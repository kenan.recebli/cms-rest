<?php

namespace App\Policies;

use App\Models\Module;
use App\Models\User;
use App\Policies\Concerns\CommonPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class SettingPolicy
{
    use CommonPolicy;
    use HandlesAuthorization;

    /**
     * The associated module ID.
     *
     * @var int
     */
    public int $moduleId;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->moduleId = Module::getByKey('settings')->id;
    }

    /**
     * Determine whether the user can manage a resource.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function manage(User $user): bool
    {
        return $user->hasPermission(8, $this->moduleId);
    }
}
