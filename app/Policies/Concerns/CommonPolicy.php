<?php

namespace App\Policies\Concerns;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

trait CommonPolicy
{
    /**
     * Allow all actions for the super.
     *
     * @param \App\Models\User $user
     *
     * @return bool|null
     */
    public function before(User $user): ?bool
    {
        return $this->moduleId
            ? ($user->is_superuser ?: null)
            : $user->is_superuser;
    }

    /**
     * Determine whether the user can create a resource.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasPermission(1, $this->moduleId);
    }

    /**
     * Determine whether the user can read a resource.
     *
     * @param \App\Models\User                         $user
     * @param \Illuminate\Database\Eloquent\Model|null $model
     *
     * @return bool
     */
    public function read(User $user, Model $model = null): bool
    {
        return $user->hasPermission(2, $this->moduleId);
    }

    /**
     * Determine whether the user can update a resource.
     *
     * @param \App\Models\User                         $user
     * @param \Illuminate\Database\Eloquent\Model|null $model
     *
     * @return bool
     */
    public function update(User $user, Model $model = null): bool
    {
        return $user->hasPermission(3, $this->moduleId);
    }

    /**
     * Determine whether the user can delete a resource.
     *
     * @param \App\Models\User                         $user
     * @param \Illuminate\Database\Eloquent\Model|null $model
     *
     * @return bool
     */
    public function delete(User $user, Model $model = null): bool
    {
        return $user->hasPermission(4, $this->moduleId);
    }
}
