<?php

namespace App\Policies\Concerns;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

trait PostPolicy
{
    use CommonPolicy;

    /**
     * Determine whether the user can delete a resource.
     *
     * @param \App\Models\User                         $user
     * @param \Illuminate\Database\Eloquent\Model|null $model
     *
     * @return bool
     */
    public function trash(User $user, Model $model = null): bool
    {
        return $user->hasPermission(5, $this->moduleId);
    }

    /**
     * Determine whether the user can delete a resource.
     *
     * @param \App\Models\User                         $user
     * @param \Illuminate\Database\Eloquent\Model|null $model
     *
     * @return bool
     */
    public function restore(User $user, Model $model = null): bool
    {
        return $user->hasPermission(6, $this->moduleId);
    }
}
