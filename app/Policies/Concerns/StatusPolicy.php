<?php

namespace App\Policies\Concerns;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

trait StatusPolicy
{
    /**
     * Determine whether the user can toggle a resource.
     *
     * @param \App\Models\User                         $user
     * @param \Illuminate\Database\Eloquent\Model|null $model
     *
     * @return bool
     */
    public function toggle(User $user, Model $model = null): bool
    {
        return $user->hasPermission(7, $this->moduleId);
    }
}
