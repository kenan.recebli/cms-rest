<?php

namespace App\Policies;

use App\Models\Module;
use App\Policies\Concerns\CommonPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class SocialNetworkPolicy
{
    use CommonPolicy;
    use HandlesAuthorization;

    /**
     * The associated module ID.
     *
     * @var string
     */
    public $moduleId;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->moduleId = Module::getByKey('social-networks')->id;
    }
}
