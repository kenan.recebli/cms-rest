<?php

namespace App\Policies;

use App\Models\Module;
use App\Policies\Concerns\CommonPolicy;
use App\Policies\Concerns\StatusPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class SectionPolicy
{
    use CommonPolicy;
    use HandlesAuthorization;
    use StatusPolicy;

    /**
     * The associated module ID.
     *
     * @var string
     */
    public $moduleId;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->moduleId = Module::getByKey('sections')->id;
    }
}
