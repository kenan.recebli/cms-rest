<?php

namespace App\Extensions;

use Carbon\Carbon;

class Date extends Carbon
{
    /**
     * Get a part of the Carbon object
     *
     * @param string $name
     *
     * @return \DateTimeZone|int|string
     */
    public function __get($name)
    {
        return match ($name) {
            'date' => match (app()->getLocale()) {
                'az', 'ru', 'tr' => $this->format('j F Y'),
                default => $this->format('F j, Y'),
            },
            'dM' => match (app()->getLocale()) {
                'az', 'ru', 'tr' => $this->format('j F'),
                default => $this->format('F j'),
            },
            'dm' => match (app()->getLocale()) {
                'az', 'ru', 'tr' => $this->format('j M'),
                default => $this->format('M j'),
            },
            'iso8601' => $this->format(static::ATOM),
            default => parent::__get($name),
        };
    }

    /**
     * Returns date formatted according to given format.
     *
     * @param string $format
     *
     * @return string
     */
    public function format($format): string
    {
        $replace = [];

        for ($i = 0, $len = strlen($format); $i < $len; ++$i) {
            $character = $format[$i];

            if (in_array($character, ['D', 'l', 'F', 'M'])) {
                if ($i > 0 && $format[$i - 1] == '\\') {
                    continue;
                }

                $key = match ($character) {
                    'D' => parent::format('l'),
                    'M' => parent::format('F'),
                    default => parent::format($character),
                };

                $original = parent::format($character);

                if (in_array($character, ['F', 'M'])) {
                    $translated = trans_choice(
                        'datetime.'.$key,
                        (($i - 2) >= 0 && in_array($format[$i - 2], ['d', 'j'])) ? 1 : 0
                    );
                } else {
                    $translated = __('datetime.'.$key);
                }

                if (in_array($character, ['D', 'M'])) {
                    $shortTranslated = __('datetime.'.$original);

                    $translated = $shortTranslated === $original
                        ? mb_substr($translated, 0, 3)
                        : $shortTranslated;
                }

                if ($translated && $original != $translated) {
                    $replace[$original] = $translated;
                }
            }
        }

        return $replace
            ? str_replace(array_keys($replace), array_values($replace), parent::format($format))
            : parent::format($format);
    }
}
