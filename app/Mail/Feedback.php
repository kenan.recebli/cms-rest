<?php

namespace App\Mail;

use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class Feedback extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Build the message.
     *
     * @return static|void
     */
    public function build()
    {
        if ($email = Setting::key('emails')[0]) {
            Config::set('app.name', $siteName = Setting::key('site_name'));

            $request = request();

            return $this->to($email)
                        ->from('no-reply@delicates.az', "$request->name | $request->email")
                        ->subject($siteName)
                        ->markdown('emails.feedback')
                        ->with(['message' => $request->message]);
        }
    }
}
