<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderProduct extends Pivot
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_name',
        'quantity',
        'with_discount',
        'discount_amount',
        'discount_unit',
        'price',
        'gain',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'product_name' => 'array',
        'with_discount' => 'bool',
    ];

    /**
     * Get product name.
     *
     * @return string
     */
    public function getNameAttribute(): string
    {
        return $this->product_name[app()->getLocale()] ?? $this->product_name[config('app.fallback_locale')];
    }
}
