<?php

namespace App\Models;

use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\CanBeSeen;
use App\Models\Concerns\ListsResources;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class Feedback extends BaseModel
{
    use BatchDeletes;
    use CanBeSeen;
    use ListsResources;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'message',
        'seen_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'seen_at',
    ];

    /**
     * List feedback.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function listForAdmin(Request $request): LengthAwarePaginator
    {
        return static::latest()->paginate($request->itemsPerPage ?: 10);
    }
}
