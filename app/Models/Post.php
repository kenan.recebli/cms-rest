<?php

namespace App\Models;

use App\Models\Concerns\Postable;

class Post extends BaseModel
{
    use Postable;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_featured' => 'bool',
    ];
}
