<?php

namespace App\Models;

use App\Models\Concerns\HasCategory;
use App\Models\Concerns\HasImage;
use App\Models\Concerns\HasPosts;
use App\Models\Concerns\HasSlug;
use App\Models\Concerns\HasStatus;
use App\Models\Concerns\HasUrl;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Orderable;
use App\Models\Concerns\Translatable;
use App\Scopes\StatusScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends BaseModel
{
    use HasCategory;
    use HasImage;
    use HasPosts;
    use HasSlug;
    use HasStatus;
    use HasUrl;
    use ListsResources;
    use Orderable;
    use Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'slug',
        'name',
        'description',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category',
        'image',
        'is_active',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'bool',
    ];

    /**
     * The relationships that should be eager loaded.
     *
     * @var string[]
     */
    protected static array $listWith = [
        // 'categories',
    ];

    /**
     * Indicates if the listing should be sorted.
     *
     * @var bool
     */
    protected static bool $sort = false;

    /**
     * Get the categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories(): HasMany
    {
        return $this->hasMany(static::class);
    }

    /**
     * Get the posts of the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Get categories.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    /*public static function list()
    {
        return static::withCount('posts')->get();
    }*/

    /**
     * Filter list.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public static function filterListForAdmin(Builder $query): void
    {
        $query->whereNull('category_id');
    }

    /**
     * Get children of the category for the admin.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getChildrenForAdmin()
    {
        return $this->categories()
                    ->withoutGlobalScope(StatusScope::class)
                    ->get();
    }

    /**
     * Get children of the category.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getChildren()
    {
        return $this->categories()
                    ->get();
    }

    /**
     * Get parent categories.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getParents()
    {
        return static::with('categories')
                     ->whereNull('category_id')
                     ->get();
    }
}
