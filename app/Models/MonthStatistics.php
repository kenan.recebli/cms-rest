<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MonthStatistics extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'month',
        'visitors',
        'views',
    ];

    /**
     * Get the month statistics.
     *
     * @param \Carbon\Carbon $date
     *
     * @return static|null
     */
    public static function fetch(Carbon $date): ?static
    {
        return static::where('month', $date->format('Ym'))->first();
    }

    /**
     * Get the month statistics by date range.
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     *
     * @return static[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getByRange(Carbon $start, Carbon $end)
    {
        return static::whereBetween('month', [$start->format('Ym'), $end->format('Ym')])->get();
    }
}
