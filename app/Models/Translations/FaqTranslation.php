<?php

namespace App\Models\Translations;

use App\Models\Concerns\HasLocale;
use App\Models\Concerns\Sluggable;
use Illuminate\Database\Eloquent\Model;

class FaqTranslation extends Model
{
    use HasLocale;
    use Sluggable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question',
        'answer',
    ];
}
