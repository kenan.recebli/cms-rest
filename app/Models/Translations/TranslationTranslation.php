<?php

namespace App\Models\Translations;

use App\Models\Concerns\HasLocale;
use Illuminate\Database\Eloquent\Model;

class TranslationTranslation extends Model
{
    use HasLocale;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
    ];
}
