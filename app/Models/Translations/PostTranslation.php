<?php

namespace App\Models\Translations;

use App\Models\Concerns\HasLocale;
use App\Models\Concerns\HasVideo;
use App\Models\Concerns\Sluggable;
use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    use HasLocale;
    use HasVideo;
    use Sluggable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'content',
        'keywords',
        'video',
    ];
}
