<?php

namespace App\Models;

use App\Models\Concerns\HasKey;
use App\Models\Concerns\HasStatus;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Orderable;

class SocialNetwork extends BaseModel
{
    use HasKey;
    use HasStatus;
    use ListsResources;
    use Orderable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'name',
        'icon',
        'link',
        'is_active',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'bool',
    ];

    /**
     * Indicates if the listing should be sorted.
     *
     * @var bool
     */
    protected static bool $sort = false;

    /**
     * Change social networks.
     *
     * @param array $networks
     *
     * @return array
     */
    public static function change(array $networks): array
    {
        $success = true;

        foreach ($networks as $key => $value) {
            if ($resource = static::getByKey($key)) {
                if (! $resource->update(['link' => $value])) {
                    $success = false;
                }
            }
        }

        return compact('success');
    }
}
