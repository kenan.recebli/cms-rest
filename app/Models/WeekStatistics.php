<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WeekStatistics extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'week',
        'visitors',
        'views',
    ];

    /**
     * Get the week statistics.
     *
     * @param \Carbon\Carbon $date
     *
     * @return static|null
     */
    public static function fetch(Carbon $date): ?WeekStatistics
    {
        return static::where('week', $date->format('YW'))->first();
    }
}
