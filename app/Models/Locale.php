<?php

namespace App\Models;

use App\Models\Concerns\HasCode;
use App\Models\Concerns\HasObserver;
use App\Models\Concerns\HasStatus;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Orderable;
use Illuminate\Database\Eloquent\Collection;

class Locale extends BaseModel
{
    use HasCode;
    use HasObserver;
    use HasStatus;
    use ListsResources;
    use Orderable;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'script',
        'native',
        'regional',
        'is_active',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'bool',
    ];

    /**
     * Indicates if the listing should be sorted.
     *
     * @var bool
     */
    protected static bool $sort = false;

    /**
     * Get raw locales.
     *
     * @return \App\Models\Locale[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public static function raw(): array|Collection|\Illuminate\Support\Collection
    {
        return static::all()
                     ->groupBy('id')
                     ->map(function (Collection $locales) {
                         return $locales->first()
                                        ->only([
                                            'code',
                                            'name',
                                            'script',
                                            'native',
                                            'regional',
                                        ]);
                     });
    }

    /**
     * Get locales as JSON.
     *
     * @return string
     */
    public static function getJson(): string
    {
        return static::raw()->toJson();
    }
}
