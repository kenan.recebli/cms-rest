<?php

namespace App\Models;

use App\Models\Concerns\HasMenu;
use App\Models\Concerns\HasSlug;
use App\Models\Concerns\HasStatus;
use App\Models\Concerns\HasUrl;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Orderable;
use App\Models\Concerns\Translatable;
use App\Scopes\StatusScope;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Menu extends BaseModel
{
    use HasMenu;
    use HasSlug;
    use HasStatus;
    use HasUrl;
    use ListsResources;
    use Orderable;
    use Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'slug',
        'name',
        'link',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'menu',
        'is_external',
        'is_active',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_external' => 'bool',
        'is_active' => 'bool',
    ];

    /**
     * Get the menus.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menus(): HasMany
    {
        return $this->hasMany(static::class);
    }

    /**
     * List resources for admin.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function listForAdmin()
    {
        return static::with('menus')
                     ->withoutGlobalScope(StatusScope::class)
                     ->whereNull('menu_id')
                     ->get();
    }

    /**
     * Get children of the menu for admin.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getChildrenForAdmin(): Collection
    {
        return $this->menus()
                    ->withoutGlobalScope(StatusScope::class)
                    ->get();
    }

    /**
     * Get children of the menu.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getChildren(): Collection
    {
        return $this->menus()
                    ->get();
    }

    /**
     * Get parent menus.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getParents()
    {
        return static::with('menus')
                     ->whereNull('menu_id')
                     ->get();
    }
}
