<?php

namespace App\Models;

use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\HasKey;
use App\Models\Concerns\HasStatus;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Role extends BaseModel
{
    use BatchDeletes;
    use HasKey;
    use HasStatus;
    use ListsResources;
    use Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected array $translatedAttributes = [
        'slug',
        'name',
        'description',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'is_admin',
        'is_active',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'bool',
        'is_active' => 'bool',
    ];

    /**
     * Get the permissions of the role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Check if the role is super.
     *
     * @return bool
     */
    public function getIsSuperroleAttribute(): bool
    {
        return in_array($this->key, ['developer', 'ceo']);
    }

    /**
     * Check if the role is not super.
     *
     * @return bool
     */
    public function getNotSuperroleAttribute(): bool
    {
        return ! $this->is_superrole;
    }

    /**
     * Check if the role allowed to be changed by the current user.
     *
     * @return bool
     */
    public function allowed(): bool
    {
        return $this->not_superrole && Auth::user()?->is_superuser;
    }

    /**
     * Filter list.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public static function filterListForAdmin(Builder $query): void
    {
        $query->whereNotIn('key', ['developer', 'ceo']);
    }

    /**
     * Get all roles.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll(Request $request): Collection|array
    {
        $keys = [];
        $user = $request->user();

        if (! $user->is_developer) {
            $keys[] = 'developer';
        }

        if (! $user->is_superuser) {
            $keys[] = 'ceo';
        }

        return static::whereNotIn('key', $keys)->get();
    }
}
