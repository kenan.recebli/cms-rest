<?php

namespace App\Models;

use App\Models\Concerns\HasFolder;
use App\Models\Concerns\HasGlobalScope;
use App\Models\Concerns\ListsOrphans;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Folder extends BaseModel
{
    use HasFolder;
    use HasGlobalScope;
    use ListsOrphans;
    use SoftDeletes;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'folders',
    ];

    /**
     * The relationship counts that should be eager loaded on every query.
     *
     * @var array
     */
    protected $withCount = [
        'files',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'folder',
        'name',
    ];

    /**
     * Get the folders that are belongs to the folder.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function folders(): HasMany
    {
        return $this->hasMany(static::class)->latest();
    }

    /**
     * Get the files that are belongs to the folder.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files(): HasMany
    {
        return $this->hasMany(File::class)->latest();
    }

    /**
     * Count folders in the folder.
     *
     * @return int
     */
    public function countFolders(): int
    {
        $count = (int) $this->folders_count;

        foreach ($this->folders as $folder) {
            $count += $folder->folders->isEmpty()
                ? $folder->folders_count
                : $folder->countFolders();
        }

        return $count;
    }

    /**
     * Count files in the folder.
     *
     * @return int
     */
    public function countFiles(): int
    {
        $count = (int) $this->files_count;

        foreach ($this->folders as $folder) {
            $count += $folder->folders->isEmpty()
                ? $folder->files_count
                : $folder->countFiles();
        }

        return $count;
    }

    /**
     * Calculate size of files in the folder.
     *
     * @return int
     */
    public function calculateFilesSize(): int
    {
        $size = (int) $this->files_size;

        foreach ($this->folders as $folder) {
            $size += $folder->folders->isEmpty()
                ? $folder->files_size
                : $folder->calculateFilesSize();
        }

        return $size;
    }
}
