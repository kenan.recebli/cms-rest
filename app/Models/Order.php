<?php

namespace App\Models;

use App\Models\Concerns\HasObserver;
use App\Models\Concerns\HasPermanentUser;
use App\Models\Concerns\HasTimestamps;
use App\Models\Concerns\HasUser;
use App\Models\Concerns\ListsResources;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends BaseModel
{
    use HasPermanentUser;
    use HasObserver;
    use HasTimestamps;
    use HasUser;
    use ListsResources;

    /**
     * Payment type is a cache.
     *
     * @var int
     */
    public static int $CACHE = 1;

    /**
     * Payment type is a Kapital.
     *
     * @var int
     */
    public static int $KAPITAL = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'payment_type',
        'note',
        'address',
        'order_id',
        'session_id',
        'quantity',
        'discount',
        'gain',
        'has_discount',
        'status',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'has_discount' => 'bool',
    ];

    /**
     * The relationships that should be eager loaded.
     *
     * @var string[]
     */
    protected static array $listWith = [
        'user',
    ];

    /**
     * Get the ordered products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class)
                    ->using(OrderProduct::class)
                    ->withPivot([
                        'product_name',
                        'quantity',
                        'with_discount',
                        'discount_amount',
                        'discount_unit',
                        'price',
                        'gain',
                        'user_gain',
                    ]);
    }

    /**
     * Set payment type.
     *
     * @param int $value
     *
     * @return void
     */
    public function setPaymentTypeAttribute(int $value): void
    {
        $this->attributes['payment_type'] = $value;

        if ($value != static::$CACHE) {
            $this->attributes['status'] = 0;
        }
    }

    /**
     * Get payment type.
     *
     * @param int $value
     *
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getPaymentTypeAttribute($value): mixed
    {
        return match ($value) {
            1 => __('app.cache'),
            2 => __('app.kapital'),
            default => null,
        };
    }

    /**
     * Get order status.
     *
     * @param int $value
     *
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getStatusAttribute(int $value): array|string|Translator|Application|null
    {
        return match ($value) {
            0 => __('Processing'),
            1 => __('Accepted'),
            2 => __('Canceled'),
            3 => __('Declined'),
            4 => __('Completed'),
        };
    }

    /**
     * Get the URL for the model.
     *
     * @return string
     */
    public function getUrlAttribute(): string
    {
        return route('orders.show', [
            'order' => $this->id,
        ]);
    }

    /**
     * Set payment data.
     *
     * @param string $orderId
     * @param string $sessionId
     *
     * @return bool
     */
    public function setPaymentData(string $orderId, string $sessionId): bool
    {
        return $this->update([
            'order_id' => $orderId,
            'session_id' => $sessionId,
        ]);
    }

    /**
     * Get the order.
     *
     * @param string $orderId
     * @param string $sessionId
     *
     * @return static|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public static function get($orderId, $sessionId)
    {
        return static::firstWhere([
            'order_id' => $orderId,
            'session_id' => $sessionId,
        ]);
    }

    /**
     * Mark order as unprocessed.
     *
     * @return bool
     */
    public function markAsUnprocessed(): bool
    {
        return $this->updateStatus(0);
    }

    /**
     * Mark order as succeed.
     *
     * @return bool
     */
    public function markAsSucceed(): bool
    {
        return $this->updateStatus(1);
    }

    /**
     * Mark order as canceled.
     *
     * @return bool
     */
    public function markAsCanceled(): bool
    {
        return $this->updateStatus(2);
    }

    /**
     * Mark order as declined.
     *
     * @return bool
     */
    public function markAsDeclined(): bool
    {
        return $this->updateStatus(3);
    }

    /**
     * Update order status.
     *
     * @param int $status
     *
     * @return bool
     */
    private function updateStatus(int $status): bool
    {
        return $this->update(compact('status'));
    }
}
