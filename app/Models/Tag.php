<?php

namespace App\Models;

use App\Models\Concerns\HasLocale;
use App\Models\Concerns\HasPosts;
use App\Models\Concerns\HasSlug;
use App\Models\Concerns\HasUrl;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Sluggable;
use Illuminate\Http\Request;

class Tag extends BaseModel
{
    use HasLocale;
    use HasPosts;
    use HasSlug;
    use HasUrl;
    use ListsResources;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'locale',
        'slug',
        'name',
    ];

    /**
     * Get the posts of the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->morphedByMany(News::class, 'taggable');
    }

    /**
     * List tags.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    /*public static function list(): LengthAwarePaginator
    {
        return static::latest()->paginate(10);
    }*/

    /**
     * Look for the tags.
     *
     * @param string $name
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function search(Request $request)
    {
        if (! $request->q) {
            return [];
        }

        $query = static::where('locale_id', $request->get('locale'))
                       ->where('name', 'like', "%$request->q%");

        if ($tags = $request->tags) {
            $query->whereNotIn('id', $tags);
        }

        return $query->take(5)->get();
    }

    /**
     * Get the item by its slug.
     *
     * @param string $slug
     *
     * @return static|null
     */
    public static function getBySlug(string $slug)
    {
        return static::where('slug', $slug)
                     ->where('locale_id', app()->getLocale())
                     ->first();
    }
}
