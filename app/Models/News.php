<?php

namespace App\Models;

use App\Models\Concerns\Postable;

class News extends BaseModel
{
    use Postable;
}
