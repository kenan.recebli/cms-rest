<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DayStatistics extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'day',
        'visitors',
        'views',
    ];

    /**
     * Get the day statistics.
     *
     * @param \Carbon\Carbon $date
     *
     * @return static|null
     */
    public static function fetch(Carbon $date): ?static
    {
        return static::where('day', $date)->first();
    }

    /**
     * Get the day statistics by date range.
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     *
     * @return static[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getByRange(Carbon $start, Carbon $end)
    {
        return static::whereBetween('day', [$start, $end])->get();
    }
}
