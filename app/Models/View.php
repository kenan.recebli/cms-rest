<?php

namespace App\Models;

use App\Models\Concerns\HasObserver;
use Carbon\Carbon;

class View extends BaseModel
{
    use HasObserver;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Count views by date.
     *
     * @param \Carbon\Carbon $date
     *
     * @return int
     */
    public static function countByDate(Carbon $date): int
    {
        return static::whereDate('created_at', $date->toDateString())->count();
    }
}
