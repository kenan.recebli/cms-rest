<?php

namespace App\Models;

use App\Models\Concerns\HasImage;
use App\Models\Concerns\HasStatus;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Orderable;

class Link extends BaseModel
{
    use HasImage;
    use HasStatus;
    use ListsResources;
    use Orderable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image',
        'name',
        'url',
        'is_active',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'bool',
    ];

    /**
     * Indicates if the listing should be sorted.
     *
     * @var bool
     */
    protected static bool $sort = false;
}
