<?php

namespace App\Models;

use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\Cacheable;
use App\Models\Concerns\ClearsTable;
use App\Models\Concerns\ListsResources;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Permission extends BaseModel
{
    use BatchDeletes;
    use Cacheable;
    use ClearsTable;
    use ListsResources;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'module',
        'action',
    ];

    /**
     * Actions.
     *
     * @var array
     */
    public static array $actions = [
        1 => 'create',
        2 => 'read',
        3 => 'update',
        4 => 'delete',
        5 => 'trash',
        6 => 'restore',
        7 => 'toggle',
        8 => 'manage',
    ];

    /**
     * The relationships that should be eager loaded.
     *
     * @var string[]
     */
    protected static array $listWith = [
        'module',
    ];

    /**
     * Get the module for the permission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module(): BelongsTo
    {
        return $this->belongsTo(Module::class)
                    ->withoutGlobalScopes()
                    ->select([
                        'id',
                        'key',
                    ]);
    }

    /**
     * Get the roles for the permission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Set the moduel for the permission.
     *
     * @param int $value
     *
     * @return void
     */
    public function setModuleAttribute(int $value): void
    {
        $this->attributes['module_id'] = $value;
    }

    /**
     * Set the action for the permission.
     *
     * @param int|string $value
     *
     * @return void
     */
    public function setActionAttribute(int|string $value): void
    {
        $this->attributes['action'] = is_numeric($value)
            ? $value
            : static::getActionCode($value);
    }

    /**
     * Get the action name.
     *
     * @return ?string
     */
    public function getActionNameAttribute(): ?string
    {
        return static::$actions[$this->action];
    }

    /**
     * Get the permission.
     *
     * @param int $action
     * @param int $moduleId
     *
     * @return \App\Models\Permission|null
     */
    public static function getByKey(int $action, int $moduleId): ?static
    {
        return static::firstWhere([
            'action' => $action,
            'module_id' => $moduleId,
        ]);
    }

    /**
     * Get actions.
     *
     * @return string
     */
    public static function getActions(): string
    {
        return implode(',', static::$actions);
    }

    /**
     * Get the code of the action.
     *
     * @param $actionName
     *
     * @return false|int|string
     */
    public static function getActionCode($actionName): bool|int|string
    {
        return array_search($actionName, static::$actions);
    }
}
