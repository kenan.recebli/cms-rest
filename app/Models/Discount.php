<?php

namespace App\Models;

class Discount extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'unit',
        'start',
        'end',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start',
        'end',
    ];

    /**
     * Get discount value.
     *
     * @return string
     */
    public function getValueAttribute()
    {
        return $this->amount.($this->unit == 1 ? ' ₼' : '%');
    }
}
