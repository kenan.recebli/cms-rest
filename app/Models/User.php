<?php

namespace App\Models;

use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\HasRoles;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\SortsResources;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

// use Laravel\Jetstream\HasProfilePhoto;

class User extends Authenticatable
{
    use BatchDeletes;
    use HasApiTokens;
    use HasFactory;
    use HasRoles;
    use ListsResources;
    use Notifiable;
    use SortsResources;

    // use HasProfilePhoto;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The relationships that should be eager loaded.
     *
     * @var string[]
     */
    protected static array $listWith = [];

    /**
     * The relationships that should be counted.
     *
     * @var string[]
     */
    protected static array $listWithCount = [
        'roles',
    ];

    /**
     * Get the social accounts that are belong to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socialAccounts(): HasMany
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * Set the password for the user.
     *
     * @param string $value
     *
     * @return void
     */
    public function setPasswordAttribute(string $value): void
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Get the fullname of the user.
     *
     * @return string
     */
    public function getFullnameAttribute(): string
    {
        return "$this->first_name $this->last_name";
    }

    /**
     * Check if the user has the given social account.
     *
     * @param int|string $accountId
     *
     * @return bool
     */
    public function hasSocialAccount(int|string $accountId): bool
    {
        return $this->socialAccounts()
                    ->where('account_id', $accountId)
                    ->exists();
    }

    /**
     * Get the user by E-mail.
     *
     * @param string $email
     *
     * @return static|null
     */
    public static function getByEmail(string $email): ?User
    {
        return static::firstWhere('email', $email);
    }

    /**
     * Filter list.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return void
     */
    public static function filterListForAdmin(Builder $query, Request $request): void
    {
        $query->whereDoesntHave('roles', function (Builder $query) use ($request) {
            $keys = [];
            $user = $request->user();

            if (! $user->is_developer) {
                $keys[] = 'developer';
            }

            if (! $user->is_superuser) {
                $keys[] = 'ceo';
            }

            $query->whereIn('key', $keys);
        });
    }
}
