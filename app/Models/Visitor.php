<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class Visitor extends EloquentModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'session',
        'platform',
        'browser',
        'version',
        'ip',
        'created_at',
    ];

    /**
     * Get online visitors count.
     *
     * @return int
     */
    public static function online(): int
    {
        return static::where('visited_at', '>', Carbon::now()->subMinutes(3))->count();
    }

    /**
     * Count visitors count by date.
     *
     * @param \Carbon\Carbon $date
     *
     * @return int
     */
    public static function countByDate(Carbon $date): int
    {
        return static::where('created_at', $date->toDateString())->count();
    }
}
