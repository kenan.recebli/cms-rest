<?php

namespace App\Models;

use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\HasObserver;
use App\Models\Concerns\HasStatus;
use App\Models\Concerns\ListsResources;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;

class Module extends BaseModel
{
    use BatchDeletes;
    use HasObserver;
    use HasStatus;
    use ListsResources;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'is_active',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'bool',
    ];

    /**
     * Get the permissions for the module.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions(): HasMany
    {
        return $this->hasMany(Permission::class);
    }

    /**
     * Get the module.
     *
     * @param string $key
     *
     * @return static|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public static function getByKey(string $key)
    {
        return Cache::rememberForever("module.$key", function () use ($key) {
            return static::select('id')->firstWhere('key', $key);
        });
    }

    /**
     * Get all permissions.
     *
     * @param int $roleId
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getPermissions(int $roleId): Collection|array
    {
        return static::has('permissions')
                     ->with([
                         'permissions.roles' => function (BelongsToMany $query) use ($roleId) {
                             $query->where('id', $roleId);
                         },
                     ])
                     ->get();
    }
}
