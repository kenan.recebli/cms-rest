<?php

namespace App\Models\Concerns;

trait GetsFullname
{
    /**
     * Get fullname.
     *
     * @return string
     */
    public function getFullnameAttribute()
    {
        return "{$this->last_name} {$this->first_name} {$this->patronymic}";
    }
}
