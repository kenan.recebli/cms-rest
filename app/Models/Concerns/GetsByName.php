<?php

namespace App\Models\Concerns;

trait GetsByName
{
    /**
     * Gets resource by name.
     *
     * @param string $name
     *
     * @return ?static
     */
    public static function getByName(string $name): ?static
    {
        return static::firstWhere('name', $name);
    }
}
