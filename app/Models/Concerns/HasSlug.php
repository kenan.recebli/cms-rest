<?php

namespace App\Models\Concerns;

trait HasSlug
{
    /**
     * Get the item by its slug.
     *
     * @param string      $slug
     * @param string|null $locale
     *
     * @return static|null
     */
    public static function getBySlug(string $slug, string $locale = null): ?static
    {
        $query = static::whereTranslation('slug', $slug, $locale ?: app()->getLocale());

        $withCount = [];

        if (method_exists(get_called_class(), 'views')) {
            $withCount[] = 'views';
        }

        if ($withCount) {
            $query->withCount($withCount);
        }

        return $query->first();
    }
}
