<?php

namespace App\Models\Concerns;

use App\Http\Resources\Admin\TagItems;
use App\Models\Tag;
use App\Observers\TaggableObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait Taggable
{
    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootTaggable(): void
    {
        static::observe(new TaggableObserver);
    }

    /**
     * Get the tags of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags(): MorphToMany
    {
        return $this->allTags()
                    ->where('locale_id', app()->getLocale());
    }

    /**
     * Get the tags of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function allTags(): MorphToMany
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get the tag items for the resource.
     *
     * @return array
     */
    public function getTagItemsAttribute(): array
    {
        $tags = [];

        foreach ($this->allTags->groupBy('locale_id') as $locale => $tag) {
            $tags[$locale] = TagItems::collection($tag);
        }

        foreach (config('translatable.locales') as $locale) {
            if (! array_key_exists($locale, $tags)) {
                $tags[$locale] = [];
            }
        }

        return $tags;
    }

    /**
     * Set the tags for the resource.
     *
     * @param array $tags
     *
     * @return void
     */
    public function syncTags(array $tags): void
    {
        $data = [];

        foreach ($tags as $locale => $tag) {
            $data = array_merge($data, array_column($tag, 'id'));

            foreach ($tag as $name) {
                if (is_string($name)) {
                    $data[] = Tag::create(compact('locale', 'name'))->id;
                }
            }
        }

        $this->allTags()->sync($data);
    }

    /**
     * Get the related posts by tags.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByTags()
    {
        return static::where('id', '!=', $this->id)
                     ->whereHas('tags', function (Builder $query) {
                         $query->whereIn('id', $this->tags->pluck('id'));
                     })
                     ->get();
    }
}
