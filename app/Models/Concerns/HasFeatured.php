<?php

namespace App\Models\Concerns;

trait HasFeatured
{
    /**
     * Get featured posts.
     *
     * @param int $limit
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getFeatured(int $limit = 10)
    {
        return static::with('section')
                     ->where('is_featured', true)
                     ->latest()
                     ->limit($limit)
                     ->get();
    }
}
