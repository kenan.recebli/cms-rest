<?php

namespace App\Models\Concerns;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\UploadedFile;

trait HasAvatar
{
    /**
     * Set the avatar for user.
     *
     * @param \Illuminate\Http\UploadedFile|null $file
     *
     * @return void
     */
    public function setAvatarAttribute(UploadedFile $file = null): void
    {
        if ($file && $file->isValid()) {
            $file->storeAs('', $name = unique_name().'.'.$file->getClientOriginalExtension(), 'avatars');

            $this->attributes['avatar'] = $name;
        }
    }

    /**
     * Get the avatar for user.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\UrlGenerator|string|null
     */
    public function getAvatarUrlAttribute(): string|UrlGenerator|Application|null
    {
        return $this->avatar
            ? url("avatars/$this->avatar")
            : null;
    }
}
