<?php

namespace App\Models\Concerns;

trait HasObserver
{
    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootHasObserver(): void
    {
        static::observe(new (substr_replace(static::class, 'App\Observers', 0, 10).'Observer'));
    }
}
