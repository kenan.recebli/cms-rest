<?php

namespace App\Models\Concerns;

use App\Scopes\StatusScope;

trait HasStatus
{
    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootHasStatus(): void
    {
        static::addGlobalScope(new StatusScope);
    }

    /**
     * Activate the resource.
     *
     * @return bool
     */
    public function activate(): bool
    {
        return $this->update([
            'is_active' => true,
        ]);
    }

    /**
     * Deactivate the resource.
     *
     * @return bool
     */
    public function deactivate(): bool
    {
        return $this->update([
            'is_active' => false,
        ]);
    }

    /**
     * Toggle the resource.
     *
     * @return bool
     */
    public function toggle(): bool
    {
        return $this->update([
            'is_active' => ! $this->is_active,
        ]);
    }
}
