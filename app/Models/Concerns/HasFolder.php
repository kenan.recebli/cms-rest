<?php

namespace App\Models\Concerns;

use App\Models\Folder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasFolder
{
    /**
     * Get the folder that owns the file.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function folder(): BelongsTo
    {
        return $this->belongsTo(Folder::class);
    }

    /**
     * Set the file for the resource.
     *
     * @param int|null $value
     *
     * @return void
     */
    public function setFolderAttribute(int $value = null): void
    {
        $this->attributes['folder_id'] = $value;
    }
}
