<?php

namespace App\Models\Concerns;

use Illuminate\Contracts\Translation\Translator;

trait HasTimeago
{
    /**
     * Get the time difference for humans.
     *
     * @return \Illuminate\Contracts\Translation\Translator|string
     */
    public function getTimeagoAttribute(): string|Translator
    {
        return timeago($this->created_at);
    }
}
