<?php

namespace App\Models\Concerns;

trait ListsOrphans
{
    /**
     * List files & folders with no parent folder.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function listOrphans()
    {
        return static::whereNull('folder_id')
                     ->latest()
                     ->get();
    }
}