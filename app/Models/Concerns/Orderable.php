<?php

namespace App\Models\Concerns;

use App\Observers\OrderingObserver;
use App\Scopes\OrderingScope;

trait Orderable
{
    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootOrderable(): void
    {
        static::observe(new OrderingObserver);
        static::addGlobalScope(new OrderingScope);
    }

    /**
     * Order resources.
     *
     * @param int $order
     *
     * @return bool
     */
    public function order(int $order): bool
    {
        $this->fillable[] = 'order';

        return $this->update(compact('order'));
    }

    /**
     * Get the last order.
     *
     * @return int
     */
    public function lastOrder(): int
    {
        return static::max('order') ?: 0;
    }
}
