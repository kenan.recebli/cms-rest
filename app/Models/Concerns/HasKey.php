<?php

namespace App\Models\Concerns;

use App\Observers\HasKeyObserver;

trait HasKey
{
    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootHasKey(): void
    {
        static::observe(new HasKeyObserver);
    }

    /**
     * Get the section ID by its key.
     *
     * @param string $key
     *
     * @return static|null
     */
    public static function getByKey(string $key): ?static
    {
        return static::firstWhere('key', $key);
    }
}
