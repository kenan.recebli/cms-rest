<?php

namespace App\Models\Concerns;

trait HasCode
{
    /**
     * Set the ID for the locale.
     *
     * @param string $value
     *
     * @return void
     */
    public function setCodeAttribute(string $value): void
    {
        $this->attributes['id'] = strtolower($value);
        $this->attributes['code'] = strtoupper($value);
    }
}
