<?php

namespace App\Models\Concerns;

use App\Http\Resources\Admin\FileCollection;
use App\Models\File;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait HasImage
{
    /**
     * Get the image of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image(): HasOne
    {
        return $this->hasOne(File::class, 'id', 'image_id');
    }

    /**
     * Set the image for the resource.
     *
     * @param array|null $value
     *
     * @return void
     */
    public function setImageAttribute(?array $value): void
    {
        $this->attributes['image_id'] = $value ? $value['id'] : null;
    }

    /**
     * Get the image item for the resource.
     *
     * @return \App\Http\Resources\Admin\FileCollection
     */
    public function getImageItemAttribute(): FileCollection
    {
        return new FileCollection($this->image);
    }
}
