<?php

namespace App\Models\Concerns;

trait HasPermanentUser
{
    /**
     * Get the fullname of the user.
     *
     * @return string
     */
    public function getFullnameAttribute(): string
    {
        return $this->user?->fullname ?: "$this->first_name $this->last_name";
    }
}
