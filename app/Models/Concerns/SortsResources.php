<?php

namespace App\Models\Concerns;

use BadMethodCallException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait SortsResources
{
    /**
     * Sort resources.
     *
     * @param \Illuminate\Http\Request              $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public static function sort(Request $request, Builder $query): void
    {
        if ($request->sortBy) {
            $sortDesc = $request->sortDesc;

            foreach ($request->sortBy as $i => $key) {
                $orderBy = $sortDesc[$i] == 'true' ? 'desc' : 'asc';

                switch ($key) {
                    case 'name':
                    case 'title':
                        try {
                            $query->orderByTranslation($key, $orderBy);
                        } catch (BadMethodCallException) {
                            $query->orderBy($key, $orderBy);
                        }
                        break;

                    case 'fullname':
                        $query->orderBy('last_name', $orderBy)
                              ->orderBy('first_name', $orderBy)
                              ->orderBy('patronymic', $orderBy);
                        break;

                    /*case 'company':
                        static::sortByRelationship($query, $key, $orderBy);
                        break;*/

                    case 'city':
                    case 'country':
                    case 'category':
                        static::sortByTranslatedRelationship($query, $key, $orderBy);
                        break;

                    default:
                        $query->orderBy($key, $orderBy);
                }
            }
        } else {
            $query->latest();
        }
    }

    /**
     * Sort by relationship.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $key
     * @param string                                $orderBy
     *
     * @return void
     */
    public static function sortByRelationship(
        Builder $query, string $key, string $orderBy, string $table = null
    )
    {
        $table = $table ?: Str::plural($key);

        $query->leftJoin($table, function (JoinClause $join) use ($key, $table) {
            $join->on("$table.id", "{$key}_id");
        })->orderBy('name', $orderBy);
    }

    /**
     * Sort by translated relationship.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $key
     * @param string                                $orderBy
     * @param string|null                           $table
     *
     * @return void
     */
    public static function sortByTranslatedRelationship(
        Builder $query, string $key, string $orderBy, string $table = null
    )
    {
        $translations = $table
            ? Str::singular($table).'_translations'
            : "{$key}_translations";

        $parentKey = $table
            ? Str::singular($table).'_id'
            : "{$key}_id";

        $table = $table ?: Str::plural($key);
        $self = $query->getModel()->getTable();

        $query->leftJoin(
            $table,
            function (JoinClause $join) use ($key, $table, $self, $translations, $parentKey) {
                $join->on("$table.id", "$self.{$key}_id")
                     ->leftJoin(
                         $translations,
                         function (JoinClause $join) use ($table, $translations, $parentKey) {
                             $join->on("$translations.$parentKey", "$table.id")
                                  ->where("$translations.locale_id", app()->getLocale());
                         }
                     );
            }
        )->orderBy("$translations.name", $orderBy);
    }
}
