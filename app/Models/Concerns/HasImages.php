<?php

namespace App\Models\Concerns;

use App\Http\Resources\Admin\FileCollection;
use App\Models\File;
use App\Observers\GalleryObserver;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Collection;

trait HasImages
{
    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootHasImages(): void
    {
        static::observe(new GalleryObserver);
    }

    /**
     * Get the images of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function images(): MorphToMany
    {
        return $this->morphToMany(File::class, 'gallery', null, null, 'image_id');
    }

    /**
     * Get URL addresses of the images.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getImagesUrlsAttribute(): Collection
    {
        return $this->images->map(function (File $file) {
            return $file->item;
        });
    }

    /**
     * Set the images for the resource.
     *
     * @param array $images
     *
     * @return void
     */
    public function syncImages(array $images): void
    {
        $this->images()->sync(array_column($images, 'id'));
    }

    /**
     * Get the image items for the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getImageItemsAttribute(): AnonymousResourceCollection
    {
        return FileCollection::collection($this->images);
    }
}
