<?php

namespace App\Models\Concerns;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasMenu
{
    /**
     * Get the menu of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class);
    }

    /**
     * Set the menu for the resource.
     *
     * @param string|null $value
     *
     * @return void
     */
    public function setMenuAttribute(?string $value): void
    {
        $this->attributes['menu_id'] = $value;
    }
}
