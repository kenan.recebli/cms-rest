<?php

namespace App\Models\Concerns;

use Illuminate\Support\Facades\Date;

trait CanBeSeen
{
    use HasTimeago;

    /**
     * Count the unseen resources.
     *
     * @return int
     */
    public static function countUnseen(): int
    {
        return static::whereNull('seen_at')->count();
    }

    /**
     * Get unread resources.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getUnread()
    {
        return static::whereNull('seen_at')->get();
    }

    /**
     * Mark resource as seen.
     *
     * @return bool
     */
    public function markAsSeen(): bool
    {
        return (bool) $this->update([
            'seen_at' => Date::now(),
        ]);
    }
}
