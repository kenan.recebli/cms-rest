<?php

namespace App\Models\Concerns;

use App\Extensions\Date;

trait HasTimestamps
{
    /**
     * Get the create time.
     *
     * @param string|null $value
     *
     * @return \App\Extensions\Date
     */
    public function getCreatedAtAttribute(?string $value): Date
    {
        return Date::parse($value);
    }

    /**
     * Get the update time.
     *
     * @param string|null $value
     *
     * @return \App\Extensions\Date
     */
    public function getUpdatedAtAttribute(?string $value): Date
    {
        return Date::parse($value);
    }
}
