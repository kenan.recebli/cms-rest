<?php

namespace App\Models\Concerns;

use App\Models\Locale;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasLocale
{
    /**
     * Get the locale of the translation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function locale(): BelongsTo
    {
        return $this->belongsTo(Locale::class);
    }

    /**
     * Set the locale for the resource.
     *
     * @param string|null $value
     *
     * @return void
     */
    public function setLocaleAttribute(?string $value): void
    {
        $this->attributes['locale_id'] = $value;
    }
}
