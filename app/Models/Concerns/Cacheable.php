<?php

namespace App\Models\Concerns;

use App\Observers\CacheableObserver;
use Illuminate\Support\Facades\Cache;

trait Cacheable
{
    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootCacheable(): void
    {
        static::observe(new CacheableObserver);
    }

    /**
     * Get all of the resources from the database.
     *
     * @param array|mixed $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function all($columns = ['*'])
    {
        return Cache::rememberForever(static::class, function () use ($columns) {
            return parent::all($columns);
        });
    }
}