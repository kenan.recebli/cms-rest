<?php

namespace App\Models\Concerns;

use App\Models\View;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasViews
{
    /**
     * Get views of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function views(): MorphMany
    {
        return $this->morphMany(View::class, 'viewable');
    }

    /**
     * Add a new view to the resource.
     *
     * @return void
     */
    public function addView(): void
    {
        $this->views()->create();
    }
}
