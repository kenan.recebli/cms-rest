<?php

namespace App\Models\Concerns;

use App\Models\Country;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasCountry
{
    /**
     * Get the country of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Set the country for the resource.
     *
     * @param int $value
     *
     * @return void
     */
    public function setCountryAttribute(int $value): void
    {
        $this->attributes['country_id'] = $value;
    }
}
