<?php

namespace App\Models\Concerns;

use Illuminate\Http\Request;

trait Searches
{
    /**
     * Search resources.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function search(Request $request)
    {
        $query = static::query();

        if ($request->q) {
            foreach (static::$searchable as $field) {
                $query->orWhere($field, 'like', "%$request->q%");
            }
        }

        if ($request->id) {
            $query->orderByRaw("field(id, '$request->id') desc");
        }

        return $query->take(10)
                     ->get();
    }
}
