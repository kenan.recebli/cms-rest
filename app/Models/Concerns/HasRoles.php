<?php

namespace App\Models\Concerns;

use App\Models\Module;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;

trait HasRoles
{
    /**
     * Get the roles of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Determine if the user has the given role.
     *
     * @param string|\Illuminate\Database\Eloquent\Collection $role
     *
     * @return bool
     */
    public function hasRole($role): bool
    {
        return is_string($role)
            ? $this->roles->contains('key', $role)
            : $role->intersect($this->roles)->isNotEmpty();
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param int $action
     * @param int $moduleId
     *
     * @return bool
     */
    public function hasPermission(int $action, int $moduleId): bool
    {
        return (($permission = Permission::getByKey($action, $moduleId))) && $this->hasRole($permission->roles);
    }

    /**
     * Check if the user is a developer.
     *
     * @return bool
     */
    public function getIsDeveloperAttribute(): bool
    {
        return $this->hasRole('developer');
    }

    /**
     * Check if the user is an admin.
     *
     * @return bool
     */
    public function getIsCeoAttribute(): bool
    {
        return $this->hasRole('ceo');
    }

    /**
     * Check if the user is a superadmin.
     *
     * @return bool
     */
    public function getIsSuperuserAttribute(): bool
    {
        return $this->is_developer or $this->is_ceo;
    }

    /**
     * Check if the user is an admin.
     *
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->roles()
                    ->where('is_admin', true)
                    ->exists();
    }

    /**
     * Get the user's permissions.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getPermissionsAttribute(): Collection
    {
        return Module::has('permissions')
                     ->with([
                         'permissions.roles' => function (BelongsToMany $query) {
                             $query->whereIn('id', $this->roles->pluck('id'));
                         },
                     ])
                     ->get()
                     ->groupBy('key')
                     ->map(function (Collection $modules) {
                         $permissions = $modules->map(function (Module $module) {
                             return $module->permissions->map(function (Permission $permission) use ($module) {
                                 return [
                                     $permission->action_name => $this->is_superuser ?: $permission->roles->isNotEmpty(),
                                 ];
                             });
                         })->first();

                         foreach ($permissions as $i => $action) {
                             foreach ($action as $key => $value) {
                                 $permissions[$key] = $value;

                                 unset($permissions[$i]);
                             }
                         }

                         return $permissions;
                     });
    }
}
