<?php

namespace App\Models\Concerns;

use Illuminate\Database\Eloquent\SoftDeletingScope;

trait HasGlobalScope
{
    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootHasGlobalScope(): void
    {
        static::addGlobalScope(new (substr_replace(static::class, 'App\Scopes', 0, 10).'Scope'));
    }

    /**
     * Fetch the resource without global scopes except it's own one.
     *
     * @param int|string $id
     *
     * @return ?static
     */
    public static function fetch(int|string $id): ?static
    {
        return static::withoutGlobalScope(SoftDeletingScope::class)
                     ->find($id);
    }
}
