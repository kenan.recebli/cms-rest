<?php

namespace App\Models\Concerns;

use App\Models\Continent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasContinent
{
    /**
     * Get the continent of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function continent(): BelongsTo
    {
        return $this->belongsTo(Continent::class);
    }

    /**
     * Set the continent for the resource.
     *
     * @param string $value
     *
     * @return void
     */
    public function setContinentAttribute(string $value): void
    {
        $this->attributes['continent_id'] = $value;
    }
}
