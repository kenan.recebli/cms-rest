<?php

namespace App\Models\Concerns;

use App\Models\Section;
use App\Observers\PostObserver;
use App\Scopes\PostScope;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

trait Postable
{
    use BatchDeletes;
    use BatchRestores;
    use BatchMovesToTrash;
    use GetsOthers;
    use GetsRecents;
    use HasCategory;
    use HasFeatured;
    use HasImage;
    use HasImages;
    use HasSlug;
    use HasTimestamps;
    use HasUrl;
    use HasViews;
    use ListsResources;
    use SoftDeletes;
    use SortsResources;
    use Taggable;
    use Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'slug',
        'title',
        'description',
        'content',
        'keywords',
        'embed_video',
    ];

    /**
     * How many other posts to get.
     *
     * @var int
     */
    protected $othersCount = 3;

    /**
     * How many other posts to get.
     *
     * @var int
     */
    protected $recentsCount = 3;

    /**
     * Create a new Postable model instance.
     *
     * @param array $attributes
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = [
            'category',
            'image',
            'price',
            'has_discount',
            'is_featured',
        ];

        $this->casts = [
            'has_discount' => 'bool',
            'is_featured' => 'bool',
        ];

        static::$listWith = [
            'category',
        ];

        static::$listWithCount = [
            'views',
        ];

        parent::__construct($attributes);
    }

    /**
     * The "booting" method of the resource.
     *
     * @return void
     */
    protected static function bootPostable(): void
    {
        static::observe(new PostObserver);
        static::addGlobalScope(new PostScope);
    }

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable(): string
    {
        return 'posts';
    }

    /**
     * Get the default foreign key name for the model.
     *
     * @return string
     */
    public function getForeignKey(): string
    {
        return 'post_id';
    }

    /**
     * Get the section of the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section(): BelongsTo
    {
        return $this->belongsTo(Section::class);
    }

    /**
     * Get the section key of the resource.
     *
     * @return string
     */
    public function getSectionKeyAttribute(): string
    {
        return Cache::rememberForever(static::class.'.section-key', function () {
            return Str::of(class_basename($this))
                      ->pluralStudly()
                      ->kebab();
        });
    }

    /**
     * List posts.
     *
     * @param string|null $q
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    /*public static function list(?string $q): LengthAwarePaginator
    {
        search_resources($query = static::withCount('views'), app()->getLocale(), $q);

        return $query->latest()
                     ->paginate(8);
    }*/

    /**
     * Get the latest posts.
     *
     * @param int $limit
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function latests(int $limit = 4)
    {
        $query = static::latest();

        if (method_exists(static::class, 'category')) {
            $query->withCount('views')
                  ->with('category');
        }

        return $query->limit($limit)
                     ->get();
    }
}
