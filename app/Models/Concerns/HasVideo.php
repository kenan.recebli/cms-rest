<?php

namespace App\Models\Concerns;

trait HasVideo
{
    /**
     * Set the video for the gallery.
     *
     * @param string $value
     *
     * @return void
     */
    public function setVideoAttribute($value): void
    {
        if ($query = parse_url($value, PHP_URL_QUERY)) {
            parse_str($query, $params);

            $videoId = $params['v'];
        } else {
            $videoId = trim(parse_url($value, PHP_URL_PATH), '/');
        }

        $this->attributes['video_id'] = $videoId;
    }

    /**
     * Get the video for the gallery.
     *
     * @return string|null
     */
    public function getVideoAttribute(): ?string
    {
        return $this->video_id
            ? "https://www.youtube.com/watch?v=$this->video_id"
            : null;
    }

    /**
     * Get the poster for the video gallery.
     *
     * @return string|null
     */
    public function getPosterAttribute(): ?string
    {
        return $this->video_id
            ? "https://i.ytimg.com/vi/$this->video_id/maxresdefault.jpg"
            : null;
    }

    /**
     * Get the embed link for the video gallery.
     *
     * @return string|null
     */
    public function getEmbedVideoAttribute(): ?string
    {
        return $this->video_id
            ? "https://www.youtube.com/embed/$this->video_id"
            : null;
    }
}
