<?php

namespace App\Models\Concerns;

use App\Models\User;
use Illuminate\Database\QueryException;

trait BatchRestores
{
    /**
     * Batch restore resources.
     *
     * @param \App\Models\User $user
     * @param array            $ids
     *
     * @return array
     */
    public static function batchRestore(User $user, array $ids): array
    {
        $resources = [];

        foreach (static::withoutGlobalScopes()->find($ids) as $resource) {
            try {
                if ($user->can('restore', $resource)) {
                    if ($resource->restore()) {
                        $resources[] = $resource;
                    }
                }
            } catch (QueryException) {
                //
            }
        }

        return $resources;
    }
}
