<?php

namespace App\Models\Concerns;

use App\Models\Category;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasCategory
{
    /**
     * Get the category of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Set the category for the resource.
     *
     * @param string|null $value
     *
     * @return void
     */
    public function setCategoryAttribute(?string $value)
    {
        $this->attributes['category_id'] = $value;
    }
}
