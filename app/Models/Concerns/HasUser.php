<?php

namespace App\Models\Concerns;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasUser
{
    /**
     * Get the user of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Set the user for the resource.
     *
     * @param string $value
     *
     * @return void
     */
    public function setUserAttribute(string $value): void
    {
        $this->attributes['user_id'] = $value;
    }
}
