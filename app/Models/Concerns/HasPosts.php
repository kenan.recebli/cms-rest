<?php

namespace App\Models\Concerns;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

trait HasPosts
{
    /**
     * Get the posts for the resource.
     *
     * @param string|null $q
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPosts(?string $q): LengthAwarePaginator
    {
        search_resources($query = $this->posts(), app()->getLocale(), $q);

        return $query->paginate(6);
    }
}
