<?php

namespace App\Models\Concerns;

use Illuminate\Support\Facades\Schema;

trait ClearsTable
{
    /**
     * Clear old data.
     *
     * @return void
     */
    public static function clear(): void
    {
        Schema::disableForeignKeyConstraints();

        static::truncate();

        $translation = config('translatable.translation_model_namespace').'\\'
            .substr_replace(static::class, '', 0, 4).'Translation';

        if (class_exists($translation)) {
            $translation::truncate();
        }

        Schema::enableForeignKeyConstraints();
    }
}
