<?php

namespace App\Models\Concerns;

use App\Models\User;
use Illuminate\Database\QueryException;

trait BatchMovesToTrash
{
    /**
     * Batch move to trash resources.
     *
     * @param \App\Models\User $user
     * @param array            $ids
     *
     * @return array
     *
     */
    public static function batchMoveToTrash(User $user, array $ids): array
    {
        $resources = [];

        foreach (static::find($ids) as $resource) {
            try {
                if ($user->can('trash', $resource)) {
                    if ($resource->delete()) {
                        $resources[] = $resource;
                    }
                }
            } catch (QueryException) {
                //
            }
        }

        return $resources;
    }
}
