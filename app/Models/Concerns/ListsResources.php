<?php

namespace App\Models\Concerns;

use App\Scopes\StatusScope;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

trait ListsResources
{
    use SortsResources;

    /**
     * List resources for admin.
     *
     * @param \Illuminate\Http\Request $request
     * @param bool                     $all
     * @param string|null              $scope
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function listForAdmin(
        Request $request,
        bool    $all = false,
        string  $extraScope = null
    ): Collection|LengthAwarePaginator
    {
        return static::list($request, 'filterListForAdmin', true, $all, $extraScope);
    }

    /**
     * List resources for site.
     *
     * @param \Illuminate\Http\Request $request
     * @param bool                     $all
     * @param string|null              $extraScope
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function listForSite(
        Request $request,
        bool    $all = false,
        string  $extraScope = null
    ): Collection|LengthAwarePaginator
    {
        return static::list($request, 'filterList', false, $all, $extraScope);
    }

    /**
     * List resources.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $filter
     * @param bool                     $woStatusScope
     * @param bool                     $all
     * @param string|null              $extraScope
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function list(
        Request $request,
        string  $filter,
        bool    $woStatusScope,
        bool    $all = false,
        string  $extraScope = null
    ): Collection|LengthAwarePaginator
    {
        $query = static::with(static::$listWith)
                       ->withCount(static::$listWithCount);

        if ($query) {
            static::sort($request, $query);
        }

        //search_resources($query, get_locale($request), $request->q);

        if (method_exists(static::class, $filter)) {
            static::$filter($query, $request);
        }

        if ($woStatusScope) {
            $query->withoutGlobalScope(StatusScope::class);
        }

        if ($extraScope) {
            $query->$extraScope();
        }

        return $all
            ? $query->get()
            : $query->paginate($request->itemsPerPage ?: 10);
    }
}
