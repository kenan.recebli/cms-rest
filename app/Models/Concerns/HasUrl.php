<?php

namespace App\Models\Concerns;

use Illuminate\Support\Str;
use Illuminate\Support\Stringable;

trait HasUrl
{
    /**
     * Get the URL for the model.
     *
     * @return string
     */
    public function getUrlAttribute(): string
    {
        return get_site_url($this->url(app()->getLocale()));
    }

    /**
     * Get the URL for the translation of the model.
     *
     * @param string $locale
     *
     * @return string
     */
    public function url(string $locale): string
    {
        $route = $this->routeName();
        $host = route('homepage');

        return str_replace(
            "$host/".__("routes.$route", [], 'en'),
            "/$locale/".__("routes.$route", [], $locale),
            route("$route.show", [
                'slug' => ($this->translate($locale) ?: $this->translate(config('app.fallback_locale')))->slug,
            ])
        );
    }

    /**
     * Get the route name.
     *
     * @return \Illuminate\Support\Stringable
     */
    public function routeName(): Stringable
    {
        return Str::of(class_basename($this))->plural()->snake('-');
    }
}
