<?php

namespace App\Models\Concerns;

use App\Http\Resources\Admin\FileCollection;
use App\Models\File;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait HasFile
{
    /**
     * Get the file of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function file(): HasOne
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }

    /**
     * Set the file for the resource.
     *
     * @param array|null $value
     *
     * @return void
     */
    public function setFileAttribute(array $value = null): void
    {
        $this->attributes['file_id'] = $value ? $value['id'] : null;
    }

    /**
     * Get the file item for the resource.
     *
     * @return \App\Http\Resources\Admin\FileCollection
     */
    public function getFileItemAttribute(): FileCollection
    {
        return new FileCollection($this->file);
    }
}
