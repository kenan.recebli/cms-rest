<?php

namespace App\Models\Concerns;

use Astrotomic\Translatable\Translatable as AstrotomicTranslatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

trait Translatable
{
    use AstrotomicTranslatable;

    /**
     * Create a new Translatable model instance.
     *
     * @param array $attributes
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->with[] = 'translations';

        parent::__construct($attributes);
    }

    /**
     * Get the slugs for the resource.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getSlugsAttribute(): Collection
    {
        return $this->translations->pluck('slug', 'locale_id');
    }

    /**
     * Fill the model with an array of attributes.
     *
     * @param array $attributes
     *
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
        if (array_key_exists('translations', $attributes)) {
            foreach ($attributes['translations'] as $locale => $translation) {
                if ($this->getLocalesHelper()->has($locale) && is_array($translation)) {
                    $this->getTranslationOrNew($locale)->fill($translation);
                }
            }
        }

        return parent::fill($attributes);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $translationField
     * @param string                                $value
     * @param string|null                           $locale
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereTranslationIn(
        Builder $query, string $translationField, string $value, ?string $locale = null
    ): Builder
    {
        return $query->whereHas(
            'translations',
            function (Builder $query) use ($translationField, $value, $locale) {
                $table = $this->getTranslationsTable();

                $query->whereIn("$table.$translationField", $value);

                if ($locale) {
                    $query->where("$table.".$this->getLocaleKey(), $locale);
                }
            }
        );
    }
}
