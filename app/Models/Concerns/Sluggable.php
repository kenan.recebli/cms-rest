<?php

namespace App\Models\Concerns;

use App\Models\Translations\CityTranslation;
use App\Models\Translations\CountryTranslation;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

trait Sluggable
{
    /**
     * Set the title and slug for the resource.
     *
     * @param string $value
     *
     * @return void
     */
    public function setTitleAttribute(string $value): void
    {
        switch (true) {
            case $this instanceof CityTranslation:
            case $this instanceof CountryTranslation:
                $this->attributes['title'] = $value;
                break;

            default:
                $this->setSlug('title', $value);
        }
    }

    /**
     * Set the name and slug for the resource.
     *
     * @param string $value
     *
     * @return void
     */
    public function setNameAttribute(string $value): void
    {
        $this->setSlug('name', $value);
    }

    /**
     * Set the title and slug for the post.
     *
     * @param ?string $value
     *
     * @return string|null
     */
    public function getTitleAttribute(?string $value): ?string
    {
        return Cache::rememberForever(static::class.":$this->id", function () use ($value) {
            preg_match_all('/\[\d+]/', $value, $matches);

            foreach ($matches[0] as $match) {
                $value = str_replace($match, dec2roman(str_replace(['[', ']'], '', $match)), $value);
            }

            return $value;
        });
    }

    /**
     * Do set the title/name and slug for the post.
     *
     * @param string $key
     * @param string $value
     *
     * @return void
     */
    public function setSlug(string $key, string $value): void
    {
        if (! $value) {
            return;
        }

        Cache::forget(static::class.":$this->id");

        $this->attributes[$key] = $value;

        preg_match_all('/\[\d+]/', $value, $matches);

        foreach ($matches[0] as $match) {
            $value = str_replace(
                $match,
                $this->convert2word(str_replace(['[', ']'], '', $match)),
                $value
            );
        }

        $value = $slug = Str::slug($value, '-', $this->locale_id ? $this->locale->code : 'en');
        $i = 1;

        while ($this->slugExists($value)) {
            $value = "$slug-".++$i;
        }

        $this->attributes['slug'] = $value;
    }

    /**
     * Determine if a record exists with the given slug.
     *
     * @param string $slug
     *
     * @return bool
     */
    private function slugExists(string $slug): bool
    {
        $query = static::withoutGlobalScopes()
                       ->where('id', '!=', $this->id)
                       ->where('slug', $slug);

        if ($this->locale_id) {
            $query->where('locale_id', $this->locale_id);
        }

        return (bool) $query->count();
    }

    /**
     * Convert arabic number to word.
     *
     * @param string $n
     *
     * @return string
     */
    private function convert2word(string $n): string
    {
        switch ($this->locale->code) {
            case 'en':
                $the = 'the ';

                switch ($n) {
                    case 20:
                        return $the.'twentieth';

                    case 100:
                        return $the.'hundredth';

                    case 1000:
                        return $the.'thousandth';
                }

                $ordinals = [
                    'first',
                    'second',
                    'third',
                    'fourth',
                    'fifth',
                    'sixth',
                    'seventh',
                    'eighth',
                    'ninth',
                    'tenth',
                    'eleventh',
                    'twelfth',
                ];

                $numbers = [
                    'one',
                    'two',
                    'three',
                    'four',
                    'five',
                    'six',
                    'seven',
                    'eight',
                    'nine',
                    'ten',
                    'eleven',
                    'twelve',
                ];

                $prefixes = [
                    'thir',
                    'for',
                    'fif',
                    'six',
                    'seven',
                    'eigh',
                    'nine',
                ];

                return match (true) {
                    $n <= 12 => $the.$ordinals[--$n],
                    $n < 20 => $the.$prefixes[$n % 10 - 3].'teenth',
                    $n < 30 => $the.'twenty-'.$ordinals[$n % 10 - 1],
                    $n > 99 => $the.$numbers[(int) ($n / 100) - 1].'-hundred-'.$this->convert2word($n % 100),
                    ! ($n % 10) => $the.$prefixes[(int) ($n / 10) - 3].'tieth',
                    default => $the.$prefixes[(int) ($n / 10) - 3].'ty-'.$ordinals[$n % 10 - 1],
                };

            case 'az':
                switch ($n) {
                    case 100:
                        return 'yüzüncü';

                    case 1000:
                        return 'mininci';
                }

                $units = [
                    'bir',
                    'iki',
                    'üç',
                    'dörd',
                    'beş',
                    'altı',
                    'yeddi',
                    'səkkiz',
                    'doqquz',
                ];

                $dozens = [
                    'on',
                    'iyirmi',
                    'otuz',
                    'qırx',
                    'əlli',
                    'altmış',
                    'yetmiş',
                    'səksən',
                    'doxsan',
                ];

                $unitsSuffixes = [
                    'inci',
                    'nci',
                    'üncü',
                    'üncü',
                    'inci',
                    'ncı',
                    'nci',
                    'inci',
                    'uncu',
                ];

                $dozensSuffixes = [
                    'uncu',
                    'nci',
                    'uncu',
                    'ıncı',
                    'nci',
                    'ıncı',
                    'inci',
                    'inci',
                    'ıncı',
                ];

                switch (true) {
                    case $n < 10:
                        return $units[--$n].$unitsSuffixes[$n];

                    case ! ($n % 100):
                        return $units[$n / 100 - 1].'-yüzüncü';

                    case ! ($n % 10):
                        return $dozens[$n = $n / 10 - 1].$dozensSuffixes[$n];

                    case $n < 100:
                        return $dozens[(int) ($n / 10) - 1].'-'.$units[$n = $n % 10 - 1].$unitsSuffixes[$n];
                }

                $hundreds = ($hundreds = (int) ($n / 100)) != 0
                    ? ''
                    : $units[--$hundreds].'-';

                $dozen = ($dozen = (int) ($n % 100 / 10) - 1) < 0
                    ? ''
                    : $dozens[$dozen].'-';

                return $hundreds."yüz-$dozen".$units[$n = $n % 10 - 1].$unitsSuffixes[$n];
        }

        return $n;
    }
}
