<?php

namespace App\Models\Concerns;

trait GetsRecents
{
    /**
     * Get recent resources.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getRecentsAttribute()
    {
        return static::where('id', '!=', $this->id)
                     ->latest()
                     ->limit($this->recentsCount)
                     ->get();
    }
}
