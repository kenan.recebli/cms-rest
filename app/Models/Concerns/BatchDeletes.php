<?php

namespace App\Models\Concerns;

use App\Models\User;
use Illuminate\Database\QueryException;

trait BatchDeletes
{
    /**
     * Batch delete resources.
     *
     * @param \App\Models\User $user
     * @param array            $ids
     *
     * @return array
     */
    public static function batchDelete(User $user, array $ids): array
    {
        $resources = [];

        foreach (static::withoutGlobalScopes()->find($ids) as $resource) {
            try {
                if ($user->can('delete', $resource)) {
                    if ($resource->forceDelete()) {
                        $resources[] = $resource;
                    }
                }
            } catch (QueryException) {
                //
            }
        }

        return $resources;
    }
}
