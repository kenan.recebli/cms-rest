<?php

namespace App\Models\Concerns;

use App\Models\City;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasCity
{
    /**
     * Get the city of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Set the city for the resource.
     *
     * @param int|null $value
     *
     * @return void
     */
    public function setCityAttribute(int $value = null): void
    {
        $this->attributes['city_id'] = $value;
    }
}
