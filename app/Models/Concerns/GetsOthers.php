<?php

namespace App\Models\Concerns;

trait GetsOthers
{
    /**
     * Get other resources.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getOthersAttribute()
    {
        $quuery = static::where('id', '!=', $this->id);

        if ($this->category_id) {
            $quuery->where('category_id', $this->category_id);
        }

        return $quuery->latest()
                      ->limit($this->othersCount)
                      ->get();
    }
}
