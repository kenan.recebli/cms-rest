<?php

namespace App\Models;

use App\Http\Resources\Admin\FileItem;
use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\Cacheable;
use App\Models\Concerns\HasFile;
use App\Models\Concerns\ListsResources;
use Illuminate\Database\Eloquent\Collection;

class Setting extends BaseModel
{
    use BatchDeletes;
    use Cacheable;
    use HasFile;
    use ListsResources;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'key';

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
        'file',
        'foreign_id',
        'foreign_type',
        'is_file',
        'is_foreign',
        'is_array',
        'is_contact',
        'is_for_dev',
        'has_trans',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_file' => 'bool',
        'is_foreign' => 'bool',
        'is_array' => 'bool',
        'is_contact' => 'bool',
        'is_for_dev' => 'bool',
        'has_trans' => 'bool',
    ];

    /**
     * Get the value of the setting.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return ($setting = static::find($key)) !== null
            ? $setting->value
            : parent::__get($key);
    }

    /**
     * Get the value of the setting.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public function getValueAttribute(mixed $value): mixed
    {
        return match (true) {
            $this->is_file => new FileItem($this->file),
            $this->is_foreign => $this->morphTo('foreign')->first(),
            $this->is_array => json_decode($value) ?: [],
            $this->has_trans => json_decode($value)->{app()->getLocale()} ?? null,
            default => $value,
        };
    }

    /**
     * Get the admin value of the setting.
     *
     * @return mixed
     */
    public function getAdminValueAttribute(): mixed
    {
        switch (true) {
            case $this->is_file:
                return $this->file_id;

            case $this->is_foreign:
                return $this->foreign_id;

            case $this->is_array:
                return json_decode($this->getRawOriginal('value')) ?: [];

            case $this->has_trans:
                $value = json_decode($this->getRawOriginal('value'));

                if (! $value) {
                    $value = [];

                    foreach (config('translatable.locales') as $locale) {
                        $value[$locale] = null;
                    }
                }

                return $value;
        }

        return $this->getRawOriginal('value');
    }

    /**
     * Get all settings.
     *
     * @return static[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public static function getAll()
    {
        return static::all()
                     ->groupBy('key')
                     ->map(function (Collection $collection) {
                         return $collection->first()->value;
                     });
    }

    /**
     * Get site setting by the key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public static function key(string $key): mixed
    {
        return static::find($key)?->value;
    }

    /**
     * Set site setting by the key.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return bool
     */
    public static function set(string $key, mixed $value): bool
    {
        return static::find($key)->update(compact($value));
    }

    /**
     * Change settings.
     *
     * @param \App\Models\User $user
     * @param array            $settings
     *
     * @return array
     */
    public static function change(User $user, array $settings): array
    {
        $success = true;

        foreach ($settings as $key => $value) {
            if ($setting = static::find($key)) {
                if ($setting->is_for_dev && ! $user->is_developer) {
                    continue;
                }

                switch (true) {
                    case $setting->is_file:
                        $field = 'file';
                        break;

                    case $setting->is_foreign:
                        $field = 'foreign_id';
                        break;

                    case $setting->is_array:
                    case $setting->has_trans:
                        $field = 'value';
                        $value = json_encode(array_filter($value));
                        break;

                    default:
                        $field = 'value';
                }

                if (! $setting->update([$field => $value])) {
                    $success = false;
                }
            }
        }

        return compact('success');
    }

    /**
     * Get contact settings.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getContacts()
    {
        return Setting::where('is_contact', true)->get();
    }
}
