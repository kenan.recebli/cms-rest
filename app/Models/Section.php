<?php

namespace App\Models;

use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\HasImage;
use App\Models\Concerns\HasKey;
use App\Models\Concerns\HasSlug;
use App\Models\Concerns\HasStatus;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Translatable;
use App\Scopes\StatusScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class Section extends BaseModel
{
    use BatchDeletes;
    use HasImage;
    use HasKey;
    use HasSlug;
    use HasStatus;
    use ListsResources;
    use Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'slug',
        'title',
        'description',
        'keywords',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'image',
        'is_active',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'bool',
    ];

    /**
     * Get the section ID by its key.
     *
     * @param string $key
     *
     * @return int|null
     */
    public static function getIdByKey(string $key): ?int
    {
        return Cache::rememberForever("section.$key", function () use ($key) {
            return static::select('id')
                         ->firstWhere('key', $key)?->id;
        });
    }

    /**
     * Filter list.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public static function filterListForAdmin(Builder $query): void
    {
        if (Auth::user()->is_developer) {
            $query->withoutGlobalScope(StatusScope::class);
        }
    }

    /**
     * Get the localized URL for the section.
     *
     * @param string $locale
     *
     * @return string
     */
    public function url(string $locale): string
    {
        return localize_route($this->key, $locale);
    }
}
