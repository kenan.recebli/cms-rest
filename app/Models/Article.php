<?php

namespace App\Models;

use App\Models\Concerns\Postable;

class Article extends BaseModel
{
    use Postable;
}
