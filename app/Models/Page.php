<?php

namespace App\Models;

use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\HasImage;
use App\Models\Concerns\HasKey;
use App\Models\Concerns\HasSlug;
use App\Models\Concerns\HasUrl;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Translatable;

class Page extends BaseModel
{
    use BatchDeletes;
    use HasImage;
    use HasKey;
    use HasSlug;
    use HasUrl;
    use ListsResources;
    use Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'slug',
        'title',
        'description',
        'content',
        'keywords',
        'file',
        'file_id',
        'embed_video',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'image',
    ];
}
