<?php

namespace App\Models;

use App\Models\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use HasTimestamps;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * Indicates if the listing should be sorted.
     *
     * @var bool
     */
    protected static bool $sort = true;

    /**
     * The relationships that should be eager loaded.
     *
     * @var string[]
     */
    protected static array $listWith = [];

    /**
     * The relationships that should be counted.
     *
     * @var string[]
     */
    protected static array $listWithCount = [];

    /**
     * The fields that are searchable.
     *
     * @var string[]
     */
    protected static array $searchable = [
        'name',
    ];
}
