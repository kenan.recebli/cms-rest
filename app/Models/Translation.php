<?php

namespace App\Models;

use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Translatable;

class Translation extends BaseModel
{
    use ListsResources;
    use Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'value',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
    ];

    /**
     * Indicates if the listing should be sorted.
     *
     * @var bool
     */
    protected static bool $sort = false;
}
