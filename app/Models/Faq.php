<?php

namespace App\Models;

use App\Models\Concerns\HasStatus;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Orderable;
use App\Models\Concerns\Translatable;

class Faq extends BaseModel
{
    use HasStatus;
    use Orderable;
    use Translatable;
    use ListsResources;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'faq';

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'question',
        'answer',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_active',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'bool',
    ];
}
