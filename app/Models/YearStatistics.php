<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class YearStatistics extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'year',
        'visitors',
        'views',
    ];

    /**
     * Get the year statistics.
     *
     * @param \Carbon\Carbon $date
     *
     * @return static|null
     */
    public static function fetch(Carbon $date): ?YearStatistics
    {
        return static::where('year', $date->format('Y'))->first();
    }

    /**
     * Get the year statistics by date range.
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     *
     * @return static[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getByRange(Carbon $start, Carbon $end)
    {
        return static::whereBetween('year', [$start->format('Y'), $end->format('Y')])->get();
    }

    /**
     * Get the total count.
     *
     * @return static|null
     */
    public static function getTotalCount(): ?YearStatistics
    {
        return static::selectRaw('sum(`visitors`) as `visitors`')
                     ->selectRaw('sum(`views`) as `views`')
                     ->first();
    }
}
