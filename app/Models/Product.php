<?php

namespace App\Models;

use App\Extensions\Date;
use App\Models\Concerns\Postable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends BaseModel
{
    use Postable;

    /**
     * The relationships that should be eager loaded.
     *
     * @var string[]
     */
    protected static array $listWith = [
        'discount',
    ];

    /**
     * Get the discount that belongs to the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function discount(): HasOne
    {
        $now = Date::now();

        return $this->hasOne(Discount::class)
                    ->where(function (Builder $query) use ($now) {
                        $query->whereDate('start', '<=', $now)
                              ->orWhereNull('start');
                    })
                    ->where(function (Builder $query) use ($now) {
                        $query->whereDate('end', '>=', $now)
                              ->orWhereNull('end');
                    });
    }

    /**
     * Get the discount that belongs to the product for the admin.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function discountAdmin(): HasOne
    {
        return $this->hasOne(Discount::class);
    }

    /**
     * Get discounted posts.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getDiscounted(): LengthAwarePaginator
    {
        return static::where('has_discount', true)
                     ->latest()
                     ->paginate(8);
    }

    /**
     * Get the total price of the product.
     *
     * @return string
     */
    public function getTotalPriceAttribute(): string
    {
        return $this->calculateTotalPrice($this->discount);
    }

    /**
     * Get the total price of the product for admin.
     *
     * @return string
     */
    public function getTotalPriceAdminAttribute(): string
    {
        return $this->calculateTotalPrice($this->discountAdmin);
    }

    /**
     * Get the cut price of the product.
     *
     * @return string
     */
    public function getCutPriceAttribute(): string
    {
        return $this->calculateTotalPrice($this->discount, false);
    }

    /**
     * Calculate the total price of the product.
     *
     * @param \App\Models\Discount|null $discount
     * @param bool                      $actual
     *
     * @return string
     */
    public function calculateTotalPrice(Discount $discount = null, bool $actual = true): string
    {
        if (($actual ? $this->has_discount : true) && $discount) {
            switch ($discount->unit) {
                case 1:
                    return number_format($this->price - $discount->amount, 2);

                case 2:
                    return number_format($this->price - $this->price * $discount->amount / 100, 2);
            }
        }

        return number_format($this->price, 2);
    }
}
