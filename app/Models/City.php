<?php

namespace App\Models;

use App\Models\Concerns\BatchDeletes;
use App\Models\Concerns\HasCountry;
use App\Models\Concerns\HasSlug;
use App\Models\Concerns\ListsResources;
use App\Models\Concerns\Translatable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class City extends BaseModel
{
    use BatchDeletes;
    use HasCountry;
    use HasSlug;
    use ListsResources;
    use Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected array $translatedAttributes = [
        'slug',
        'name',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country',
        'is_capital',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_capital' => 'bool',
    ];

    /**
     * The relationships that should be eager loaded.
     *
     * @var string[]
     */
    protected static array $listWith = [
        'country.continent',
    ];

    /**
     * Get the city.
     *
     * @param string              $countryId
     * @param string              $name
     * @param \Carbon\Carbon|null $time
     *
     * @return null|static
     */
    public static function getByCountry(string $countryId, string $name, Carbon $time = null): ?City
    {
        return Cache::remember("$countryId:$name", $time ?: Carbon::now(), function () use ($countryId, $name) {
            return static::where('country_id', $countryId)
                         ->whereTranslation('name', $name, 'en')
                         ->first();
        });
    }
}
