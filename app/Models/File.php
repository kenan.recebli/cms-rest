<?php

namespace App\Models;

use App\Models\Concerns\HasFolder;
use App\Models\Concerns\HasObserver;
use App\Models\Concerns\ListsOrphans;
use Astrotomic\Translatable\Translatable;
use getID3;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File as FileFacade;
use Illuminate\Support\Facades\Storage;

class File extends BaseModel
{
    use HasFolder,
        HasObserver,
        ListsOrphans,
        Translatable;

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected array $translatedAttributes = [
        'slug',
        'name',
        'alt',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'folder',
        'filename',
        'extension',
        'size',
        'width',
        'height',
        'duration',
        'is_image',
        'is_video',
        'is_audio',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_image' => 'bool',
        'is_video' => 'bool',
        'is_audio' => 'bool',
    ];

    /**
     * Set the extension of the file.
     *
     * @param string $value
     *
     * @return void
     */
    public function setExtensionAttribute(string $value): void
    {
        if ($value) {
            $this->attributes['extension'] = $value;
        }
    }

    /**
     * Get a filesystem instance.
     *
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function getDiskAttribute(): Filesystem
    {
        return Storage::disk(
            Cache::rememberForever($this->cacheName('disk'), function () {
                return guess_disk_name($this->is_image, $this->is_video, $this->is_audio);
            })
        );
    }

    /**
     * Get the full path for the file at the given "short" path.
     *
     * @return string
     */
    public function getPathAttribute(): string
    {
        return Cache::rememberForever($this->cacheName('path'), function () {
            return $this->disk->path($this->file);
        });
    }

    /**
     * Get the URL to the file.
     *
     * @return string
     */
    public function getUrlAttribute(): string
    {
        //return localize($this->raw_url);
        return $this->raw_url;
    }

    /**
     * Get the unlocalized raw URL to the file.
     *
     * @return string
     */
    public function getRawUrlAttribute(): string
    {
        return route($this->is_image ? 'image' : 'file.view', [
            $this->is_image ? 'image' : 'file' => $this->slug.$this->ext,
        ]);
    }

    /**
     * Get the view URL to the file.
     *
     * @return string
     */
    public function getViewAttribute(): string
    {
        return localize(route('file.view', ['file' => $this->slug.$this->ext]));
    }

    /**
     * Get the file as an item.
     *
     * @return object
     */
    public function getItemAttribute(): object
    {
        return (object) $this->only([
            'url',
            'alt',
            'width',
            'height',
        ]);
    }

    /**
     * Get the file.
     *
     * @return string
     */
    public function getFileAttribute(): string
    {
        return $this->filename.$this->ext;
    }

    /**
     * Get the extension of the file.
     *
     * @return string|null
     */
    public function getExtAttribute(): ?string
    {
        return $this->extension ? ".$this->extension" : null;
    }

    /**
     * Get format of the file.
     *
     * @return string|null
     */
    public function getFormatAttribute(): ?string
    {
        return $this->extension
            ? ($this->extension == 'jpg' ? 'JPEG' : strtoupper($this->extension))
            : null;
    }

    /**
     * Get size of the file.
     *
     * @param int $value
     *
     * @return string
     */
    public function getSizeAttribute(int $value): string
    {
        return format_bytes($value);
    }

    /**
     * Get dimensions of the image or video.
     *
     * @return string|null
     */
    public function getDimensionsAttribute(): ?string
    {
        return $this->width && $this->height
            ? $this->width.'x'.$this->height
            : null;
    }

    /**
     * Get file's ID3 data.
     *
     * @return array
     */
    public function getId3Attribute(): array
    {
        return Cache::rememberForever($this->cacheName('id3'), function () {
            return (new getID3)->analyze($this->path);
        });
    }

    /**
     * Get the mimetype the file.
     *
     * @return string
     */
    public function getMimeTypeAttribute(): string
    {
        return Cache::rememberForever($this->cacheName('mimetype'), function () {
            return FileFacade::mimeType($this->path);
        });
    }

    /**
     * Check if the file is an SVG.
     *
     * @return bool
     */
    public function getIsSvgAttribute(): bool
    {
        return $this->extension == 'svg';
    }

    /**
     * Determine if a file exists.
     *
     * @return bool
     */
    public function exists(): bool
    {
        return $this->disk->exists($this->file);
    }

    /**
     * Scale the image for the thumbnail.
     *
     * @param int $width
     * @param int $height
     *
     * @return string
     */
    public function scale(int $width, int $height): string
    {
        return route('thumbnail', [
            'dimensions' => $width.'x'.$height,
            'image' => $this->slug.$this->ext,
        ]);
    }

    /**
     * Get the file by its name.
     *
     * @param string      $name
     * @param string|null $locale
     *
     * @return null|static
     */
    public static function getByName(string $name, string $locale = null): ?File
    {
        $parts = explode('.', $name);

        $query = static::whereTranslation('slug', $parts[0], $locale ?: app()->getLocale());

        if (isset($parts[1])) {
            $query->where('extension', $parts[1]);
        }

        if ($item = $query->first()) {
            return $item;
        }

        return $locale != ($fbLocale = config('app.fallback_locale'))
            ? static::getByName($name, $fbLocale)
            : null;
    }

    /**
     * Delete file from the storage.
     *
     * @return void
     */
    public function deleteFromStorage(): void
    {
        $this->disk->delete($this->file);
    }

    /**
     * Make a name for the cache.
     *
     * @param string $attribute
     *
     * @return string
     */
    private function cacheName(string $attribute): string
    {
        return "file.$this->id:$attribute";
    }

    /**
     * Set dimensions and duration for the newly uploaded file.
     *
     * @return void
     */
    public function setDimensionsAndDuration(): void
    {
        $width = $height = $duration = null;

        if ($this->is_image) {
            [$width, $height] = getimagesize($this->path);
        } elseif ($this->is_video) {
            $video = $this->id3['video'];

            $width = $video['resolution_x'];
            $height = $video['resolution_y'];
        }

        if ($this->is_video || $this->is_audio) {
            $parts = explode(':', $duration = $this->id3['playtime_string']);

            if (count($parts) < 3) {
                $duration = '0:'.$parts[0].':'.$parts[1];
            }
        }

        $this->update(compact('width', 'height', 'duration'));
    }

    /**
     * Batch delete files.
     *
     * @param array $ids
     *
     * @return array
     *
     * @throws \Illuminate\Database\QueryException
     */
    public static function batchDelete(array $ids): array
    {
        $deleted = [];

        foreach (static::find($ids) as $file) {
            try {
                if ($file->delete()) {
                    $deleted[] = $file->id;
                }
            } catch (QueryException) {
                //
            }
        }

        return $deleted;
    }

    /**
     * Move files.
     *
     * @param array    $ids
     * @param int|null $folderId
     *
     * @return array
     */
    public static function move(array $ids, int $folderId = null): array
    {
        $moved = [];

        foreach (static::find($ids) as $file) {
            if ($file->folder()->associate($folderId)->save()) {
                $moved[] = $file->id;
            }
        }

        return $moved;
    }

    /**
     * Get images.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getImages(): Collection
    {
        return static::where('is_image', true)->get();
    }

    /**
     * Get the URL for the translation of the model.
     *
     * @param string $locale
     *
     * @return string
     */
    public function url(string $locale): string
    {
        return localize($this->url, $locale);
    }
}
