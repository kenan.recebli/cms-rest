<?php

namespace App\Models;

use App\Models\Concerns\HasContinent;
use App\Models\Concerns\HasImage;
use App\Models\Concerns\HasImages;
use App\Models\Concerns\HasSlug;
use App\Models\Concerns\Translatable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Country extends BaseModel
{
    use HasContinent;
    use HasImage;
    use HasImages;
    use HasSlug;
    use Translatable;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that have translations.
     *
     * @var array
     */
    protected array $translatedAttributes = [
        'slug',
        'name',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'continent',
    ];

    /**
     * Get the cities of the country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities(): HasMany
    {
        return $this->hasMany(City::class);
    }

    /**
     * Get the capital of the country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function capital(): HasOne
    {
        return $this->hasOne(City::class)
                    ->where('is_capital', true);
    }

    /**
     * Get visitors for the country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitors(): HasMany
    {
        return $this->hasMany(Visitor::class);
    }

    /**
     * Add a city to the country.
     *
     * @param array $city
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function addCity(array $city): Model
    {
        return $this->cities()->create($city);
    }

    /**
     * List cities of the country.
     *
     * @param int $itemsPerPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listCities(int $itemsPerPage): LengthAwarePaginator
    {
        return $this->cities()
                    ->latest()
                    ->paginate($itemsPerPage ?: 10);
    }

    /**
     * Get the country by code.
     *
     * @param string $code
     *
     * @return null|static
     */
    public static function getByCode(string $code): ?static
    {
        return static::firstWhere('code', $code);
    }
}
