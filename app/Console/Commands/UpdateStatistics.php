<?php

namespace App\Console\Commands;

use App\Extensions\Date;
use App\Models\DayStatistics;
use App\Models\MonthStatistics;
use App\Models\View;
use App\Models\Visitor;
use App\Models\WeekStatistics;
use App\Models\YearStatistics;
use Illuminate\Console\Command;

class UpdateStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $visitors = Visitor::countByDate($date = Date::yesterday());
        $views = View::countByDate($date);

        static::update(DayStatistics::class, $date, $data = compact('visitors', 'views'), 'day', 'Y-m-d');
        static::update(WeekStatistics::class, $date, $data, 'week', 'YW');
        static::update(MonthStatistics::class, $date, $data, 'month', 'Ym');
        static::update(YearStatistics::class, $date, $data, 'year', 'Y');
    }

    /**
     * Do update statistics.
     *
     * @param string               $class
     * @param \App\Extensions\Date $date
     * @param array                $data
     * @param string               $field
     * @param string               $format
     *
     * @return void
     */
    public static function update(
        string $class, Date $date, array $data, string $field, string $format
    ): void
    {
        $statistics = $class::fetch($date);

        if ($statistics) {
            foreach ($data as $key => $item) {
                $data[$key] += $statistics->$key;
            }

            $statistics->update($data);
        } else {
            $class::create([$field => $date->format($format)] + $data);
        }
    }
}
