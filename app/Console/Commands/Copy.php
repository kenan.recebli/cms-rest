<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Copy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy {src} {dest}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * The source file.
     *
     * @var string
     */
    private string $src;

    /**
     * The target file.
     *
     * @var string
     */
    private string $dest;

    /**
     * The source file (uppercase).
     *
     * @var string
     */
    private string $ucSrc;

    /**
     * The target file (uppercase).
     *
     * @var string
     */
    private string $ucDest;

    /**
     * The source file (plural).
     *
     * @var string
     */
    private string $plSrc;

    /**
     * The target file (plural, uppercase).
     *
     * @var string
     */
    private string $plDest;

    /**
     * The source file (plural, uppercase).
     *
     * @var string
     */
    private string $plUcSrc;

    /**
     * The target file (plural).
     *
     * @var string
     */
    private string $plUcDest;

    /**
     * @var string
     */
    private string $kSrc;

    /**
     * @var string
     */
    private string $kDest;

    /**
     * @var string
     */
    private string $kPlSrc;

    /**
     * @var string
     */
    private string $kPlDest;

    /**
     * @var string
     */
    private string $kUcSrc;

    /**
     * @var string
     */
    private string $kUcDest;

    /**
     * @var string
     */
    private string $kPlUcSrc;

    /**
     * @var string
     */
    private string $kPlUcDest;

    /**
     * @var string
     */
    private string $sSrc;

    /**
     * @var string
     */
    private string $sDest;

    /**
     * @var string
     */
    private string $sPlSrc;

    /**
     * @var string
     */
    private string $sPlDest;

    /**
     * @var string
     */
    private string $sUcSrc;

    /**
     * @var string
     */
    private string $sUcDest;

    /**
     * @var string
     */
    private string $sPlUcSrc;

    /**
     * @var string
     */
    private string $sPlUcDest;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->src = $this->argument('src');
        $this->dest = $this->argument('dest');

        $this->ucSrc = ucfirst($this->src);
        $this->ucDest = ucfirst($this->dest);

        $this->plSrc = Str::pluralStudly($this->src);
        $this->plDest = Str::pluralStudly($this->dest);

        $this->plUcSrc = Str::pluralStudly($this->ucSrc);
        $this->plUcDest = Str::pluralStudly($this->ucDest);

        $this->kSrc = Str::kebab($this->src);
        $this->kDest = Str::kebab($this->dest);

        $this->kUcSrc = Str::kebab($this->ucSrc);
        $this->kUcDest = Str::kebab($this->ucDest);

        $this->kPlSrc = Str::kebab($this->plSrc);
        $this->kPlDest = Str::kebab($this->plDest);

        $this->kPlUcSrc = Str::kebab($this->plUcSrc);
        $this->kPlUcDest = Str::kebab($this->plUcDest);

        $this->sSrc = Str::snake($this->src);
        $this->sDest = Str::snake($this->dest);

        $this->sUcSrc = Str::snake($this->ucSrc);
        $this->sUcDest = Str::snake($this->ucDest);

        $this->sPlSrc = Str::snake($this->plSrc);
        $this->sPlDest = Str::snake($this->plDest);

        $this->sPlUcSrc = Str::snake($this->plUcSrc);
        $this->sPlUcDest = Str::snake($this->plUcDest);

        $this->scan(app_path('*'));
        $this->scan(database_path('*'));
    }

    private function scan($path)
    {
        foreach (glob($path, GLOB_MARK) as $file) {
            if (is_dir($file)) {
                self::scan("$file*");
            } else {
                switch (true) {
                    case str_contains($file, $this->plSrc):
                        $this->copy($file, $this->plSrc, $this->plDest);
                        break;

                    case str_contains($file, $this->src):
                        $this->copy($file, $this->src, $this->dest);
                        break;

                    case str_contains($file, $this->plUcSrc):
                        $this->copy($file, $this->plUcSrc, $this->plUcDest);
                        break;

                    case str_contains($file, $this->ucSrc):
                        $this->copy($file, $this->ucSrc, $this->ucDest);
                        break;

                    case str_contains($file, $this->kPlSrc):
                        $this->copy($file, $this->kPlSrc, $this->kPlDest);
                        break;

                    case str_contains($file, $this->kSrc):
                        $this->copy($file, $this->kSrc, $this->kDest);
                        break;

                    case str_contains($file, $this->kPlUcSrc):
                        $this->copy($file, $this->kPlUcSrc, $this->kPlUcDest);
                        break;

                    case str_contains($file, $this->kUcSrc):
                        $this->copy($file, $this->kUcSrc, $this->kUcDest);
                        break;

                    case str_contains($file, $this->sPlSrc):
                        $this->copy($file, $this->sPlSrc, $this->sPlDest);
                        break;

                    case str_contains($file, $this->sSrc):
                        $this->copy($file, $this->sSrc, $this->sDest);
                        break;

                    case str_contains($file, $this->sPlUcSrc):
                        $this->copy($file, $this->sPlUcSrc, $this->sPlUcDest);
                        break;

                    case str_contains($file, $this->sUcSrc):
                        $this->copy($file, $this->sUcSrc, $this->sUcDest);
                }
            }
        }
    }

    private function copy($file, $src, $dest)
    {
        try {
            copy($file, $file = str_replace($src, $dest, $file));

            $this->replace($file);
        } catch (Exception $exception) {
            dump($exception->getMessage());
        }
    }

    private function replace($file)
    {
        file_put_contents(
            $file,
            str_replace(
                [
                    $this->plSrc,
                    $this->plUcSrc,
                    $this->src,
                    $this->ucSrc,
                    $this->kPlSrc,
                    $this->kPlUcSrc,
                    $this->kSrc,
                    $this->kUcSrc,
                    $this->sPlSrc,
                    $this->sPlUcSrc,
                    $this->sSrc,
                    $this->sUcSrc,
                ],
                [
                    $this->plDest,
                    $this->plUcDest,
                    $this->dest,
                    $this->ucDest,
                    $this->kPlDest,
                    $this->kPlUcDest,
                    $this->kDest,
                    $this->kUcDest,
                    $this->sPlDest,
                    $this->sPlUcDest,
                    $this->sDest,
                    $this->sUcDest,
                ],
                file_get_contents($file)
            )
        );
    }
}
