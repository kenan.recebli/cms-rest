<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CopyFrom extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copyfrom {from} {src}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Where to copy from.
     *
     * @var string
     */
    private static string $from;

    /**
     * The source file.
     *
     * @var string
     */
    private static string $src;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        static::$from = $from = $this->argument('from');
        static::$src = $this->argument('src');

        $from = base_path("../$from");

        static::scan("$from/app/*");
        static::scan("$from/database/*");
    }

    private static function scan($path)
    {
        $from = static::$from;
        $src = static::$src;
        $ucSrc = ucfirst($src);
        $pSrc = Str::pluralStudly($src);
        $pUcSrc = Str::pluralStudly($ucSrc);

        foreach (glob($path, GLOB_MARK) as $file) {
            switch (true) {
                case is_dir($file);
                    static::scan("$file*");
                    break;

                case str_contains($file, $src):
                case str_contains($file, $ucSrc):
                case str_contains($file, $pSrc):
                case str_contains($file, $pUcSrc):
                    copy($file, str_replace("/../$from", '', $file));
            }
        }
    }
}
