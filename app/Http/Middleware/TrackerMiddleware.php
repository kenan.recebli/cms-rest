<?php

namespace App\Http\Middleware;

use App\Models\City;
use App\Models\Country;
use App\Models\Visitor;
use Carbon\Carbon;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class TrackerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Illuminate\Http\Response
     */
    public function handle(Request $request, Closure $next)
    {
        /*if (! ($user = $request->user()) or ! $user->is_admin) {
            $agent = new Agent;

            if (! $agent->isRobot()) {
                $time = Carbon::now();

                $visitor = Visitor::firstOrNew([
                    'session' => $request->session()->getId(),
                    'platform' => $agent->platform(),
                    'browser' => $browser = $agent->browser(),
                    'version' => $agent->version($browser),
                    'ip' => $ip = $request->server('HTTP_CF_CONNECTING_IP') ?: $request->ip(),
                    'created_at' => $time->toDateString(),
                ]);

                $visitor->visited_at = $time->toDateTimeString();

                if ($visitor->exists) {
                    $visitor->visits++;
                } else {
                    $location = get_location($ip);
                    $country = Country::getByCode($location->country);

                    $visitor->country_id = $country->id;
                    $visitor->continent_id = $country->continent->id;

                    if ($city = $location->region) {
                        $city = City::get($country->id, $name = utf8_encode($city), $time->addMonths(3))
                            ?: $country->addCity(['name:en' => $name]);

                        $visitor->city_id = $city->id;
                    }
                }

                try {
                    $visitor->save();
                } catch (Exception) {
                    //
                }
            }
        }*/

        return $next($request);
    }
}
