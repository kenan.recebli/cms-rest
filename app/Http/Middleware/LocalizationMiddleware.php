<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class LocalizationMiddleware
{
    /**
     * Create a new LocalizationMiddleware instance.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $locale = $request->header('Accept-Language');

        if (! $locale or ! in_array($locale, $this->app->config->get('translatable.locales'))) {
            $this->app->setLocale($this->app->getFallbackLocale());
        }

        return $next($request);
    }
}
