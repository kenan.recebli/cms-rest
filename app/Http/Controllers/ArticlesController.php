<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Concerns\ListsCollection;
use App\Http\Controllers\Concerns\ShowsResource;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;
use App\Models\Article;

class ArticlesController extends Controller
{
    use ListsCollection;
    use ShowsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->section = 'articles';

        $this->model = Article::class;

        $this->collection = ArticleCollection::class;

        $this->resource = ArticleResource::class;
    }
}
