<?php

namespace App\Http\Controllers;

use App\Models\File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FilesController extends Controller
{
    /**
     * Get the current application locale.
     *
     * @var string
     */
    private string $locale;

    /**
     * ImagesController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->locale = app()->getLocale();
    }

    /**
     * Download the file.
     *
     * @param string $file
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(string $file): BinaryFileResponse
    {
        $file = $this->get($file);

        return response()->download($file->path, ($file->alt ?: $file->name).".$file->extension");
    }

    /**
     * View the file.
     *
     * @param string $file
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function view(string $file): BinaryFileResponse
    {
        return response()->file($this->get($file)->path);
    }

    /**
     * Get the file and cache it if not already cached.
     *
     * @param string $file
     *
     * @return \App\Models\File
     */
    private function get(string $file): File
    {
        $file = Cache::remember("$this->locale.$file", Carbon::now()->addMonth(), function () use ($file) {
            return File::getByName($file);
        });

        abort_unless($file && $file->exists(), 404);

        return $file;
    }
}
