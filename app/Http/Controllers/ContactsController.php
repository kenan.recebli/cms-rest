<?php

namespace App\Http\Controllers;

use App\Http\Resources\SectionResource;
use App\Models\Section;
use App\Models\Setting;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return new JsonResponse([
            'data' => Setting::getContacts()
                             ->groupBy('key')
                             ->map(function (Collection $contact) {
                                 return $contact->first()->value;
                             }),
            'section' => new SectionResource(Section::getByKey('contacts')),
        ]);
    }
}
