<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Concerns\ListsCollection;
use App\Http\Resources\FaqCollection;
use App\Models\Faq;

class FaqController extends Controller
{
    use ListsCollection;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->section = 'faq';

        $this->model = Faq::class;

        $this->collection = FaqCollection::class;

        $this->listAll = true;
    }
}
