<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Resources\Admin\FileCollection;
use App\Http\Resources\Admin\FolderCollection;
use App\Http\Resources\Admin\FolderResource;
use App\Models\File;
use App\Models\Folder;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class FoldersController extends Controller
{
    use CRUDController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Folder::class;

        $this->editResource = FolderCollection::class;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:100',
                Rule::unique('folders', 'name')
                    ->ignore($request->id)
                    ->where(function (Builder $query) use ($request) {
                        return $query->where('folder_id', $request->folder)
                                     ->where('name', $request->name);
                    }),
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if ($folder = $request->folder) {
            $folder = Folder::find($folder);
        }

        return new JsonResponse([
            'current_folder' => $folder
                ? new FolderResource($folder)
                : null,
            'folders' => FolderCollection::collection($folder?->folders ?: Folder::listOrphans()),
            'files' => FileCollection::collection($folder?->files ?: File::listOrphans()),
        ]);
    }
}
