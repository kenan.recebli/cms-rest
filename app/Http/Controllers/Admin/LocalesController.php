<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\SortsResource;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Requests\SortLocales;
use App\Http\Resources\Admin\LocaleResource;
use App\Models\Locale;
use Illuminate\Http\Request;

class LocalesController extends Controller
{
    use CRUDController;
    use TogglesResource;
    use SortsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Locale::class;

        $this->collection = LocaleResource::class;

        $this->editResource = LocaleResource::class;

        $this->listAll = true;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return [
            'code' => "required|string|max:2|unique:locales,code,$request->id",
            'name' => "required|string|max:41|unique:locales,name,$request->id",
            'script' => 'required|string|max:4',
            'native' => 'required|string|max:16',
            'regional' => "required|string|max:6|unique:locales,regional,$request->id",
            'is_active' => 'nullable|boolean',
        ];
    }

    /**
     * Sort resources.
     *
     * @param \App\Http\Requests\SortLocales $request
     *
     * @return void
     */
    public function sort(SortLocales $request): void
    {
        $this->doSort($request);
    }
}
