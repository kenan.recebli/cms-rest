<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

trait DeletesResource
{
    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int|string               $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, int|string $id): JsonResponse
    {
        if ($this->can('delete', $resource = $this->get_resource($id))) {
            if ($response = $this->response($resource->forceDelete())) {
                $this->postDelete($request, $resource);
            }

            return $response;
        }

        $this->throwUnauthorized();
    }

    /**
     * Batch delete resources.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function batchDelete(Request $request): JsonResponse
    {
        if ($this->can('delete')) {
            $resources = $this->model::batchDelete($request->user(), $request->ids);

            foreach ($resources as $resource) {
                $this->postDelete($request, $resource);
            }

            return $this->response((bool) $resources);
        }

        $this->throwUnauthorized();
    }
}
