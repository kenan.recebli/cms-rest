<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

trait TogglesResource
{
    use BaseController;

    /**
     * Toggle the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int|string               $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggle(Request $request, int|string $id): JsonResponse
    {
        $key = $request->key;

        if ($this->can($key == 'is_active' ? 'toggle' : 'update', $resource = $this->get_resource($id))) {
            return $this->response(
                $resource->update([
                    $key => ! $resource->$key,
                ])
            );
        }

        $this->throwUnauthorized();
    }
}
