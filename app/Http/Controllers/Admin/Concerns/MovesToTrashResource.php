<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

trait MovesToTrashResource
{
    /**
     * Move to trash the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function moveToTrash(Request $request, int $id): JsonResponse
    {
        if ($this->can('trash', $resource = $this->get_resource($id))) {
            if ($response = $this->response($resource->delete())) {
                $this->postMoveToTrash($request, $resource);
            }

            return $response;
        }

        $this->throwUnauthorized();
    }

    /**
     * Batch move to trash resources.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function batchMoveToTrash(Request $request): JsonResponse
    {
        if ($this->can('trash')) {
            $resources = $this->model::batchMoveToTrash($request->user(), $request->ids);

            foreach ($resources as $resource) {
                $this->postMoveToTrash($request, $resource);
            }

            return $this->response((bool) $resources);
        }

        $this->throwUnauthorized();
    }
}
