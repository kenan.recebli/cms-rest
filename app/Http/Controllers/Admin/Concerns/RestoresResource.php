<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

trait RestoresResource
{
    /**
     * Restore the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(Request $request, int $id): JsonResponse
    {
        if ($this->can('restore', $resource = $this->get_resource($id))) {
            if ($response = $this->response($resource->restore())) {
                $this->postRestore($request, $resource);
            }

            return $response;
        }

        $this->throwUnauthorized();
    }

    /**
     * Batch restore resources.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function batchRestore(Request $request): JsonResponse
    {
        if ($this->can('restore')) {
            $resources = $this->model::batchRestore($request->user(), $request->ids);

            foreach ($resources as $resource) {
                $this->postRestore($request, $resource);
            }

            return $this->response((bool) $resources);
        }

        $this->throwUnauthorized();
    }
}
