<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\Request;

trait ListsCollection
{
    use BaseController;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if ($this->can('read')) {
            return $this->collection::collection(
                $this->model::{$this->listFn}($request, $this->listAll)
            );
        }

        $this->throwUnauthorized();
    }
}
