<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

trait GetsList
{
    /**
     * Get a list of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function get(): AnonymousResourceCollection
    {
        return $this->listing::collection($this->model::all());
    }
}
