<?php

namespace App\Http\Controllers\Admin\Concerns;

trait CRUDController
{
    use ListsCollection;
    use StoresResource;
    use EditsResource;
    use UpdatesResource;
    use DeletesResource;
}
