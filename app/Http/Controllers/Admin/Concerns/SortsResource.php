<?php

namespace App\Http\Controllers\Admin\Concerns;

use App\Http\Requests\Sort;
use Illuminate\Http\Request;

trait SortsResource
{
    use BaseController;

    /**
     * Sort resources.
     *
     * @param \App\Http\Requests\Sort $request
     *
     * @return void
     */
    public function sort(Sort $request): void
    {
        $this->doSort($request);
    }

    /**
     * Do sort resources.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function doSort(Request $request): void
    {
        foreach ($request->ids as $i => $localeId) {
            if ($locale = $this->model::withoutGlobalScopes()->find($localeId)) {
                $locale->order(++$i);
            }
        }
    }
}
