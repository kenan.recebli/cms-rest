<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\JsonResponse;

trait ReturnsResponse
{
    /**
     * Return a response.
     *
     * @param bool $success
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(bool $success): JsonResponse
    {
        return new JsonResponse(compact('success'));
    }
}
