<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;

trait PostController
{
    use CRUDController;
    use ListsTrashedCollection;
    use MovesToTrashResource;
    use RestoresResource;
    use DeletesResource;
    use TogglesResource;
    use SearchesResource;

    /**
     * Get resource and throw 404 if not found.
     *
     * @param int|string $id
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function get_resource(int|string $id): ?Model
    {
        abort_unless($resource = $this->model::withoutGlobalScope(SoftDeletingScope::class)->find($id), 404);

        return $resource;
    }
}
