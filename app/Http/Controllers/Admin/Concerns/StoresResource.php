<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

trait StoresResource
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function store(Request $request): JsonResource
    {
        if ($this->can('create')) {
            $request->validate($this->rules($request));

            if ($resource = $this->model::create($request->all())) {
                $this->postSave($request, $resource);
            }

            return new $this->editResource($resource);
        }

        $this->throwUnauthorized();
    }
}
