<?php

namespace App\Http\Controllers\Admin\Concerns;

use App\Extensions\Date;
use App\Models\DayStatistics;
use App\Models\MonthStatistics;
use App\Models\YearStatistics;

trait Statistician
{
    /**
     * Get statistics for the last n days.
     *
     * @param int $n
     *
     * @return array
     */
    private static function getLastDays(int $n): array
    {
        return static::getDaysRange($n, Date::tomorrow()->subDays($n), Date::yesterday());
    }

    /**
     * Get statistics for the last n days.
     *
     * @param int                  $n
     * @param \App\Extensions\Date $start
     * @param \App\Extensions\Date $end
     *
     * @return array
     */
    private static function getDaysRange(int $n, Date $start, Date $end): array
    {
        $lastDays = [
            'days' => [],
            'visitors' => [],
            'views' => [],
        ];

        $statistics = DayStatistics::getByRange($start, $end);

        for (; $n > 1; $n--) {
            $statistic = $statistics->where('day', $start->toDateString())->first();

            $lastDays['days'][] = $start->dm;
            $lastDays['visitors'][] = $statistic?->visitors ?: 0;
            $lastDays['views'][] = $statistic?->views ?: 0;

            $start->addDay();
        }

        return $lastDays;
    }

    /**
     * Get statistics for the last n months.
     *
     * @param int $n
     *
     * @return array
     */
    private static function getLastMonths(int $n): array
    {
        $lastMonths = [
            'months' => [],
            'visitors' => [],
            'views' => [],
        ];

        $days = date('d') - 1;

        $end = Date::today()->subDays($days)->addMonth();
        $start = Date::today()->subDays($days)->subMonths($n - 1);

        $statistics = MonthStatistics::getByRange($start, $end);

        for ($i = $n; $i > 0; $i--) {
            $statistic = $statistics->where('month', $start->format('Ym'))->first();

            $lastMonths['months'][] = $start->format('F');
            $lastMonths['visitors'][] = $statistic?->visitors ?: 0;
            $lastMonths['views'][] = $statistic?->views ?: 0;

            $start->addMonth();
        }

        return $lastMonths;
    }

    /**
     * Get statistics for the last 5 years.
     *
     * @param int $n
     *
     * @return array
     */
    private static function getLastYears(int $n): array
    {
        $lastYears = [
            'years' => [],
            'visitors' => [],
            'views' => [],
        ];

        $days = date('z');

        $end = Date::today()->subDays($days)->addYear();
        $start = Date::today()->subDays($days)->subYears($n - 1);

        $statistics = YearStatistics::getByRange($start, $end);

        for ($i = $n; $i > 0; $i--) {
            $statistic = $statistics->where('year', $year = $start->format('Y'))->first();

            $lastYears['years'][] = $year;
            $lastYears['visitors'][] = $statistic?->visitors ?: 0;
            $lastYears['views'][] = $statistic?->views ?: 0;

            $start->addYear();
        }

        return $lastYears;
    }
}
