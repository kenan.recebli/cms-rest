<?php

namespace App\Http\Controllers\Admin\Concerns;

use App\Http\Resources\Admin\Listing;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

trait BaseController
{
    use ReturnsResponse;

    /**
     * The associated model.
     *
     * @var string|null
     */
    public ?string $model = null;

    /**
     * The collection class.
     *
     * @var string|null
     */
    public ?string $collection = null;

    /**
     * The resource class for editing.
     *
     * @var string|null
     */
    public ?string $editResource = null;

    /**
     * The resource class for showing.
     *
     * @var string|null
     */
    public ?string $showResource = null;

    /**
     * The listing collection class.
     *
     * @var string|null
     */
    public ?string $listing = Listing::class;

    /**
     * The listing function.
     *
     * @var string
     */
    public string $listFn = 'listForAdmin';

    /**
     * Indicates whether all resources should be listed.
     *
     * @var bool
     */
    public bool $listAll = false;

    /**
     * Check if the user can perform the action.
     *
     * @param string                                   $action
     * @param \Illuminate\Database\Eloquent\Model|null $model
     *
     * @return bool
     */
    public function can(string $action, Model $model = null): bool
    {
        return Auth::user()->can($action, $model ?: $this->model);
    }

    /**
     * Get resource and throw 404 if not found.
     *
     * @param int|string $id
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function get_resource(int|string $id): ?Model
    {
        abort_unless($resource = $this->model::withoutGlobalScopes()->find($id), 404);

        return $resource;
    }

    /**
     * Save extra data of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BaseModel    $model
     *
     * @return void
     */
    public function postSave(Request $request, BaseModel $model): void
    {
        //
    }

    /**
     * Do some action after deleting the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BaseModel    $model
     *
     * @return void
     */
    public function postDelete(Request $request, BaseModel $model): void
    {
        //
    }

    /**
     * Do some action after moving the resource to trash.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BaseModel    $model
     *
     * @return void
     */
    public function postMoveToTrash(Request $request, BaseModel $model): void
    {
        //
    }

    /**
     * Do some action after restoring the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BaseModel    $model
     *
     * @return void
     */
    public function postRestore(Request $request, BaseModel $model): void
    {
        //
    }

    /**
     * Throw unauthorized exception.
     *
     * @return \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function throwUnauthorized(): AccessDeniedHttpException
    {
        throw new AccessDeniedHttpException('This action is unauthorized.');
    }
}
