<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

trait UpdatesResource
{
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int|string               $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int|string $id): JsonResponse
    {
        if ($this->can('update', $resource = $this->get_resource($id))) {
            $request->validate($this->rules($request));

            if ($result = $resource->update($request->all())) {
                $this->postSave($request, $resource);
            }

            return $this->response($result);
        }

        $this->throwUnauthorized();
    }
}
