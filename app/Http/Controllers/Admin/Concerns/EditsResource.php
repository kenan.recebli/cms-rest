<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\Resources\Json\JsonResource;

trait EditsResource
{
    use BaseController;

    /**
     * Show the form for editing the specified resource.
     *
     * @param int|string $id
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function edit(int|string $id): JsonResource
    {
        if ($this->can('update', $resource = $this->get_resource($id))) {
            return new $this->editResource($resource);
        }

        $this->throwUnauthorized();
    }
}
