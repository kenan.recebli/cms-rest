<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\Request;

trait ListsTrashedCollection
{
    use BaseController;

    /**
     * Display a listing of the trashed resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function trash(Request $request)
    {
        if ($this->can('restore')) {
            return $this->collection::collection(
                $this->model::{$this->listFn}($request, $this->listAll, 'onlyTrashed')
            );
        }

        $this->throwUnauthorized();
    }
}
