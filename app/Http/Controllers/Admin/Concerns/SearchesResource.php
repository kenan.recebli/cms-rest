<?php

namespace App\Http\Controllers\Admin\Concerns;

use Illuminate\Http\Request;

trait SearchesResource
{
    /**
     * Search resources.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        return $this->listing::collection($this->model::search($request));
    }
}
