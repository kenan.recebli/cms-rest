<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\SortsResource;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Resources\Admin\FaqCollection;
use App\Http\Resources\Admin\FaqResource;
use App\Models\Faq;
use Astrotomic\Translatable\Validation\RuleFactory;

class FaqController extends Controller
{
    use CRUDController;
    use SortsResource;
    use TogglesResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Faq::class;

        $this->collection = FaqCollection::class;

        $this->editResource = FaqResource::class;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'is_active' => 'required|boolean',
            'translations.%question%' => 'required|string',
            'translations.%answer%' => 'required|string',
        ]);
    }
}
