<?php

namespace App\Http\Controllers\Admin;

use App\Extensions\Date;
use App\Http\Controllers\Admin\Concerns\Statistician;
use App\Http\Controllers\Controller;
use App\Models\View;
use App\Models\Visitor;
use Illuminate\Http\JsonResponse;

class StatisticsController extends Controller
{
    use Statistician;

    /**
     * Get statistics.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $date = Date::today();

        $visitors = Visitor::countByDate($date);
        $views = View::countByDate($date);

        $month = static::getLastDays(30);
        $year = static::getLastMonths(12);
        $fiveYears = static::getLastYears(5);

        $month['days'][] = $date->dm;
        $month['views'][] = $views;
        $month['visitors'][] = $visitors;

        $year['views'][11] += $views;
        $year['visitors'][11] += $visitors;

        $fiveYears['views'][4] += $views;
        $fiveYears['visitors'][4] += $visitors;

        return new JsonResponse(compact('month', 'year', 'fiveYears'));
    }
}
