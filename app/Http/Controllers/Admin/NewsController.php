<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\PostController;
use App\Http\Resources\Admin\NewsCollection;
use App\Http\Resources\Admin\NewsResource;
use App\Models\News;
use Astrotomic\Translatable\Validation\RuleFactory;

class NewsController extends Controller
{
    use PostController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = News::class;

        $this->collection = NewsCollection::class;

        $this->editResource = NewsResource::class;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'category' => 'required|integer|exists:categories,id',
            'image' => 'required|array',
            'image.id' => 'required|integer|exists:files,id',
            'is_featured' => 'nullable|boolean',
            'translations.%title%' => 'required|string|max:250',
            'translations.%description%' => 'nullable|string',
            'translations.%content%' => 'nullable|string',
            'translations.%keywords%' => 'nullable|string',
            'translations.%video%' => 'nullable|string|url',
        ]);
    }
}
