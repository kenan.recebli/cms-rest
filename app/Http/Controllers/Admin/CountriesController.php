<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\GetsList;
use App\Http\Controllers\Admin\Concerns\ListsCollection;
use App\Http\Controllers\Admin\Concerns\ShowsResource;
use App\Http\Controllers\Admin\Concerns\UpdatesResource;
use App\Http\Resources\Admin\CountryCollection;
use App\Http\Resources\Admin\CountryResource;
use App\Models\Country;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\Request;

class CountriesController extends Controller
{
    use ListsCollection;
    use ShowsResource;
    use UpdatesResource;
    use GetsList;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Country::class;

        $this->collection = CountryCollection::class;

        $this->editResource = CountryResource::class;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return RuleFactory::make([
            'code' => "required|string|unique:countries,code,$request->id",
            'translations.%name%' => 'required|string|max:250',
        ]);
    }
}
