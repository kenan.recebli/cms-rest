<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\GetsList;
use App\Http\Controllers\Admin\Concerns\ListsCollection;
use App\Http\Controllers\Admin\Concerns\UpdatesResource;
use App\Http\Resources\Admin\ContinentCollection;
use App\Http\Resources\Admin\ContinentResource;
use App\Models\Continent;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\Request;

class ContinentsController extends Controller
{
    use ListsCollection;
    use UpdatesResource;
    use GetsList;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Continent::class;

        $this->collection = ContinentCollection::class;

        $this->editResource = ContinentResource::class;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return RuleFactory::make([
            'code' => "required|string|unique:continents,code,$request->id",
            'translations.%name%' => 'required|string|max:250',
        ]);
    }
}
