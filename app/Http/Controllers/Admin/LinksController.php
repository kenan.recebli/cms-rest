<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\SortsResource;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Resources\Admin\LinkCollection;
use App\Http\Resources\Admin\LinkResource;
use App\Models\Link;

class LinksController extends Controller
{
    use CRUDController;
    use SortsResource;
    use TogglesResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Link::class;

        $this->collection = LinkCollection::class;

        $this->editResource = LinkResource::class;

        $this->listAll = true;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'image' => 'required|array',
            'image.id' => 'required|integer|exists:files,id',
            'name' => 'required|string|max:70',
            'url' => 'nullable|string|url',
        ];
    }
}
