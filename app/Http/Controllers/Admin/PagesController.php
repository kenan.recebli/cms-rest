<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Resources\Admin\PageCollection;
use App\Http\Resources\Admin\PageResource;
use App\Models\Page;
use Astrotomic\Translatable\Validation\RuleFactory;

class PagesController extends Controller
{
    use CRUDController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Page::class;

        $this->collection = PageCollection::class;

        $this->editResource = PageResource::class;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'image' => 'nullable|array',
            'image.id' => 'nullable|integer|exists:files,id',
            'translations.%title%' => 'required|string|max:250',
            'translations.%description%' => 'nullable|string',
            'translations.%content%' => 'nullable|string',
            'translations.%keywords%' => 'nullable|string',
            'translations.%file%' => 'nullable|array',
            'translations.%file.id%' => 'nullable|integer|exists:files,id',
            'translations.%video%' => 'nullable|string|url',
        ]);
    }
}
