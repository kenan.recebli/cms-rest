<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\GetsList;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Resources\Admin\ModuleCollection;
use App\Http\Resources\Admin\ModuleList;
use App\Http\Resources\Admin\ModuleResource;
use App\Models\Module;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    use CRUDController;
    use TogglesResource;
    use GetsList;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Module::class;

        $this->collection = ModuleCollection::class;

        $this->editResource = ModuleResource::class;

        $this->listing = ModuleList::class;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return [
            'key' => "required|string|max:50|unique:modules,key,$request->id",
            'is_active' => 'required|boolean',
        ];
    }
}
