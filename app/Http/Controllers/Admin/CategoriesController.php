<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\GetsList;
use App\Http\Controllers\Admin\Concerns\SortsResource;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Resources\Admin\CategoryCollection;
use App\Http\Resources\Admin\CategoryResource;
use App\Models\Category;
use Astrotomic\Translatable\Validation\RuleFactory;

class CategoriesController extends Controller
{
    use CRUDController;
    use TogglesResource;
    use SortsResource;
    use GetsList;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Category::class;

        $this->collection = CategoryCollection::class;

        $this->editResource = CategoryResource::class;

        $this->listAll = true;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'category' => 'nullable|integer|exists:categories,id',
            'is_active' => 'required|boolean',
            'translations.%name%' => 'required|string|max:70',
            'translations.%description%' => 'nullable|string|max:160',
        ]);
    }
}
