<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\PostController;
use App\Http\Resources\Admin\ArticleCollection;
use App\Http\Resources\Admin\ArticleResource;
use App\Models\Article;
use Astrotomic\Translatable\Validation\RuleFactory;

class ArticlesController extends Controller
{
    use PostController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Article::class;

        $this->collection = ArticleCollection::class;

        $this->editResource = ArticleResource::class;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'image' => 'required|array',
            'image.id' => 'required|integer|exists:files,id',
            'category' => 'required|integer|exists:categories,id',
            'is_featured' => 'nullable|boolean',
            'translations.%title%' => 'required|string|max:250',
            'translations.%description%' => 'nullable|string',
            'translations.%content%' => 'nullable|string',
            'translations.%keywords%' => 'nullable|string',
            'translations.%video%' => 'nullable|string|url',
        ]);
    }
}
