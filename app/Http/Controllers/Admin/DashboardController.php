<?php

namespace App\Http\Controllers\Admin;

use App\Extensions\Date;
use App\Http\Controllers\Admin\Concerns\Statistician;
use App\Models\MonthStatistics;
use App\Models\View;
use App\Models\Visitor;
use App\Models\WeekStatistics;
use App\Models\YearStatistics;
use Illuminate\Http\JsonResponse;

class DashboardController extends Controller
{
    use Statistician;

    /**
     * Show the dashboard.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $date = Date::yesterday();

        $week = WeekStatistics::fetch($date);
        $month = MonthStatistics::fetch($date);
        $year = YearStatistics::fetch($date);

        $lastTenDays = static::getLastDays(10);

        $date = $date->addDay();

        $visitors = Visitor::countByDate($date);
        $views = View::countByDate($date);

        $lastTenDays['days'][] = $date->dm;
        $lastTenDays['visitors'][] = $visitors;
        $lastTenDays['views'][] = $views;

        $total = YearStatistics::getTotalCount();

        return new JsonResponse([
            'last_ten_days' => $lastTenDays,
            'online' => Visitor::online(),
            'visitors' => [
                'today' => number_format($visitors),
                'yesterday' => number_format($lastTenDays['visitors'][8]),
                'this_week' => number_format($week?->visitors + $visitors ?: 0),
                'this_month' => number_format($month?->visitors + $visitors ?: 0),
                'this_year' => number_format($year?->visitors + $visitors ?: 0),
                'total' => number_format($total->visitors + $visitors),
            ],
            'views' => [
                'today' => number_format($views),
                'yesterday' => number_format($lastTenDays['views'][8]),
                'this_week' => number_format($week?->views + $views ?: 0),
                'this_month' => number_format($month?->views + $views ?: 0),
                'this_year' => number_format($year?->views + $views ?: 0),
                'total' => number_format($total->views + $views),
            ],
        ]);
    }
}
