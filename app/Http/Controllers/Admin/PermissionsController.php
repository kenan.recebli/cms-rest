<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Resources\Admin\PermissionCollection;
use App\Http\Resources\Admin\PermissionResource;
use App\Models\Permission;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PermissionsController extends Controller
{
    use CRUDController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Permission::class;

        $this->collection = PermissionCollection::class;

        $this->editResource = PermissionResource::class;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return [
            'module' => [
                'required',
                'integer',
                'exists:permissions,id',
                Rule::unique('permissions', 'module_id')
                    ->ignore($request->id)
                    ->where(function (Builder $query) use ($request) {
                        return $query->where('module_id', $request->module)
                                     ->where('action', Permission::getActionCode($request->action));
                    }),
            ],
            'action' => 'required|in:'.Permission::getActions(),
        ];
    }

    /**
     * Get permission actions.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function actions(): JsonResponse
    {
        return new JsonResponse(array_values(Permission::$actions));
    }
}
