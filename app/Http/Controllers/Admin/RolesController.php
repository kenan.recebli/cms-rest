<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Requests\SavePermissions;
use App\Http\Resources\Admin\Listing;
use App\Http\Resources\Admin\ModulePermissionsCollection;
use App\Http\Resources\Admin\RoleCollection;
use App\Http\Resources\Admin\RoleList;
use App\Http\Resources\Admin\RoleResource;
use App\Models\Module;
use App\Models\Role;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class RolesController extends Controller
{
    use CRUDController;
    use TogglesResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Role::class;

        $this->collection = RoleCollection::class;

        $this->editResource = RoleResource::class;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return RuleFactory::make([
            'key' => "nullable|string|max:50|unique:roles,key,$request->id",
            'is_admin' => 'nullable|boolean',
            'is_active' => 'nullable|boolean',
            'translations.%name%' => 'required|string|max:70',
            'translations.%description%' => 'nullable|string',
        ]);
    }

    /**
     * Get a list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function get(Request $request): AnonymousResourceCollection
    {
        return Listing::collection(Role::getAll($request));
    }

    /**
     * List permissions.
     *
     * @param \App\Models\Role $role
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listPermissions(Role $role): JsonResponse
    {
        if ($role->allowed()) {
            return new JsonResponse([
                'modules' => ModulePermissionsCollection::collection(Module::getPermissions($role->id)),
                'role' => new RoleList($role),
            ]);
        }

        $this->throwUnauthorized();
    }

    /**
     * Save permissions.
     *
     * @param \App\Http\Requests\SavePermissions $request
     * @param \App\Models\Role                   $role
     *
     * @return array
     */
    public function savePermissions(SavePermissions $request, Role $role): array
    {
        $permissions = [];

        foreach ($request->all() as $module) {
            $permissions = collect($module['permissions'])
                ->where('can', true)
                ->pluck('id')
                ->merge($permissions);
        }

        return $role->permissions()->sync($permissions);
    }
}
