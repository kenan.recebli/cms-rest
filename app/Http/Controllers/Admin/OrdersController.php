<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\ListsCollection;
use App\Http\Controllers\Admin\Concerns\ShowsResource;
use App\Http\Resources\Admin\OrderCollection;
use App\Http\Resources\Admin\OrderResource;
use App\Models\Order;

class OrdersController extends Controller
{
    use ListsCollection;
    use ShowsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Order::class;

        $this->collection = OrderCollection::class;

        $this->showResource = OrderResource::class;
    }
}
