<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\DeletesResource;
use App\Http\Controllers\Admin\Concerns\ListsCollection;
use App\Http\Controllers\Admin\Concerns\ShowsResource;
use App\Http\Resources\Admin\FeedbackCollection;
use App\Http\Resources\Admin\FeedbackResource;
use App\Models\Feedback;

class FeedbackController extends Controller
{
    use ListsCollection;
    use ShowsResource;
    use DeletesResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Feedback::class;

        $this->collection = FeedbackCollection::class;

        $this->showResource = FeedbackResource::class;
    }

    /**
     * Do some action while showing the resource.
     *
     * @param \App\Models\Feedback $model
     *
     * @return void
     */
    public function preShow(Feedback $model): void
    {
        if (! $model->seen_at) {
            $model->markAsSeen();
        }
    }
}
