<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\ShowsResource;
use App\Http\Controllers\Admin\Concerns\UpdatesResource;
use App\Http\Requests\MoveFiles;
use App\Http\Requests\UploadFile;
use App\Http\Resources\Admin\FileCollection;
use App\Http\Resources\Admin\FileResource;
use App\Models\File;
use App\Models\Folder;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FilesController extends Controller
{
    use ShowsResource;
    use UpdatesResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = File::class;

        $this->editResource = FileResource::class;
    }

    /**
     * Generate validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'translations.%name%' => 'required|string|max:250',
            'translations.%alt%' => 'nullable|string|max:250',
        ]);
    }

    /**
     * Upload a file.
     *
     * @param \App\Http\Requests\UploadFile $request
     *
     * @return \App\Http\Resources\Admin\FileCollection|\Illuminate\Http\JsonResponse
     */
    public function store(UploadFile $request)
    {
        $uploadedFile = $request->file('file');

        if (! $uploadedFile->isValid()) {
            return new JsonResponse(['error' => __('files.invalid_file')], 422);
        }

        $filename = unique_name();
        $clientName = $uploadedFile->getClientOriginalName();
        $extension = $uploadedFile->getClientOriginalExtension();
        $mimeType = $uploadedFile->getClientMimeType();
        $size = $uploadedFile->getSize();

        $is_image = Str::startsWith($mimeType, 'image') && $extension != 'svg';
        $is_video = ! $is_image && Str::startsWith($mimeType, 'video');
        $is_audio = ! ($is_image || $is_video) && Str::startsWith($mimeType, 'audio');

        $disk = guess_disk_name($is_image, $is_video, $is_audio);
        $name = $extension ? substr($clientName, 0, -(strlen($extension) + 1)) : $clientName;
        $data = compact('filename', 'extension', 'size', 'is_image', 'is_video', 'is_audio');

        foreach (config('translatable.locales') as $locale) {
            $data[$locale] = compact('name');

            if ($is_image) {
                $data[$locale]['alt'] = $name;
            }
        }

        try {
            if ($folder = $request->folder) {
                $folder = Folder::find($request->folder);
            }

            $file = $folder
                ? $folder->files()->create($data)
                : File::create($data);

            $uploadedFile->storeAs('', $file->file, $disk);

            if ($file) {
                $file->setDimensionsAndDuration();
            }

            return new FileCollection($file);
        } catch (QueryException) {
            return new JsonResponse(['error' => __('files.unexpected_error'), 500]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\File|null    $file
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function destroy(Request $request, File $file = null): JsonResponse
    {
        if ($file) {
            try {
                return new JsonResponse([
                    'success' => $file->delete(),
                ]);
            } catch (QueryException) {
                return new JsonResponse([
                    'error' => __('Cannot delete or update a parent row'),
                ]);
            }
        } else {
            return new JsonResponse([
                'success' => File::batchDelete($request->get('files')),
            ]);
        }
    }

    /**
     * Move files and folders.
     *
     * @param \App\Http\Requests\MoveFiles $request
     * @param \App\Models\Folder|null      $folder
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function move(MoveFiles $request, Folder $folder = null): JsonResponse
    {
        return new JsonResponse([
            'success' => File::move($request->get('files'), $folder?->id),
        ]);
    }
}
