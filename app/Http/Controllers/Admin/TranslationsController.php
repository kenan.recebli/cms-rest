<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Resources\Admin\TranslationResource;
use App\Models\Translation;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Finder\Finder;

class TranslationsController extends Controller
{
    use CRUDController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Translation::class;

        $this->collection = TranslationResource::class;

        $this->editResource = TranslationResource::class;

        $this->listAll = true;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'key' => 'required|string|max:50',
            'translations.%value%' => 'nullable|string',
        ]);
    }

    /**
     * Import translations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function import(): JsonResponse
    {
        $this->storeTranslations('components_path');
        $this->storeTranslations('pages_path');

        return $this->response(true);
    }

    /**
     * Publish translations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(): JsonResponse
    {
        $translations = [];

        foreach (Translation::all() as $model) {
            foreach ($model->translations as $translation) {
                $translations[$translation->locale_id][$model->key] = $translation->value;
            }
        }

        foreach ($translations as $locale => $lines) {
            file_put_contents(lang_path("$locale.json"), json_encode($lines));
        }

        return $this->response(true);
    }

    /**
     * Temporary todo
     *
     * @param string $path
     *
     * @return array
     */
    public function findTranslations(string $path): array
    {
        $path = config("filesystems.$path");
        $keys = [];
        $functions = [
            '\$t',
            '\$tc',
        ];

        $pattern =
            "[^\w]".                                       // Must not have an alphanum before real method
            '('.implode('|', $functions).')'.              // Must start with one of the functions
            "\(\s*".                                       // Match opening parenthesis
            "(?P<quote>['\"])".                            // Match " or ' and store in {quote}
            "(?P<string>(?:\\\k{quote}|(?!\k{quote}).)*)". // Match any string that can be {quote} escaped
            "\k{quote}".                                   // Match " or ' previously matched
            "\s*[\),]";                                    // Close parentheses or new parameter

        foreach ((new Finder)->in($path)->files() as $file) {
            if (preg_match_all("/$pattern/siU", $file->getContents(), $matches)) {
                foreach ($matches['string'] as $key) {
                    $keys[] = $key;
                }
            }
        }

        return array_unique($keys);
    }

    /**
     * Store translations to database.
     *
     * @param string $path
     *
     * @return void
     */
    public function storeTranslations(string $path): void
    {
        foreach ($this->findTranslations($path) as $key) {
            $translation = Translation::firstWhere($data = compact('key'));

            if (! $translation) {
                foreach (config('translatable.locales') as $locale) {
                    $data[$locale] = [
                        'value' => $key,
                    ];
                }

                Translation::create($data);
            }
        }
    }
}
