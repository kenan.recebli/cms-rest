<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Resources\Admin\MeResource;
use App\Http\Resources\Admin\UserCollection;
use App\Http\Resources\Admin\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    use CRUDController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = User::class;

        $this->collection = UserCollection::class;

        $this->editResource = UserResource::class;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        $rule = $request->profile
            ? 'nullable'
            : 'required';

        return [
            'email' => "required|string|email|max:50|unique:users,email,$request->id",
            'first_name' => 'required|string|max:50',
            'last_name' => 'nullable|string|max:50',
            'current_password' => 'required|current_password:api',
            'password' => ($request->method() == 'PUT' ? 'nullable' : 'required').'|string|min:8',
            'confirm' => 'required_with:password|string|min:8|same:password',
            'roles' => "$rule|array",
            'roles.*' => "$rule|integer|exists:roles,id",
        ];
    }

    /**
     * Check if the user can perform the action.
     *
     * @param string                $action
     * @param \App\Models\User|null $model
     *
     * @return bool
     */
    public function can(string $action, User $model = null): bool
    {
        $user = Auth::user();

        return ($model && $model->is($user)) ?: $user->can($action, $model ?: $this->model);
    }

    /**
     * Save roles of the user.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User         $user
     *
     * @return void
     */
    public function postSave(Request $request, User $user): void
    {
        if (! $request->profile) {
            $user->roles()->sync($request->roles);
        }
    }

    /**
     * Do some action after deleting the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User         $user
     *
     * @return void
     */
    public function postDelete(Request $request, User $user): void
    {
        //
    }

    /**
     * Get info of the authenticated user.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Http\Resources\Admin\MeResource
     */
    public function info(Request $request): MeResource
    {
        return new MeResource($request->user());
    }
}
