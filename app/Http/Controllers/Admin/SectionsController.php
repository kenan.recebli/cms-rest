<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Resources\Admin\SectionCollection;
use App\Http\Resources\Admin\SectionResource;
use App\Models\Section;
use Astrotomic\Translatable\Validation\RuleFactory;

class SectionsController extends Controller
{
    use CRUDController;
    use TogglesResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Section::class;

        $this->collection = SectionCollection::class;

        $this->editResource = SectionResource::class;

        $this->devActions = [
            'create',
            'delete',
            'toggle',
        ];
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'is_active' => 'nullable|boolean',
            'translations.%title%' => 'required|string|max:250',
            'translations.%description%' => 'nullable|string',
            'translations.%keywords%' => 'nullable|string',
        ]);
    }
}
