<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\SearchesResource;
use App\Http\Resources\Admin\TagCollection;
use App\Http\Resources\Admin\TagResource;
use App\Models\Tag;

class TagsController extends Controller
{
    use CRUDController;
    use SearchesResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Tag::class;

        $this->collection = TagCollection::class;

        $this->editResource = TagResource::class;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'locale' => 'required|string|size:2|exists:locales,id',
            'name' => 'required|string|max:250',
        ];
    }
}
