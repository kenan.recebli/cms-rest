<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\SortsResource;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Resources\Admin\Listing;
use App\Http\Resources\Admin\MenuCollection;
use App\Http\Resources\Admin\MenuResource;
use App\Models\Category;
use App\Models\Menu;
use App\Models\Page;
use App\Models\Section;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class MenusController extends Controller
{
    use CRUDController;
    use TogglesResource;
    use SortsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Menu::class;

        $this->collection = MenuCollection::class;

        $this->editResource = MenuResource::class;

        $this->listFn = 'getParents';
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'menu' => 'nullable|integer|exists:menus,id',
            'is_external' => 'nullable|boolean',
            'is_active' => 'required|boolean',
            'translations.%name%' => 'required|string|max:250',
            'translations.%link%' => 'nullable|string',
        ]);
    }

    /**
     * Get menus.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function get(): AnonymousResourceCollection
    {
        return Listing::collection(Menu::getParents());
    }

    /**
     * Get site links.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function links(): JsonResponse
    {
        $links = [];

        foreach (config('translatable.locales') as $locale) {
            $links[$locale]['general'][] = [
                'text' => __('Home', [], $locale),
                'url' => localize_route('homepage', $locale),
            ];

            $this->getLinks($links, Section::class, $locale);
            $this->getLinks($links, Category::class, $locale, 'name');
            $this->getLinks($links, Page::class, $locale);
        }

        return new JsonResponse($links);
    }

    /**
     * Get item links.
     *
     * @param array  $links
     * @param string $model
     * @param string $locale
     * @param string $key
     *
     * @return void
     */
    public function getLinks(array &$links, string $model, string $locale, string $key = 'title')
    {
        foreach ($model::all() as $item) {
            if ($item->key == 'error') {
                continue;
            }

            $links[$locale][$item->getTable()][] = [
                'text' => $item->translate($locale)->$key ?? $item->$key,
                'url' => $item->url($locale),
            ];
        }
    }
}
