<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\PostController;
use App\Http\Resources\Admin\ProductCollection;
use App\Http\Resources\Admin\ProductResource;
use App\Models\Product;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    use PostController;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Product::class;

        $this->collection = ProductCollection::class;

        $this->editResource = ProductResource::class;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return RuleFactory::make([
            'image' => 'required|array',
            'image.id' => 'required|integer|exists:files,id',
            'category' => 'required|integer|exists:categories,id',
            'price' => 'required|numeric|min:0',
            'is_featured' => 'nullable|boolean',
            'has_discount' => 'nullable|boolean',
            'discount.amount' => 'nullable|numeric|min:0',
            'discount.unit' => 'nullable|required_with:discount.amount|integer|in:1,2',
            'discount.start' => 'nullable|date'.(request()->method() == 'PUT' ? '' : '|after:today'),
            'discount.end' => 'nullable|date|after:discount.start',
            'translations.%title%' => 'required|string|max:250',
            'translations.%description%' => 'nullable|string',
            'translations.%content%' => 'nullable|string',
            'translations.%keywords%' => 'nullable|string',
            'translations.%video%' => 'nullable|string|url',
        ]);
    }

    /**
     * Save resource's extra data.
     *
     * @return void
     */
    public function postSave(Request $request, Product $product): void
    {
        if ($request->has_discount) {
            $discount = $product->discountAdmin();

            if ($product->discountAdmin) {
                $discount->update($request->discount);
            } else {
                $discount->create($request->discount);
            }
        }
    }
}
