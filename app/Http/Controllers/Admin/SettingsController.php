<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\TogglesResource;
use App\Http\Requests\SaveSettings;
use App\Http\Resources\Admin\FileCollection;
use App\Http\Resources\Admin\SettingCollection;
use App\Http\Resources\Admin\SettingResource;
use App\Http\Resources\SocialNetworkResource;
use App\Models\Feedback;
use App\Models\Setting;
use App\Models\SocialNetwork;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    use CRUDController;
    use TogglesResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Setting::class;

        $this->collection = SettingCollection::class;

        $this->editResource = SettingResource::class;
    }

    /**
     * Generate validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return [
            'key' => "required|string|unique:settings,key,$request->key,key",
            'file' => 'nullable|integer|exists:files,id',
            'is_file' => 'nullable|boolean',
            'is_array' => 'nullable|boolean',
            'is_for_dev' => 'nullable|boolean',
            'is_contact' => 'nullable|boolean',
            'has_trans' => 'nullable|boolean',
        ];
    }

    /**
     * Manage settings.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function manage(): JsonResponse
    {
        $settings = [];
        $networks = [];

        foreach (Setting::all() as $setting) {
            $settings[$setting->key] = $setting->file_id
                ? new FileCollection($setting->file)
                : $setting->admin_value;
        }

        foreach (SocialNetwork::all() as $network) {
            $networks[$network->key] = new SocialNetworkResource($network);
        }

        return new JsonResponse(compact('settings', 'networks'));
    }

    /**
     * Save settings.
     *
     * @param \App\Http\Requests\SaveSettings $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(SaveSettings $request): JsonResponse
    {
        $response = [];

        if ($settings = $request->settings) {
            $response['settings'] = Setting::change($request->user(), $settings);
        }

        if ($networks = $request->networks) {
            $response['networks'] = SocialNetwork::change($networks);
        }

        return new JsonResponse($response);
    }

    /**
     * Get settings.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(): JsonResponse
    {
        return new JsonResponse(
            Setting::getAll()
                   ->put('site_url', localize(config('app.site_url')))
                   ->put('feedback_count', Feedback::countUnseen())
        );
    }
}
