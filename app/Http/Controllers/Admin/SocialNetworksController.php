<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Concerns\CRUDController;
use App\Http\Controllers\Admin\Concerns\SortsResource;
use App\Http\Resources\Admin\SocialNetworkCollection;
use App\Http\Resources\Admin\SocialNetworkResource;
use App\Models\SocialNetwork;
use Illuminate\Http\Request;

class SocialNetworksController extends Controller
{
    use CRUDController;
    use SortsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = SocialNetwork::class;

        $this->collection = SocialNetworkCollection::class;

        $this->editResource = SocialNetworkResource::class;

        $this->listAll = true;
    }

    /**
     * Get the validation rules.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function rules(Request $request): array
    {
        return [
            'key' => "required|string|unique:social_networks,key,$request->key,key",
            'name' => "required|string|unique:social_networks,name,$request->name,name",
            'icon' => "required|string|unique:social_networks,icon,$request->key,icon",
            'link' => 'nullable|string|url',
            'is_active' => 'required|boolean',
        ];
    }
}
