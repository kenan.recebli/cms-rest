<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Concerns\ShowsResource;
use App\Http\Resources\CategoryResource;
use App\Models\Category;

class CategoriesController extends Controller
{
    use ShowsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Category::class;

        $this->resource = CategoryResource::class;
    }
}
