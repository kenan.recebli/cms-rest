<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Concerns\ShowsResource;
use App\Http\Resources\PageResource;
use App\Models\Page;

class PagesController extends Controller
{
    use ShowsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Page::class;

        $this->resource = PageResource::class;
    }
}
