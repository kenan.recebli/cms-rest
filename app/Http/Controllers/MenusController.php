<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Concerns\ListsCollection;
use App\Http\Resources\MenuCollection;
use App\Models\Menu;

class MenusController extends Controller
{
    use ListsCollection;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = Menu::class;

        $this->collection = MenuCollection::class;

        $this->listFn = 'getParents';
    }
}
