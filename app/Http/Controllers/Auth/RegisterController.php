<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Admin\Concerns\ReturnsResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Register;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use ReturnsResponse;

    /**
     * Handle a registration request for the application.
     *
     * @param \App\Http\Requests\Register $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Register $request): JsonResponse
    {
        event(new Registered($user = $this->create($request)));

        return $this->response((bool) $user);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param \App\Http\Requests\Register $request
     *
     * @return \App\Models\User
     */
    protected function create(Register $request): User
    {
        return User::create($request->all());
    }
}
