<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Concerns\ListsCollection;
use App\Http\Controllers\Concerns\ShowsResource;
use App\Http\Resources\NewsCollection;
use App\Http\Resources\NewsResource;
use App\Models\News;

class NewsController extends Controller
{
    use ListsCollection;
    use ShowsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->section = 'news';

        $this->model = News::class;

        $this->collection = NewsCollection::class;

        $this->resource = NewsResource::class;
    }
}
