<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Concerns\ListsCollection;
use App\Http\Controllers\Concerns\ShowsResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductsController extends Controller
{
    use ListsCollection;
    use ShowsResource;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->section = 'products';

        $this->model = Product::class;

        $this->collection = ProductCollection::class;

        $this->resource = ProductResource::class;
    }
}
