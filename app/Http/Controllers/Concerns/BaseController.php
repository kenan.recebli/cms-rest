<?php

namespace App\Http\Controllers\Concerns;

use Illuminate\Database\Eloquent\Model;

trait BaseController
{
    /**
     * @var string|null
     */
    public ?string $model = null;

    /**
     * @var string|null
     */
    public ?string $collection = null;

    /**
     * @var string|null
     */
    public ?string $resource = null;

    /**
     * @var string|null
     */
    public ?string $section = null;

    /**
     * @var string
     */
    public string $listFn = 'listForSite';

    /**
     * Indicates whether all resources should be listed.
     *
     * @var bool
     */
    public bool $listAll = false;

    /**
     * @var string|null
     */
    public string $getFn = 'getBySlug';

    /**
     * Get resource and throw 404 if not found.
     *
     * @param int|string $id
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function get_resource(int|string $id): ?Model
    {
        abort_unless($resource = $this->model::{$this->getFn}($id), 404);

        return $resource;
    }
}
