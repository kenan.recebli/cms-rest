<?php

namespace App\Http\Controllers\Concerns;

use App\Http\Resources\SectionResource;
use App\Models\Section;
use Illuminate\Http\Request;

trait ListsCollection
{
    use BaseController;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $data = $this->collection::collection(
            $this->model::{$this->listFn}($request, $this->listAll)
        );

        if ($this->section) {
            $data = $data->additional([
                'section' => new SectionResource(Section::getByKey($this->section)),
            ]);
        }

        return $data;
    }
}
