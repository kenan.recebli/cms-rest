<?php

namespace App\Http\Controllers\Concerns;

use Illuminate\Database\Eloquent\Model;

trait ShowsResource
{
    use BaseController;

    /**
     * Do some action before showing the resource.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function preShow(Model $model): void
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int|string $id
     *
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(int|string $id)
    {
        $this->preShow($resource = $this->get_resource($id));

        return new $this->resource($resource);
    }
}
