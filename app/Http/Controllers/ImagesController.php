<?php

namespace App\Http\Controllers;

use App\Models\File;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ImagesController extends Controller
{
    /**
     * Allowed thumbnail dimensions.
     *
     * @var array
     */
    private static array $dimensions = ['*'];

    /**
     * Get the current application locale.
     *
     * @var string
     */
    private string $locale;

    /**
     * ImagesController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->locale = app()->getLocale();
    }

    /**
     * Show the image in the original size.
     *
     * @param string $image
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function original(string $image): BinaryFileResponse
    {
        $file = static::get("$this->locale:$image", $date = Carbon::now()->addMonth(), $image);

        return static::render(response()->file($file->path), $date);
    }

    /**
     * Generate thumbnail for the image.
     *
     * @param string $dimensions
     * @param string $image
     *
     * @return \Illuminate\Http\Response
     */
    public function thumbnail(string $dimensions, string $image): Response
    {
        abort_unless(
            in_array($size = $dimensions, static::$dimensions) or static::$dimensions == ['*'],
            404
        );

        ini_set('memory_limit', '-1');

        $file = static::get($key = "$this->locale:$image", $date = Carbon::now()->addMonth(), $image);

        $image = Cache::remember("$key:$size", $date, function () use ($file, $dimensions) {
            $image = Image::make($file->path);

            [$width, $height] = explode('x', $dimensions);

            $image->resize($width, $height, function (Constraint $constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $content = $image->encode();

            $image->destroy();

            return $content;
        });

        return static::render(response($image, 200, ['Content-Type' => $file->mimeType]), $date);
    }

    /**
     * Get the image and cache it if not already cached.
     *
     * @param string         $key
     * @param \Carbon\Carbon $date
     * @param string         $file
     *
     * @return \App\Models\File|null
     */
    private static function get(string $key, Carbon $date, string $file): ?File
    {
        $file = Cache::remember($key, $date, function () use ($file) {
            return File::getByName($file);
        });

        abort_unless($file && $file->exists(), 404);

        return $file;
    }

    /**
     * Render the image response.
     *
     * @param \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse $response
     * @param \Carbon\Carbon                                                                 $date
     *
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    private static function render(Response|BinaryFileResponse $response, Carbon $date)
    {
        return $response->setExpires($date->addYear())
                        ->setMaxAge(31536000)
                        ->setPublic();
    }
}
