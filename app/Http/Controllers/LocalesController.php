<?php

namespace App\Http\Controllers;

use App\Http\Resources\LocaleCollection;
use App\Models\Locale;
use Illuminate\Http\JsonResponse;

class LocalesController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        return new JsonResponse(
            json_decode(
                file_get_contents(
                    lang_path("$id.json")
                )
            )
        );
    }

    /**
     * Get locales.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function get()
    {
        return LocaleCollection::collection(Locale::all());
    }
}
