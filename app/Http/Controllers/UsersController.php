<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUser;
use App\Http\Resources\Admin\MeResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Get the authenticated user's info.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Http\Resources\Admin\MeResource|\Illuminate\Http\JsonResponse
     */
    public function info(Request $request)
    {
        return ($user = $request->user())
            ? new MeResource($user)
            : new JsonResponse([
                'error' => 'Unauthenticated user',
            ], 403);
    }

    /**
     * Get user for edit.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Http\Resources\UserResource
     */
    public function get(Request $request): UserResource
    {
        return new UserResource($request->user());
    }

    /**
     * Update user info.
     *
     * @param \App\Http\Requests\UpdateUser $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUser $request): JsonResponse
    {
        return new JsonResponse([
            'success' => $request->user()->update($request->all()),
        ]);
    }
}
