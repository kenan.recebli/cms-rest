<?php

use App\Http\Resources\Admin\FileResource;
use Carbon\Carbon;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Http\Resources\Json\JsonResource;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

if (! function_exists('localize')) {
    /**
     * Get the URL adapted to locale.
     *
     * @param string|null $url
     * @param string|null $locale
     * @param array       $attributes
     *
     * @return false|string
     */
    function localize(?string $url, string $locale = null, array $attributes = []): bool|string
    {
        return LaravelLocalization::getLocalizedUrl($locale ?: app()->getLocale(), $url, $attributes);
    }
}

if (! function_exists('localize_route')) {
    /**
     * Localize the giving route.
     *
     * @param string $route
     * @param string $locale
     *
     * @return false|string
     */
    function localize_route(string $route, string $locale): bool|string
    {
        return rtrim("/$locale/".__("routes.$route", [], $locale), '/');
    }
}

if (! function_exists('resource_translations')) {
    /**
     * Generate the resource translations.
     *
     * @param \Illuminate\Http\Resources\Json\JsonResource $resource
     * @param array                                        $object
     * @param array                                        $attributes
     *
     * @return void
     */
    function resource_translations(JsonResource $resource, array &$object, array $attributes): void
    {
        foreach (config('translatable.locales') as $locale) {
            $translation = $resource->translate($locale);

            foreach ($attributes as $attribute) {
                $object['translations'][$locale][$attribute] = $translation
                    ? ($attribute == 'file' ? $translation->file_item : $translation->$attribute)
                    : null;
            }

            if ($resource instanceof FileResource) {
                $object['translations'][$locale]['url'] = $translation
                    ? localize($resource->raw_url, $locale)
                    : null;
            }
        }
    }
}

if (! function_exists('search_resources')) {
    /**
     * Search resources.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $locale
     * @param string|null                           $q
     *
     * @return void
     */
    function search_resources($query, string $locale, string $q = null): void
    {
        if ($q) {
            $query->whereTranslationLike('title', "%$q%", $locale)
                  ->orWhereTranslationLike('description', "%$q%", $locale)
                  ->orWhereTranslationLike('content', "%$q%", $locale);
        }
    }
}

if (! function_exists('get_location')) {
    /**
     * Get location by the IP.
     *
     * @param string $ip
     *
     * @return object
     */
    function get_location(string $ip): object
    {
        function default_location(): object
        {
            return (object) [
                'region' => 'Baki',
                'country' => 'AZ',
            ];
        }

        try {
            return $ip == '127.0.0.1'
                ? default_location()
                : json_decode(file_get_contents("https://ipinfo.io/$ip/geo"));
        } catch (Exception) {
            return default_location();
        }
    }
}

if (! function_exists('timeago')) {
    /**
     * Get the difference in a human readable format in the current locale.
     *
     * @param \Carbon\Carbon $time
     *
     * @return \Illuminate\Contracts\Translation\Translator|string
     */
    function timeago(Carbon $time): string|Translator
    {
        if (($diff = time() - ($time = $time->timestamp)) <= 19) {
            return __('datetime.just_now');
        }

        if ($diff > 19 && $diff <= 59) {
            return __('datetime.few_seconds_ago');
        }

        $int = [
            $diff < 604800 => ['day', 86400],
            $diff < 86400 => ['hour', 3600],
            $diff < 3600 => ['minute', 60],
        ];

        if (isset($int[1])) {
            $value = floor($diff / $int[1][1]);
            $age = $int[1][0];

            if ($value < 2 && $age == 'day') {
                return __('datetime.yesterday_at').' '.date('H:i', $time);
            }

            if ($value > 1 && $value < 8 && $age == 'day') {
                return __('datetime.'.date('l', $time)).' '.date('H:i', $time);
            }

            if ($value <= 59 && $age != 'day') {
                return $value.' '.trans_choice('datetime.'.$age, $value).' '.__('datetime.ago');
            }
        }

        return date('j', $time).' '.__('datetime.'.date('M', $time)).' '.date('Y H:i', $time);
    }
}

if (! function_exists('guess_disk_name')) {
    /**
     * Guess the storage disk name.
     *
     * @param bool $isImage
     * @param bool $isVideo
     * @param bool $isAudio
     *
     * @return string
     */
    function guess_disk_name(bool $isImage, bool $isVideo, bool $isAudio): string
    {
        return match (true) {
            $isImage => 'images',
            $isVideo => 'videos',
            $isAudio => 'audios',
            default => 'files',
        };
    }
}

if (! function_exists('format_bytes')) {
    /**
     * Format bytes.
     *
     * @param int $bytes
     * @param int $precision
     *
     * @return string
     */
    function format_bytes(int $bytes, int $precision = 2): string
    {
        if ($bytes < 1024) {
            return $bytes.' '.trans_choice('files.B', $bytes);
        }

        $units = ['', 'KB', 'MB', 'GB', 'TB'];
        $index = floor($base = log($bytes, 1024));

        return round(pow(1024, $base - $index), $precision).' '.__('files.'.$units[(int) $index]);
    }
}

if (! function_exists('dec2roman')) {
    /**
     * Convert decimal number to roman numeral.
     *
     * @param string $num
     *
     * @return string
     */
    function dec2roman(string $num): string
    {
        $n = (int) $num;
        $res = '';

        $romanNumerals = [
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1,
        ];

        foreach ($romanNumerals as $roman => $number) {
            $multiplier = (int) ($n / $number);

            $res .= str_repeat($roman, $multiplier);

            $n %= $number;
        }

        return $res;
    }
}

if (! function_exists('unique_name')) {
    /**
     * Generate a unique name.
     *
     * @return string
     */
    function unique_name(): string
    {
        return md5(uniqid(mt_rand()));
    }
}

if (! function_exists('get_site_url')) {
    /**
     * Get the site URL.
     *
     * @param string $uri
     *
     * @return string
     */
    function get_site_url($uri)
    {
        return config('app.site_url').$uri;
    }
}
