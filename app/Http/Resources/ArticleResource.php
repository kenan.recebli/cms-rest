<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'image' => $this->image->item,
            'images' => $this->images_urls,
            'title' => $this->title,
            'description' => $this->description,
            'content' => $this->content,
            'slugs' => $this->slugs,
            // 'views_count' => $this->views_count,
            'created_at' => $this->created_at,
            'category' => new CategoryResource($this->category),
        ];
    }
}
