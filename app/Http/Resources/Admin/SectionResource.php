<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class SectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $section = [
            'id' => $this->id,
            'key' => $this->key,
            'image' => $this->image_item,
            'is_active' => $this->is_active,
        ];

        resource_translations($this, $section, [
            'title',
            'description',
            'keywords',
        ]);

        return $section;
    }
}
