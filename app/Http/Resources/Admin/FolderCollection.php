<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class FolderCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'parent' => $this->folder_id,
            'name' => $this->name,
            'folders_count' => $this->countFolders(),
            'files_count' => $this->countFiles(),
            'files_size' => format_bytes($this->calculateFilesSize()),
        ];
    }
}
