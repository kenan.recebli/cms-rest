<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $discount = $this->amount - $this->gain;

        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'amount' => $this->amount,
            'discount' => $discount ? number_format($discount, 2) : null,
            'with_discount' => (bool) $discount,
            'gain' => $this->gain,
            'payment_type' => $this->payment_type,
            'note' => $this->note,
            'status' => $this->status,
            'products' => OrderProductResource::collection($this->products),
        ];
    }
}

