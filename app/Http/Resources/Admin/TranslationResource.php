<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class TranslationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $translation = [
            'id' => $this->id,
            'key' => $this->key,
        ];

        resource_translations($this, $translation, [
            'value',
        ]);

        return $translation;
    }
}
