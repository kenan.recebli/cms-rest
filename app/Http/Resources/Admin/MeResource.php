<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class MeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'fullname' => $this->fullname,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'is_developer' => $this->is_developer,
            'is_ceo' => $this->is_ceo,
            'is_superuser' => $this->is_superuser,
            'roles' => RoleList::collection($this->roles),
            'permissions' => $this->is_developer
                ? ['*']
                : $this->permissions,
        ];
    }
}
