<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class TranslationCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $translations = [
            'id' => $this->id,
            'key' => $this->key,
        ];

        foreach ($this->translations as $translation) {
            $translations['translations'][$translation->locale_id] = $translation->value;
        }

        return $translations;
    }
}
