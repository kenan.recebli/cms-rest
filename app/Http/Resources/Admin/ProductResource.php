<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $product = [
            'id' => $this->id,
            'category' => $this->category_id,
            'image' => $this->image_item,
            'images' => $this->image_items,
            'price' => $this->price,
            'is_featured' => $this->is_featured,
            'is_trashed' => (bool) $this->deleted_at,
            'has_discount' => $this->has_discount,
            'discount' => $this->discountAdmin
                ? new DiscountResource($this->discountAdmin)
                : (object) [],
        ];

        resource_translations($this, $product, [
            'title',
            'description',
            'content',
            'video',
        ]);

        return $product;
    }
}
