<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class UserList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $role = $this->roles->first();

        return [
            'id' => $this->id,
            'name' => $this->fullname,
            'avatar' => $this->avatar_url,
            'role' => $role?->name,
        ];
    }
}
