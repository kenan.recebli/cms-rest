<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class DiscountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'amount' => $this->amount,
            'unit' => $this->unit,
            'start' => $this->start?->toDateString(),
            'end' => $this->end?->toDateString(),
        ];
    }
}
