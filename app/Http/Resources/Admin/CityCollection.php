<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CityCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'continent' => $this->country->continent->name,
            'country' => $this->country->name,
            'name' => $this->name,
            'is_capital' => $this->is_capital,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
