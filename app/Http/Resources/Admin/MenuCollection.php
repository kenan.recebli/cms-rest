<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $menus = [
            'id' => $this->id,
            'name' => $this->name,
            'link' => $this->link,
            'is_active' => $this->is_active,
        ];

        if (! $this->menu_id) {
            $menus['children'] = static::collection($this->getChildrenForAdmin());
        }

        return $menus;
    }
}
