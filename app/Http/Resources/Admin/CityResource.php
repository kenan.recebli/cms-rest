<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $city = [
            'id' => $this->id,
            'country' => $this->country_id,
            'is_capital' => $this->is_capital,
        ];

        resource_translations($this, $city, [
            'name',
        ]);

        return $city;
    }
}
