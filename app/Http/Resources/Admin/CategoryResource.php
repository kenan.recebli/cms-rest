<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $category = [
            'id' => $this->id,
            'category' => $this->category_id,
            'image' => $this->image_item,
            'is_active' => $this->is_active,
        ];

        resource_translations($this, $category, [
            'name',
            'description',
        ]);

        return $category;
    }
}
