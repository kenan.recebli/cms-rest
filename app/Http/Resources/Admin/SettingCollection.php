<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->key,
            'is_file' => $this->is_file,
            'is_foreign' => $this->is_foreign,
            'is_array' => $this->is_array,
            'is_contact' => $this->is_contact,
            'is_for_dev' => $this->is_for_dev,
            'has_trans' => $this->has_trans,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}

