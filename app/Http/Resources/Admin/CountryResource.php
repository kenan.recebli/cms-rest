<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $country = [
            'id' => $this->id,
            'continent_id' => $this->continent_id,
        ];

        resource_translations($this, $country, [
            'name',
        ]);

        return $country;
    }
}
