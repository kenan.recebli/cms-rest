<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $pivot = $this->pivot;

        return [
            'id' => $this->id,
            'title' => $pivot->name,
            'quantity' => $pivot->quantity,
            'with_discount' => $pivot->with_discount,
            'discount' => $pivot->with_discount ? number_format($pivot->price - $pivot->gain, 2) : 0,
            'price' => $pivot->price,
            'gain' => $pivot->gain,
            'user_gain' => $pivot->user_gain,
        ];
    }
}

