<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $article = [
            'id' => $this->id,
            'category' => $this->category_id,
            'image' => $this->image_item,
            'images' => $this->image_items,
            'tags' => $this->tag_items,
            'is_featured' => $this->is_featured,
            'is_trashed' => (bool) $this->deleted_at,
        ];

        resource_translations($this, $article, [
            'title',
            'description',
            'content',
            'video',
        ]);

        return $article;
    }
}
