<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class FaqResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $faq = [
            'id' => $this->id,
            'is_active' => $this->is_active,
        ];

        resource_translations($this, $faq, [
            'question',
            'answer',
        ]);

        return $faq;
    }
}
