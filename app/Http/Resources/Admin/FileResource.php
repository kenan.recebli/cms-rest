<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $file = [
            'id' => $this->id,
            'folder' => $this->folder_id,
            'name' => $this->name,
            'name_ext' => $this->name.$this->ext,
            'alt' => $this->alt,
            'format' => $this->format,
            'size' => $this->size,
            'dimensions' => $this->dimensions,
            'duration' => $this->duration,
            'is_image' => $this->is_image,
            'is_video' => $this->is_video,
            'is_audio' => $this->is_audio,
            'is_svg' => $this->is_svg,
            'url' => $this->url,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        resource_translations($this, $file, [
            'name',
            'alt',
        ]);

        return $file;
    }
}
