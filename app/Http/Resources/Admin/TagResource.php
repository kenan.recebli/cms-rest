<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $tag = [
            'id' => $this->id,
        ];

        resource_translations($this, $tag, [
            'name',
            'description',
        ]);

        return $tag;
    }
}
