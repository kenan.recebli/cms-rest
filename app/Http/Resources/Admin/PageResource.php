<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $page = [
            'id' => $this->id,
            'key' => $this->key,
            'image' => $this->image_item,
        ];

        resource_translations($this, $page, [
            'title',
            'description',
            'content',
            'file',
            'video',
        ]);

        return $page;
    }
}
