<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $menu = [
            'id' => $this->id,
            'menu' => $this->menu_id,
            'is_external' => $this->is_external,
            'is_active' => $this->is_active,
        ];

        resource_translations($this, $menu, [
            'name',
            'link',
        ]);

        return $menu;
    }
}
