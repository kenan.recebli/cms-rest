<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $role = [
            'id' => $this->id,
            'key' => $this->key,
            'is_active' => $this->is_active,
            'is_admin' => $this->is_admin,
        ];

        resource_translations($this, $role, [
            'name',
        ]);

        return $role;
    }
}
