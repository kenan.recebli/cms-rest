<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'fullname' => $this->user->fullname,
            'phone' => $this->phone,
            'quantity' => $this->quantity,
            'price' => $this->amount,
            'gain' => $this->gain,
            'created_at' => $this->created_at,
            'has_discount' => $this->has_discount,
            'discount' => ($discount = $this->amount - $this->gain)
                ? number_format($discount, 2)
                : null,
        ];
    }
}

