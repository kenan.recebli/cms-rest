<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'image' => $this->image->item,
            'price' => $this->price,
            'total_price' => $this->total_price,
            'has_discount' => $this->has_discount && $this->discount,
            // 'in_wishlist' => $user?->wishlist->contains($this->id),
            // 'in_cart' => $user?->cart->contains($this->id),
            // 'views_count' => $this->views_count,
            'created_at' => $this->created_at,
            'category' => new CategoryResource($this->category),
        ];
    }
}
