<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'link' => $this->link,
            'slug' => $this->slug,
            'is_parent' => ! $this->menu_id,
            'is_external' => $this->is_external,
            'children' => MenuCollection::collection($this->menus),
        ];
    }
}
