<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'image' => $this->image?->item,
            'title' => $this->title,
            'description' => $this->description,
            'content' => $this->content,
            'slugs' => $this->slugs,
        ];
    }
}
