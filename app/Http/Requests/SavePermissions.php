<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SavePermissions extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->role->allowed();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            '*' => 'nullable|array',
            '*.permissions' => 'required|array',
            '*.permissions.*' => 'nullable|array',
            '*.permissions.*.id' => 'required|integer|exists:permissions,id',
            '*.permissions.*.can' => 'required|boolean',
        ];
    }
}
