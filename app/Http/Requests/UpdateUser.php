<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()?->is_admin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => "required|string|email|max:70|unique:users,email,$this->id",
            'first_name' => 'required|string|max:50',
            'last_name' => 'required|string|max:50',
            'avatar' => 'nullable|image|dimensions:min_width=150,min_height=150|max:10240',
            'password' => 'nullable|string|min:8',
            'confirm' => 'nullable|required_with:password|string|min:8|same:password',
            'current_password' => 'nullable|required_with:password|password',
        ];
    }
}
