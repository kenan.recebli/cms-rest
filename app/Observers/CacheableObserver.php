<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class CacheableObserver
{
    /**
     * Listen to the model created event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function created(Model $model): void
    {
        static::clearCache($model);
    }

    /**
     * Listen to the model updated event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function updated(Model $model): void
    {
        static::clearCache($model);
    }

    /**
     * Listen to the model trashed event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function trashed(Model $model): void
    {
        static::clearCache($model);
    }

    /**
     * Listen to the model restored event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function restored(Model $model): void
    {
        static::clearCache($model);
    }

    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function deleted(Model $model): void
    {
        static::clearCache($model);
    }

    /**
     * Clear the cache.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    private static function clearCache(Model $model): void
    {
        Cache::forget(get_class($model));
    }
}
