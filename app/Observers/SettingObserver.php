<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class SettingObserver
{
    /**
     * Listen to the model saved event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function saved(Model $model): void
    {
        $this->removeFromCache($model->key);
    }

    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function deleted(Model $model): void
    {
        $this->removeFromCache($model->key);
    }

    /**
     * Remove setting value from the cache.
     *
     * @param string $key
     *
     * @return void
     */
    private function removeFromCache(string $key): void
    {
        Cache::forget("setting-$key");
    }
}
