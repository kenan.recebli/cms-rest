<?php

namespace App\Observers;

use App\Models\Section;
use Illuminate\Database\Eloquent\Model;

class PostObserver
{
    /**
     * Listen to the model creating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function creating(Model $model)
    {
        $model->setAttribute('section_id', Section::getIdByKey($model->section_key));
    }
}