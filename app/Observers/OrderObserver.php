<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OrderObserver
{
    /**
     * Listen to the model creating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function creating(Model $model): void
    {
        $user = Auth::user();

        $model->setAttribute('user_id', $user->id);
        $model->setAttribute('amount', $user->cart->sum('price'));
        $model->setAttribute('gain', $user->cart->sum('total_price'));
    }

    /**
     * Listen to the model created event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function created(Model $model): void
    {
        $user = $model->user;
        $products = [];

        foreach ($user->cart as $product) {
            $pivot = $product->pivot;

            $products[$product->id] = [
                'product_name' => $product->translations->pluck('title', 'locale_id'),
                'quantity' => $pivot->quantity,
                'with_discount' => $product->has_discount,
                'discount_amount' => $product->has_discount ? $product->discount->amount : null,
                'discount_unit' => $product->has_discount ? $product->discount->unit : null,
                'price' => $product->price,
                'gain' => $product->total_price,
                'user_gain' => $product->has_discount
                    ? $product->price - $product->total_price
                    : null,
            ];
        }

        $model->products()->sync($products);

        $user->cart()->detach();
    }
}
