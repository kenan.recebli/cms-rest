<?php

namespace App\Observers;

use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;

class ModuleObserver
{
    /**
     * Listen to the model created event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function created(Model $model): void
    {
        $model->permissions()->createMany(
            array_map(function ($action) {
                return compact('action');
            }, Permission::$actions)
        );
    }
}
