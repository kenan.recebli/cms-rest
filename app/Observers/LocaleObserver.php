<?php

namespace App\Observers;

use App\Models\Locale;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class LocaleObserver
{
    /**
     * Listen to the model created event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function created(Model $model)
    {
        if ($model->is_active) {
            static::update();
        }
    }

    /**
     * Listen to the model updated event.
     *
     * @return void
     */
    public function updated()
    {
        static::update();
    }

    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function deleted(Model $model)
    {
        if ($model->is_active) {
            static::update();
        }
    }

    /**
     * Update locales in .env file.
     *
     * @return void
     */
    private static function update()
    {
        foreach ($lines = file($env = base_path('.env')) as $i => $line) {
            if (Str::startsWith($line, 'LOCALES')) {
                $lines[$i] = 'LOCALES="'.addslashes(Locale::getJson()).'"'.PHP_EOL;
            }
        }

        file_put_contents($env, implode('', $lines));
    }
}
