<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;

class FileObserver
{
    /**
     * Listen to the model updated event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function updated(Model $model): void
    {
        static::clearCache();
    }

    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function deleted(Model $model): void
    {
        $model->deleteFromStorage();

        static::clearCache();
    }

    /**
     * Clear the cache.
     *
     * @return void
     */
    private static function clearCache(): void
    {
        Artisan::call('cache:clear');
    }
}
