<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserObserver
{
    /**
     * Listen to the model updating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function updating(Model $model): void
    {
        if ($model->isDirty('avatar')) {
            $this->deleted($model);
        }
    }

    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function deleted(Model $model): void
    {
        Storage::disk('avatars')->delete($model->getOriginal('avatar')); // todo: keep?
    }
}
