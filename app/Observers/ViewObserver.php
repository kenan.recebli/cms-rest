<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

class ViewObserver
{
    /**
     * Listen to the model creating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function creating(Model $model)
    {
        $model->setAttribute('created_at', Date::now());
    }
}