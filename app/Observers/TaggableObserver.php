<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class TaggableObserver
{
    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function saved(Model $model)
    {
        if ($tags = request()->tags) {
            $model->syncTags($tags);
        }
    }

    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function deleted(Model $model)
    {
        $model->tags()->detach();
    }
}