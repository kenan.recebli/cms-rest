<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class HasKeyObserver
{
    /**
     * Listen to the model creating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function creating(Model $model)
    {
        if (! $model->key) {
            $model->setAttribute('key', Str::uuid());
        }
    }
}
