<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class OrderingObserver
{
    /**
     * Listen to the model creating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function creating(Model $model): void
    {
        $model->fillable(array_merge(['order'], $model->getFillable()));

        $model->setAttribute('order', $model->lastOrder() + 1);
    }

    /**
     * Listen to the model updating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function updating(Model $model): void
    {
        if ($model->isDirty('is_active') && $model->is_active) {
            $this->creating($model);
        }
    }
}
