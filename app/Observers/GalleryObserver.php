<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class GalleryObserver
{
    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function saved(Model $model): void
    {
        if ($images = request()->images) {
            $model->syncImages($images);
        }
    }

    /**
     * Listen to the model deleted event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function deleted(Model $model): void
    {
        $model->images()->detach();
    }
}