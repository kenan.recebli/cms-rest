<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->id();
            $table->string('continent_id', 2)->nullable()->index();
            $table->tinyInteger('country_id')->unsigned()->nullable()->index();
            $table->foreignId('city_id')->nullable()->index();
            $table->string('session', 40);
            $table->string('platform', 29);
            $table->string('browser', 29);
            $table->string('version', 19);
            $table->ipAddress('ip');
            $table->integer('visits')->unsigned()->default(1);
            $table->date('created_at');
            $table->timestamp('visited_at')->nullable();

            $table->unique([
                'session',
                'platform',
                'browser',
                'version',
                'ip',
                'created_at',
            ], 'visitors_session_unique');

            $table->foreign('continent_id')
                  ->references('id')
                  ->on('continents');

            $table->foreign('country_id')
                  ->references('id')
                  ->on('countries');

            $table->foreign('city_id')
                  ->references('id')
                  ->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('visitors');
    }
};
