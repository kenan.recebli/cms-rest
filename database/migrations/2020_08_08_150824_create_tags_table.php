<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 250);
            $table->string('name', 250);
            $table->timestamps(6);

            $table->unique(['locale_id', 'slug']);

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('tags');
    }
};
