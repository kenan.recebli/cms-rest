<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('page_translations', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->tinyInteger('page_id')->unsigned()->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 250);
            $table->string('title', 250);
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->text('keywords')->nullable();
            $table->foreignId('file_id')->nullable()->index();
            $table->string('video_id', 11)->nullable();

            $table->unique(['page_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);

            $table->foreign('page_id')
                  ->references('id')
                  ->on('pages')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');

            $table->foreign('file_id')
                  ->references('id')
                  ->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('page_translations');
    }
};
