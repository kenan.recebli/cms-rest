<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('week_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('week')->unsigned()->unique();
            $table->integer('visitors')->unsigned();
            $table->integer('views')->unsigned();
            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('week_statistics');
    }
};
