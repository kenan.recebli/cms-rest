<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('role_translations', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->tinyInteger('role_id')->unsigned()->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 70);
            $table->string('name', 70);
            $table->text('description')->nullable();

            $table->unique(['role_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);
            $table->unique(['locale_id', 'name']);

            $table->foreign('role_id')
                  ->references('id')
                  ->on('roles')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('role_translations');
    }
};
