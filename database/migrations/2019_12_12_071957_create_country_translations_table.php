<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('country_translations', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->tinyInteger('country_id')->unsigned()->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 100);
            $table->string('name', 100);

            $table->unique(['country_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);
            $table->unique(['locale_id', 'name']);

            $table->foreign('country_id')
                  ->references('id')
                  ->on('countries')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('country_translations');
    }
};
