<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('translation_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('translation_id')->index();
            $table->string('locale_id', 2)->index();
            $table->text('value')->nullable();

            $table->unique(['translation_id', 'locale_id']);

            $table->foreign('translation_id')
                  ->references('id')
                  ->on('translations')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('translation_translations');
    }
};
