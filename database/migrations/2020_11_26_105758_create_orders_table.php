<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index();
            $table->string('order_id')->nullable();
            $table->string('session_id')->nullable();
            $table->string('first_name', 50);
            $table->string('last_name', 50)->nullable();
            $table->string('email', 50);
            $table->string('phone', 20);
            $table->boolean('payment_type')->unsigned()->default(1)->comment('1 - cache, 2 - kapital');
            $table->text('note')->nullable();
            $table->string('address')->nullable();
            $table->smallInteger('quantity')->unsigned();
            $table->decimal('amount')->unsigned();
            $table->boolean('has_discount')->unsigned()->index();
            $table->decimal('discount')->unsigned()->nullable();
            $table->decimal('gain')->unsigned();
            $table->boolean('status')
                  ->unsigned()
                  ->default(0)
                  ->comment('0 - unprocessed, 1 - succeed, 2 - canceled, 3 - declined, 4 - completed');
            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
