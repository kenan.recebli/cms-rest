<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('file_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('file_id')->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 250);
            $table->string('name', 250);
            $table->string('alt', 250)->nullable();

            $table->unique(['file_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);

            $table->foreign('file_id')
                  ->references('id')
                  ->on('files')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('file_translations');
    }
};
