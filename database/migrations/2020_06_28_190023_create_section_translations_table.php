<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('section_translations', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->tinyInteger('section_id')->unsigned()->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 250);
            $table->string('title', 250);
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();

            $table->unique(['section_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);

            $table->foreign('section_id')
                  ->references('id')
                  ->on('sections')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('section_translations');
    }
};
