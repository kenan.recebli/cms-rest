<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('order_product', function (Blueprint $table) {
            $table->foreignId('order_id');
            $table->foreignId('post_id');
            $table->string('product_name', 250);
            $table->smallInteger('quantity')->unsigned()->default(1);
            $table->boolean('with_discount')->unsigned()->index();
            $table->decimal('discount_amount')->unsigned()->nullable();
            $table->boolean('discount_unit')->unsigned()->nullable()->comment('1 - currency, 2 - percent');
            $table->decimal('price')->unsigned();
            $table->decimal('gain')->unsigned();
            $table->decimal('user_gain')->unsigned()->nullable();

            $table->primary(['order_id', 'post_id']);

            $table->foreign('order_id')
                  ->references('id')
                  ->on('orders')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();

            $table->foreign('post_id')
                  ->references('id')
                  ->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('product_order');
    }
};
