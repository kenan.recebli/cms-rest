<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('faq_translations', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->smallInteger('faq_id')->unsigned()->index();
            $table->string('locale_id', 2)->index();
            $table->text('question');
            $table->text('answer');

            $table->unique(['faq_id', 'locale_id']);

            $table->foreign('faq_id')
                  ->references('id')
                  ->on('faq')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('faq_translations');
    }
};
