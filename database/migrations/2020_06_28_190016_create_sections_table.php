<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('key', 36)->unique();
            $table->foreignId('image_id')->nullable()->after('key')->index();
            $table->boolean('is_active')->unsigned()->default(false)->index();
            $table->timestamps(6);

            $table->foreign('image_id')
                  ->references('id')
                  ->on('files')
                  ->onDelete('set null')
                  ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('sections');
    }
};
