<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->smallInteger('menu_id')->unsigned()->nullable()->index();
            $table->boolean('is_external')->unsigned()->default(false)->index();
            $table->boolean('is_active')->unsigned()->index();
            $table->tinyInteger('order')->unsigned()->nullable()->index();
            $table->timestamps(6);

            $table->foreign('menu_id')
                  ->references('id')
                  ->on('menus')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('menus');
    }
};
