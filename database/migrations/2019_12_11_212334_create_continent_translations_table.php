<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('continent_translations', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('continent_id', 2)->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 250);
            $table->string('name', 250);

            $table->unique(['continent_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);
            $table->unique(['locale_id', 'name']);

            $table->foreign('continent_id')
                  ->references('id')
                  ->on('continents')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('continent_translations');
    }
};
