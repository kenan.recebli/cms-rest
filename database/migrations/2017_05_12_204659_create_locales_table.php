<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('locales', function (Blueprint $table) {
            $table->string('id', 2)->primary();
            $table->string('code', 2)->unique();
            $table->string('name', 50)->unique();
            $table->string('script', 4);
            $table->string('native', 20);
            $table->string('regional', 6)->unique();
            $table->boolean('is_active')->unsigned()->default(false)->index();
            $table->tinyInteger('order')->unsigned()->nullable()->index();
            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('locales');
    }
};
