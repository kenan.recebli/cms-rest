<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->string('key', 50)->primary();
            $table->text('value')->nullable();
            $table->foreignId('file_id')->nullable()->index();
            $table->foreignId('foreign_id')->nullable();
            $table->string('foreign_type', 50)->nullable();
            $table->boolean('is_file')->unsigned()->default(false)->index();
            $table->boolean('is_foreign')->unsigned()->default(false)->index();
            $table->boolean('is_array')->unsigned()->default(false)->index();
            $table->boolean('is_contact')->unsigned()->default(false)->index();
            $table->boolean('is_for_dev')->unsigned()->default(false)->index();
            $table->boolean('has_trans')->unsigned()->default(false)->index();
            $table->timestamps(6);

            $table->index(['foreign_id', 'foreign_type']);

            $table->foreign('file_id')
                  ->references('id')
                  ->on('files')
                  ->onUpdate('set null')
                  ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('settings');
    }
};
