<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->foreignId('folder_id')->nullable()->index();
            $table->string('filename', 34)->unique();
            $table->string('extension', 5)->nullable()->index();
            $table->integer('size')->unsigned()->index();
            $table->smallInteger('width')->unsigned()->nullable();
            $table->smallInteger('height')->unsigned()->nullable();
            $table->time('duration')->nullable();
            $table->boolean('is_image')->unsigned()->index();
            $table->boolean('is_video')->unsigned()->index();
            $table->boolean('is_audio')->unsigned()->index();
            $table->timestamps(6);
            $table->softDeletes('deleted_at', 6);

            $table->foreign('folder_id')
                  ->references('id')
                  ->on('folders')
                  ->onUpdate('set null')
                  ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }
};
