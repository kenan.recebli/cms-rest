<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('section_id')->unsigned()->index();
            $table->smallInteger('category_id')->unsigned()->nullable()->index();
            $table->foreignId('image_id')->index();
            $table->decimal('price')->unsigned()->nullable();
            $table->boolean('has_discount')->unsigned()->default(false)->index();
            $table->boolean('is_featured')->unsigned()->default(false)->index();
            $table->timestamps(6);
            $table->softDeletes('deleted_at', 6);

            $table->foreign('section_id')
                  ->references('id')
                  ->on('sections');

            $table->foreign('category_id')
                  ->references('id')
                  ->on('categories');

            $table->foreign('image_id')
                  ->references('id')
                  ->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('posts');
    }
};
