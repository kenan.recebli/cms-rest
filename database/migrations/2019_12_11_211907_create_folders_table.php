<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('folders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('folder_id')->nullable()->index();
            $table->string('name', 100);
            $table->timestamps(6);
            $table->softDeletes('deleted_at', 6);

            $table->unique(['folder_id', 'name']);

            $table->foreign('folder_id')
                  ->references('id')
                  ->on('folders')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('folders');
    }
};
