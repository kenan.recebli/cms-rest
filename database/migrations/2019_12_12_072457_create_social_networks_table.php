<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('social_networks', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('key', 15)->unique();
            $table->string('name', 15)->unique();
            $table->string('icon', 15)->unique();
            $table->text('link')->nullable();
            $table->boolean('is_active')->unsigned()->index();
            $table->tinyInteger('order')->unsigned()->nullable()->index();
            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('social_networks');
    }
};
