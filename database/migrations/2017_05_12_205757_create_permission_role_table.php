<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('permission_role', function (Blueprint $table) {
            $table->smallInteger('permission_id')->unsigned();
            $table->tinyInteger('role_id')->unsigned();

            $table->primary(['permission_id', 'role_id']);

            $table->foreign('permission_id')
                  ->references('id')
                  ->on('permissions')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();

            $table->foreign('role_id')
                  ->references('id')
                  ->on('roles')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('permission_role');
    }
};
