<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('permissions', function (Blueprint $table) {
            $comment = '1 - create, 2 - read, 3 - update, 4 - delete, 5 - trash, 6 - restore, 7 - toggle, 8 - manage';

            $table->smallIncrements('id');
            $table->tinyInteger('module_id')->unsigned()->index();
            $table->boolean('action')->unsigned()->comment($comment);
            $table->timestamps(6);

            $table->unique(['module_id', 'action']);

            $table->foreign('module_id')
                  ->references('id')
                  ->on('modules')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('permissions');
    }
};
