<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('links', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->foreignId('image_id')->index();
            $table->string('name', 30);
            $table->text('url')->nullable();
            $table->boolean('is_active')->unsigned()->index();
            $table->smallInteger('order')->unsigned()->nullable()->index();
            $table->timestamps(6);

            $table->foreign('image_id')
                  ->references('id')
                  ->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('links');
    }
};
