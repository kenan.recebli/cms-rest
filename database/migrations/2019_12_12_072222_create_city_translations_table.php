<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('city_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('city_id')->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 100);
            $table->string('name', 100);

            $table->unique(['city_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);

            $table->foreign('city_id')
                  ->references('id')
                  ->on('cities')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('city_translations');
    }
};
