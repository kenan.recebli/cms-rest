<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('menu_translations', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->smallInteger('menu_id')->unsigned()->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 70)->nullable();
            $table->string('name', 70);
            $table->text('link')->nullable();

            $table->unique(['menu_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);

            $table->foreign('menu_id')
                  ->references('id')
                  ->on('menus')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('menu_translations');
    }
};
