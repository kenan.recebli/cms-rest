<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->foreignId('image_id')->index();
            $table->foreignId('gallery_id');
            $table->string('gallery_type');

            $table->primary(['image_id', 'gallery_id', 'gallery_type']);

            $table->foreign('image_id')
                  ->references('id')
                  ->on('files')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('galleries');
    }
};
