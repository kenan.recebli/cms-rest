<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('post_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id')->index();
            $table->string('locale_id', 2)->index();
            $table->string('slug', 250);
            $table->string('title', 250);
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->text('keywords')->nullable();
            $table->string('video_id', 11)->nullable();

            $table->unique(['post_id', 'locale_id']);
            $table->unique(['locale_id', 'slug']);

            $table->foreign('post_id')
                  ->references('id')
                  ->on('posts')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('locale_id')
                  ->references('id')
                  ->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('post_translations');
    }
};
