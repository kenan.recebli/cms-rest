<?php

namespace Database\Seeders;

use App\Extensions\Date;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = [
            [
                'id' => 'az',
                'code' => 'AZ',
                'name' => 'Azerbaijani (Latin)',
                'script' => 'Latn',
                'native' => 'Azərbaycanca',
                'regional' => 'az_AZ',
            ],
            [
                'id' => 'en',
                'code' => 'EN',
                'name' => 'English',
                'script' => 'Latn',
                'native' => 'English',
                'regional' => 'en_GB',
            ],
            [
                'id' => 'ru',
                'code' => 'RU',
                'name' => 'Russian',
                'script' => 'Cyrl',
                'native' => 'Русский',
                'regional' => 'ru_RU',
            ],
            [
                'id' => 'de',
                'code' => 'DE',
                'name' => 'German',
                'script' => 'Latn',
                'native' => 'Deutsch',
                'regional' => 'de_DE',
            ],
            [
                'id' => 'es',
                'code' => 'ES',
                'name' => 'Spanish',
                'script' => 'Latn',
                'native' => 'español',
                'regional' => 'es_ES',
            ],
            [
                'id' => 'fr',
                'code' => 'FR',
                'name' => 'French',
                'script' => 'Latn',
                'native' => 'français',
                'regional' => 'fr_FR',
            ],
            [
                'id' => 'it',
                'code' => 'IT',
                'name' => 'Italian',
                'script' => 'Latn',
                'native' => 'italiano',
                'regional' => 'it_IT',
            ],
            [
                'id' => 'sv',
                'code' => 'SV',
                'name' => 'Swedish',
                'script' => 'Latn',
                'native' => 'svenska',
                'regional' => 'sv_SE',
            ],
            [
                'id' => 'vi',
                'code' => 'VI',
                'name' => 'Vietnamese',
                'script' => 'Latn',
                'native' => 'Tiếng Việt',
                'regional' => 'vi_VN',
            ],
            [
                'id' => 'tr',
                'code' => 'TR',
                'name' => 'Turkish',
                'script' => 'Latn',
                'native' => 'Türkçe',
                'regional' => 'tr_TR',
            ],
            [
                'id' => 'cs',
                'code' => 'CS',
                'name' => 'Czech',
                'script' => 'Latn',
                'native' => 'čeština',
                'regional' => 'cs_CZ',
            ],
            [
                'id' => 'el',
                'code' => 'EL',
                'name' => 'Greek',
                'script' => 'Grek',
                'native' => 'Ελληνικά',
                'regional' => 'el_GR',
            ],
            [
                'id' => 'uk',
                'code' => 'UK',
                'name' => 'Ukrainian',
                'script' => 'Cyrl',
                'native' => 'українська',
                'regional' => 'uk_UA',
            ],
            [
                'id' => 'ur',
                'code' => 'UR',
                'name' => 'Urdu',
                'script' => 'Arab',
                'native' => 'اردو',
                'regional' => 'ur_PK',
            ],
            [
                'id' => 'ar',
                'code' => 'AR',
                'name' => 'Arabic',
                'script' => 'Arab',
                'native' => 'العربية',
                'regional' => 'ar_AE',
            ],
            [
                'id' => 'fa',
                'code' => 'FA',
                'name' => 'Persian',
                'script' => 'Arab',
                'native' => 'فارسی',
                'regional' => 'fa_IR',
            ],
            [
                'id' => 'hi',
                'code' => 'HI',
                'name' => 'Hindi',
                'script' => 'Deva',
                'native' => 'हिन्दी',
                'regional' => 'hi_IN',
            ],
            [
                'id' => 'th',
                'code' => 'TH',
                'name' => 'Thai',
                'script' => 'Thai',
                'native' => 'ไทย',
                'regional' => 'th_TH',
            ],
            [
                'id' => 'ka',
                'code' => 'KA',
                'name' => 'Georgian',
                'script' => 'Geor',
                'native' => 'ქართული',
                'regional' => 'ka_GE',
            ],
            [
                'id' => 'jp',
                'code' => 'JP',
                'name' => 'Japanese',
                'script' => 'Jpan',
                'native' => '日本語',
                'regional' => 'ja_JP',
            ],
            [
                'id' => 'zh',
                'code' => 'ZH',
                'name' => 'Chinese (Simplified)',
                'script' => 'Hans',
                'native' => '简体中文',
                'regional' => 'zh_CN',
            ],
            [
                'id' => 'ko',
                'code' => 'KO',
                'name' => 'Korean',
                'script' => 'Hang',
                'native' => '한국어',
                'regional' => 'ko_KR',
            ],
        ];

        $active = [
            'az',
            'en',
            'ru',
        ];
        $date = Date::now();

        foreach ($locales as $i => &$locale) {
            $locale += [
                'order' => ++$i,
                'is_active' => in_array($locale['id'], $active),
                'created_at' => $date,
                'updated_at' => $date,
            ];
        }

        DB::table('locales')->insert($locales);
    }
}
