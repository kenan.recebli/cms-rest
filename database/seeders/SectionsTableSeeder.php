<?php

namespace Database\Seeders;

use App\Models\Section;
use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sections = [
            [
                'key' => 'articles',
                'is_active' => true,
                'title:az' => 'Məqalələr',
                'title:en' => 'Articles',
                'title:ru' => 'Статьи',
            ],
            [
                'key' => 'news',
                'is_active' => true,
                'title:az' => 'Xəbərlər',
                'title:en' => 'News',
                'title:ru' => 'Новости',
            ],
            [
                'key' => 'products',
                'is_active' => true,
                'title:az' => 'Məhsullar',
                'title:en' => 'Products',
                'title:ru' => 'Товары',
            ],
            [
                'key' => 'faq',
                'is_active' => true,
                'title:az' => 'Tez-tez soruşulan suallar',
                'title:en' => 'Frequently asked questions',
                'title:ru' => 'Часто задаваемые вопросы',
            ],
            [
                'key' => 'contacts',
                'is_active' => true,
                'title:az' => 'Bizimlə əlaqə',
                'title:en' => 'Contact us',
                'title:ru' => 'Свяжитесь с нами',
            ],
            [
                'key' => 'error',
                'is_active' => true,
                'title:az' => '404 Not Found',
                'title:en' => '404 Not Found',
                'title:ru' => '404 Not Found',
            ],
        ];

        foreach ($sections as $section) {
            Section::create($section);
        }
    }
}
