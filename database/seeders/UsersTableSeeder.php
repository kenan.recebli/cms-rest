<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $users = [
            [
                'first_name' => 'Kənan',
                'last_name' => 'Rəcəbli',
                'email' => 'kenan.recebli@gmail.com',
                'email_verified_at' => $date = Date::now(),
                'password' => '12345678',
            ],
            [
                'first_name' => 'CMS',
                'last_name' => null,
                'email' => 'info@cms.az',
                'email_verified_at' => $date,
                'password' => '12345678',
            ],
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
