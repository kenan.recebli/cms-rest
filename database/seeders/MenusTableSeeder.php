<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\Page;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'is_active' => true,
                'name:az' => 'Ana səhifə',
                'name:en' => 'Home',
                'route' => 'homepage',
            ],
            [
                'is_active' => true,
                'name:az' => 'Məhsullar',
                'name:en' => 'Products',
                'route' => 'products',
            ],
            [
                'is_active' => true,
                'name:az' => 'FAQ',
                'name:en' => 'FAQ',
                'route' => 'faq',
            ],
            [
                'is_active' => true,
                'name:az' => 'Haqqımızda',
                'name:en' => 'About us',
                'page' => 'about-us',
            ],
            [
                'is_active' => true,
                'name:az' => 'Bizimlə əlaqə',
                'name:en' => 'Contact us',
                'route' => 'contacts',
            ],
        ];

        foreach ($menus as $menu) {
            if (isset($menu['route'])) {
                foreach (config('translatable.default_locales') as $locale) {
                    $menu["link:$locale"] = localize_route($menu['route'], $locale);
                }

                unset($menu['route']);
            } elseif (isset($menu['page'])) {
                foreach (config('translatable.default_locales') as $locale) {
                    $menu["link:$locale"] = Page::getByKey($menu['page'])->url($locale);
                }

                unset($menu['page']);
            }

            Menu::create($menu);
        }
    }
}
