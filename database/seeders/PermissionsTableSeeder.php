<?php

namespace Database\Seeders;

use App\Models\Module;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Permission::clear();

        $toggable = [
            'locales',
            'roles',
            'social-networks',
            'sections',
            'faq',
            'links',
            'menus',
            'categories',
        ];

        $postable = [
            'articles',
            'news',
            'products',
        ];

        $managable = [
            'translations',
            'settings',
        ];

        $others = [
            'translations' => [
                8,
            ],
            'continents' => [
                2,
                3,
            ],
            'countries' => [
                2,
                3,
            ],
            'settings' => [
                8,
            ],
            'sections' => [
                2,
                3,
            ],
            'feedback' => [
                2,
            ],
            'orders' => [
                2,
            ],
        ];

        $range = range(1, count(Permission::$actions));

        foreach (Module::withoutGlobalScopes()->get() as $module) {
            foreach ($range as $action) {
                switch (true) {
                    case ! in_array($module->key, $postable) && in_array($action, [5, 6]):
                    case ! in_array($module->key, $toggable) && $action == 7:
                    case ! in_array($module->key, $managable) && $action == 8:
                    case array_key_exists($module->key, $others) && ! in_array($action, $others[$module->key]):
                        continue 2;
                }

                Permission::create([
                    'module' => $module->id,
                    'action' => $action,
                ]);
            }
        }
    }
}
