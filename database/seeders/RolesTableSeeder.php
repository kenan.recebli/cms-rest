<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $roles = [
            [
                'key' => 'developer',
                'is_admin' => true,
                'is_active' => true,
                'name:az' => 'Developer',
                'name:en' => 'Developer',
                'name:ru' => 'Developer',
            ],
            [
                'key' => 'ceo',
                'is_admin' => true,
                'is_active' => true,
                'name:az' => 'CEO',
                'name:en' => 'CEO',
                'name:ru' => 'CEO',
            ],
            [
                'key' => 'admin',
                'is_admin' => true,
                'is_active' => true,
                'name:az' => 'Administrator',
                'name:en' => 'Administrator',
                'name:ru' => 'Администратор',
            ],
            [
                'key' => 'editor',
                'is_admin' => true,
                'is_active' => true,
                'name:az' => 'Redaktor',
                'name:en' => 'Editor',
                'name:ru' => 'Редактор',
            ],
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
    }
}
