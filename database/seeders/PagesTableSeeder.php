<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'key' => 'about-us',
                'title:az' => 'Haqqımızda',
                'title:en' => 'About us',
            ],
            [
                'key' => 'privacy',
                'title:az' => 'Məxfilik',
                'title:en' => 'Privacy',
            ],
        ];

        foreach ($pages as $page) {
            Page::create($page);
        }
    }
}
