<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $permissions = [];

        foreach (Permission::all() as $permission) {
            $permissions[] = [
                'permission_id' => $permission->id,
                'role_id' => 3,
            ];
        }

        DB::table('permission_role')->insert($permissions);
    }
}
