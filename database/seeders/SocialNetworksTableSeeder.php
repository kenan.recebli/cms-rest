<?php

namespace Database\Seeders;

use App\Models\SocialNetwork;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SocialNetworksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $networks = [
            [
                'name' => $name = 'Facebook',
                'key' => Str::slug($name),
                'icon' => 'facebook',
                'is_active' => true,
            ],
            [
                'name' => $name = 'Twitter',
                'key' => Str::slug($name),
                'icon' => 'twitter',
                'is_active' => true,
            ],
            [
                'name' => $name = 'LinkedIn',
                'key' => Str::slug($name),
                'icon' => 'linkedin',
                'is_active' => false,
            ],
            [
                'name' => $name = 'Instagram',
                'key' => Str::slug($name),
                'icon' => 'instagram',
                'is_active' => true,
            ],
            [
                'name' => $name = 'YouTube',
                'key' => Str::slug($name),
                'icon' => 'youtube',
                'is_active' => false,
            ],
            [
                'name' => $name = 'WhatsApp',
                'key' => Str::slug($name),
                'icon' => 'whatsapp',
                'is_active' => false,
            ],
            [
                'name' => $name = 'Telegram',
                'key' => Str::slug($name),
                'icon' => 'telegram',
                'is_active' => false,
            ],
        ];

        foreach ($networks as $network) {
            SocialNetwork::create($network);
        }
    }
}
