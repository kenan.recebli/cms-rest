<?php

namespace Database\Seeders;

use App\Models\File;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $disk = Storage::disk('images');

        $files = [
            [
                'filename' => $name = 'logo',
                'extension' => $extension = 'png',
                'size' => $disk->size("$name.$extension"),
                'is_image' => true,
                'is_video' => false,
                'is_audio' => false,
                'name:az' => 'Loqo',
                'name:en' => 'Logo',
                'name:ru' => 'Лого',
            ],
        ];

        foreach ($files as $file) {
            $file = File::create($file);

            $file->setDimensionsAndDuration();
        }
    }
}
