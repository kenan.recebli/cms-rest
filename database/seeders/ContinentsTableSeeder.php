<?php

namespace Database\Seeders;

use App\Models\Continent;
use Illuminate\Database\Seeder;

class ContinentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $continents = [
            [
                'code' => 'AF',
                'name:az' => 'Afrika',
                'name:en' => 'Africa',
                'name:ru' => 'Африка',
            ],
            [
                'code' => 'AN',
                'name:az' => 'Antarktida',
                'name:en' => 'Antarctica',
                'name:ru' => 'Антарктида',
            ],
            [
                'code' => 'AS',
                'name:az' => 'Asiya',
                'name:en' => 'Asia',
                'name:ru' => 'Азия',
            ],
            [
                'code' => 'EU',
                'name:az' => 'Avropa',
                'name:en' => 'Europe',
                'name:ru' => 'Европа',
            ],
            [
                'code' => 'NA',
                'name:az' => 'Şimali Amerika',
                'name:en' => 'North America',
                'name:ru' => 'Северная Америка',
            ],
            [
                'code' => 'OC',
                'name:az' => 'Okeaniya',
                'name:en' => 'Oceania',
                'name:ru' => 'Океания',
            ],
            [
                'code' => 'SA',
                'name:az' => 'Cənubi Amerika',
                'name:en' => 'South America',
                'name:ru' => 'Южная Америка',
            ],
        ];

        foreach ($continents as $continent) {
            Continent::create($continent);
        }
    }
}
