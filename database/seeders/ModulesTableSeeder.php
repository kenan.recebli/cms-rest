<?php

namespace Database\Seeders;

use App\Models\Module;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $modules = [
            [
                'key' => 'locales',
                'is_active' => true,
            ],
            [
                'key' => 'folders',
                'is_active' => true,
            ],
            [
                'key' => 'files',
                'is_active' => true,
            ],
            [
                'key' => 'users',
                'is_active' => true,
            ],
            [
                'key' => 'roles',
                'is_active' => true,
            ],
            [
                'key' => 'translations',
                'is_active' => true,
            ],
            [
                'key' => 'continents',
                'is_active' => true,
            ],
            [
                'key' => 'countries',
                'is_active' => true,
            ],
            [
                'key' => 'cities',
                'is_active' => true,
            ],
            [
                'key' => 'settings',
                'is_active' => true,
            ],
            [
                'key' => 'social-networks',
                'is_active' => true,
            ],
            [
                'key' => 'pages',
                'is_active' => true,
            ],
            [
                'key' => 'sections',
                'is_active' => true,
            ],
            [
                'key' => 'faq',
                'is_active' => true,
            ],
            [
                'key' => 'links',
                'is_active' => true,
            ],
            [
                'key' => 'feedback',
                'is_active' => true,
            ],
            [
                'key' => 'menus',
                'is_active' => true,
            ],
            [
                'key' => 'categories',
                'is_active' => true,
            ],
            [
                'key' => 'tags',
                'is_active' => true,
            ],
            [
                'key' => 'articles',
                'is_active' => true,
            ],
            [
                'key' => 'news',
                'is_active' => true,
            ],
            [
                'key' => 'products',
                'is_active' => true,
            ],
            [
                'key' => 'orders',
                'is_active' => true,
            ],
        ];

        foreach ($modules as $module) {
            Module::create($module);
        }
    }
}
