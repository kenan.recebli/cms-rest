<?php

return [

    'homepage' => '',
    'articles' => 'stati',
    'news' => 'novosti',
    'products' => 'produkty',
    'pages' => 'stranicy',
    'faq' => 'casto-zadavaemye-voprosy',
    'contacts' => 'svyazites-s-nami',
    'categories' => 'kategorii',
    'tags' => 'tegi',

];
