<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Files Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to translate file-specific lines.
    |
    */

    'invalid_file' => 'The uploaded file is not valid.',
    'unexpected_error' => 'Unexpected error occurred during upload.',

    // Size units
    'B' => 'байт|байта',
    'KB' => 'КБ',
    'MB' => 'МБ',
    'GB' => 'ГБ',
    'TB' => 'ТБ',

];
