<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'İstifadəçi adı və ya şifrə səhvdir',
    'password' => 'Təqdim olunan şifrə səhvdir.',
    'throttle' => 'Həddindən artıq daxil olma cəhdləri. :seconds saniyə sonra yenidən cəhd edin.',

];
