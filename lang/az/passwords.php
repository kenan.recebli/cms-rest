<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Şifrəniz yeniləndi!',
    'sent' => 'Şifrə yeniləmə linki sizə E-mail olaraq göndərildi!',
    'throttled' => 'Yenidən cəhd etməzdən əvvəl gözləyin.',
    'token' => 'Şifrə yeniləmə tokeni etibarsızdır.',
    'user' => 'Bu E-mail\'ə uyğun istifadəçi tapılmadı.',

];
