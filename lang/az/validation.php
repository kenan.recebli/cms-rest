<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute qəbul edilməlidir.',
    'accepted_if' => 'The :attribute must be accepted when :other is :value.',
    'active_url' => ':attribute doğru URL deyil.',
    'after' => ':attribute :date tarixindən sonra olmalıdır.',
    'after_or_equal' => ':attribute :date tarixi ilə eyni və ya sonra olmalıdır.',
    'alpha' => ':attribute yalnız hərflərdən ibarət ola bilər.',
    'alpha_dash' => ':attribute yalnız hərf, rəqəm və tire simvolundan ibarət ola bilər.',
    'alpha_num' => ':attribute yalnız hərf və rəqəmlərdən ibarət ola bilər.',
    'array' => ':attribute massiv formatında olmalıdır.',
    'before' => ':attribute :date tarixindən əvvəl olmalıdır.',
    'before_or_equal' => ':attribute :date tarixindən əvvəl və ya bərabər olmalıdır.',
    'between' => [
        'array' => ':attribute :min ilə :max intervalında hissədən ibarət olmalıdır.',
        'file' => ':attribute :min ilə :max KB ölçüsü intervalında olmalıdır.',
        'numeric' => ':attribute :min ilə :max arasında olmalıdır.',
        'string' => ':attribute :min ilə :max simvolu intervalında olmalıdır.',
    ],
    'boolean' => ':attribute doğru və ya yanlış ola bilər.',
    'confirmed' => ':attribute doğrulanması yanlışdır.',
    'current_password' => 'Cari şifrə yanlışdır.',
    'date' => ':attribute tarix formatında olmalıdır.',
    'date_equals' => ':attribute :date-ə bərabər bir tarix olmalıdır.',
    'date_format' => ':attribute :format formatında olmalıdır.',
    'declined' => 'The :attribute must be declined.',
    'declined_if' => 'The :attribute must be declined when :other is :value.',
    'different' => ':attribute və :other fərqli olmalıdır.',
    'digits' => ':attribute :digits rəqəmli olmalıdır.',
    'digits_between' => ':attribute :min ilə :max rəqəmləri intervalında olmalıdır.',
    'dimensions' => ':attribute doğru şəkil ölçülərində deyil.',
    'distinct' => ':attribute dublikat qiymətlidir.',
    'doesnt_end_with' => 'The :attribute may not end with one of the following: :values.',
    'doesnt_start_with' => 'The :attribute may not start with one of the following: :values.',
    'email' => ':attribute doğru email formatında deyil.',
    'ends_with' => ':attribute nömrəsi aşağıdakılardan biri ilə bitməlidir: :values.',
    'enum' => 'The selected :attribute is invalid.',
    'exists' => 'Seçilmiş :attribute yanlışdır.',
    'file' => ':attribute fayl formatında olmalıdır.',
    'filled' => ':attribute qiyməti olmalıdır.',
    'gt' => [
        'array' => 'The :attribute must have more than :value items.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'numeric' => 'The :attribute must be greater than :value.',
        'string' => 'The :attribute must be greater than :value characters.',
    ],
    'gte' => [
        'array' => 'The :attribute must have :value items or more.',
        'file' => 'The :attribute must be greater than or equal to :value kilobytes.',
        'numeric' => 'The :attribute must be greater than or equal to :value.',
        'string' => 'The :attribute must be greater than or equal to :value characters.',
    ],
    'image' => ':attribute şəkil formatında olmalıdır.',
    'in' => 'Seçilmiş :attribute yanlışdır.',
    'in_array' => ':attribute :other qiymətləri arasında olmalıdır.',
    'integer' => ':attribute tam ədəd olmalıdır.',
    'ip' => ':attribute İP adres formatında olmalıdır.',
    'ipv4' => ':attribute İPv4 adres formatında olmalıdır.',
    'ipv6' => ':attribute İPv6 adres formatında olmalıdır.',
    'json' => ':attribute JSON formatında olmalıdır.',
    'lt' => [
        'array' => 'The :attribute must have less than :value items.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'numeric' => 'The :attribute must be less than :value.',
        'string' => 'The :attribute must be less than :value characters.',
    ],
    'lte' => [
        'array' => 'The :attribute must not have more than :value items.',
        'file' => 'The :attribute must be less than or equal to :value kilobytes.',
        'numeric' => 'The :attribute must be less than or equal to :value.',
        'string' => 'The :attribute must be less than or equal to :value characters.',
    ],
    'mac_address' => 'The :attribute must be a valid MAC address.',
    'max' => [
        'array' => ':attribute maksimum :max hədd\'dən ibarət olmalıdır.',
        'file' => ':attribute maksimum :max KB ölçüsündə olmalıdır.',
        'numeric' => ':attribute maksiumum :max rəqəmdən ibarət olmalıdır.',
        'string' => ':attribute maksimum :max simvoldan ibarət olmalıdır.',
    ],
    'max_digits' => 'The :attribute must not have more than :max digits.',
    'mimes' => ':attribute :values tipində fayl olmalıdır.',
    'mimetypes' => ':attribute :values tipində fayl olmalıdır.',
    'min' => [
        'array' => ':attribute minimum :min hədd\'dən ibarət olmalıdır.',
        'file' => ':attribute minimum :min KB ölçüsündə olmalıdır.',
        'numeric' => ':attribute minimum :min rəqəmdən ibarət olmalıdır.',
        'string' => ':attribute minimum :min simvoldan ibarət olmalıdır.',
    ],
    'min_digits' => 'The :attribute must have at least :min digits.',
    'multiple_of' => ':attribute, :value multiples olmalıdır.',
    'not_in' => 'Seçilmiş :attribute yanlışdır.',
    'not_regex' => 'Format :attribute qəbuledilməzdir.',
    'numeric' => ':attribute rəqəmlərdən ibarət olmalıdır.',
    'password' => [
        'letters' => 'The :attribute must contain at least one letter.',
        'mixed' => 'The :attribute must contain at least one uppercase and one lowercase letter.',
        'numbers' => 'The :attribute must contain at least one number.',
        'symbols' => 'The :attribute must contain at least one symbol.',
        'uncompromised' => 'The given :attribute has appeared in a data leak. Please choose a different :attribute.',
    ],
    'present' => ':attribute iştirak etməlidir.',
    'prohibited' => ':attribute sahəsi qadağandır.',
    'prohibited_if' => 'Sahəsində :attribute qadağan zaman :other-:value.',
    'prohibited_unless' => ':attribute sahəsi yalnız :other :values-da olmadıqda qadağandır.',
    'prohibits' => 'The :attribute field prohibits :other from being present.',
    'regex' => ':attribute formatı yanlışdır.',
    'required' => 'Bu xana mütləqdir.',
    'required_array_keys' => 'The :attribute field must contain entries for: :values.',
    'required_if' => 'Bu xana mütləqdir.',
    'required_if_accepted' => 'Bu xana mütləqdir.',
    'required_unless' => 'Bu xana mütləqdir.',
    'required_with' => 'Bu xana mütləqdir.',
    'required_with_all' => 'Bu xana mütləqdir.',
    'required_without' => 'Bu xana mütləqdir.',
    'required_without_all' => 'Bu xana mütləqdir.',
    'same' => ':attribute və :other eyni olmalıdır.',
    'size' => [
        'array' => ':attribute :size hədd\'dən ibarət olmalıdır.',
        'file' => ':attribute :size KB ölçüsündə olmalıdır.',
        'numeric' => ':attribute :size ölçüsündə olmalıdır.',
        'string' => ':attribute :size simvoldan ibarət olmalıdır.',
    ],
    'starts_with' => ':attribute aşağıdakılardan biri ilə başlamalıdır: :values.',
    'string' => ':attribute hərf formatında olmalıdır.',
    'timezone' => ':attribute ərazi formatında olmalıdır.',
    'unique' => 'Bu ad artıq mövcuddur.',
    'uploaded' => ':attribute yüklənməsi mümkün olmadı.',
    'url' => ':attribute formatı yanlışdır.',
    'uuid' => ':attribute etibarlı UUID olmalıdır.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'confirm' => [
            'same' => 'Şifrələr uyğun gəlmir.',
        ],
        'image.id' => [
            'required' => 'Şəkil seçilməyib.',
        ],
        '*' => [
            'date_format' => 'Bu xana düzgün deyil.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
