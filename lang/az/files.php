<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Files Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to translate file-specific lines.
    |
    */

    'invalid_file' => 'The uploaded file is not valid.',
    'unexpected_error' => 'Unexpected error occurred during upload.',

    // Size units
    'B' => 'bayt',
    'KB' => 'KB',
    'MB' => 'MB',
    'GB' => 'GB',
    'TB' => 'TB',

];
