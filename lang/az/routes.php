<?php

return [

    'homepage' => '',
    'articles' => 'meqaleler',
    'news' => 'xeberler',
    'products' => 'mehsullar',
    'pages' => 'sehifeler',
    'faq' => 'tez-tez-sorusulan-suallar',
    'contacts' => 'bizimle-elaqe',
    'categories' => 'kateqoriyalar',
    'tags' => 'teqler',

];
