<?php

return [

    /*
    |--------------------------------------------------------------------------
    | DateTime Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to translate date/time.
    |
    */

    // Days of week (short)
    'Sun' => 'B',
    'Mon' => 'B.e',
    'Tue' => 'Ç.a',
    'Wed' => 'Ç',
    'Thu' => 'C.a',
    'Fri' => 'C',
    'Sat' => 'Ş',

    // Days of week
    'Sunday' => 'Bazar',
    'Monday' => 'Bazar ertəsi',
    'Tuesday' => 'Çərşənbə axşamı',
    'Wednesday' => 'Çərşənbə',
    'Thursday' => 'Cümə axşamı',
    'Friday' => 'Cümə',
    'Saturday' => 'Şənbə',

    // Months (short)
    'Jan' => 'Yan',
    'Feb' => 'Fev',
    'Mar' => 'Mar',
    'Apr' => 'Apr',
    'May' => 'May',
    'Jun' => 'İyn',
    'Jul' => 'İyl',
    'Aug' => 'Avq',
    'Sep' => 'Sen',
    'Oct' => 'Okt',
    'Nov' => 'Noy',
    'Dec' => 'Dek',

    // Months
    'January' => 'Yanvar',
    'February' => 'Fevral',
    'March' => 'Mart',
    'April' => 'Aprel',
    'June' => 'İyun',
    'July' => 'İyul',
    'August' => 'Avqust',
    'September' => 'Sentyabr',
    'October' => 'Oktyabr',
    'November' => 'Noyabr',
    'December' => 'Dekabr',

    // Time
    'd' => 'Gün',
    'h' => 'Saat',
    'i' => 'Dəq',
    's' => 'San',

];
