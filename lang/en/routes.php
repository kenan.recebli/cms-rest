<?php

return [

    'homepage' => '',
    'articles' => 'articles',
    'news' => 'news',
    'products' => 'products',
    'pages' => 'pages',
    'faq' => 'frequently-asked-questions',
    'contacts' => 'contact-us',
    'categories' => 'categories',
    'tags' => 'tags',

];
