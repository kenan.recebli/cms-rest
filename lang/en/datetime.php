<?php

return [

    /*
    |--------------------------------------------------------------------------
    | DateTime Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to translate date/time.
    |
    */

    // Days of week (short)
    'Sun' => 'Sun',
    'Mon' => 'Mon',
    'Tue' => 'Tue',
    'Wed' => 'Wed',
    'Thu' => 'Thu',
    'Fri' => 'Fri',
    'Sat' => 'Sat',

    // Days of week
    'Sunday' => 'Sunday',
    'Monday' => 'Monday',
    'Tuesday' => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday' => 'Thursday',
    'Friday' => 'Friday',
    'Saturday' => 'Saturday',

    // Months (short)
    'Jan' => 'Jan',
    'Feb' => 'Feb',
    'Mar' => 'Mar',
    'Apr' => 'Apr',
    'May' => 'May',
    'Jun' => 'Jun',
    'Jul' => 'Jul',
    'Aug' => 'Aug',
    'Sep' => 'Sep',
    'Oct' => 'Oct',
    'Nov' => 'Nov',
    'Dec' => 'Dec',

    // Months
    'January' => 'January',
    'February' => 'February',
    'March' => 'March',
    'April' => 'April',
    'June' => 'June',
    'July' => 'July',
    'August' => 'August',
    'September' => 'September',
    'October' => 'October',
    'November' => 'November',
    'December' => 'December',

    // Time
    'd' => 'Day|Days',
    'h' => 'Hour|Hours',
    'i' => 'Min|Mins',
    's' => 'Sec|Secs',

];
